<?php
	require_once('inc/loki.php');
	$loki = new loki;
	$loki->loki_lock(0);

	$periodo = 3;
	$objetivo = 5000;
	$objmayor = 1000;
	$objmedio = 1/14;
	$t = time();
	$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

#VENDEDOR
	if($_SESSION['tipo'] == 3){
		echo '<h2>Para leer los códigos, abre cualquier lector QR en tu celular</h2>';
	}

#PROMOTOR
	elseif($_SESSION['tipo'] == 2){
		require_once('inc/sql.php');
		$sql = new sql;

			echo '
				<h2>Tu QR para compartir es:</h2>
				<div>
					<a href="img/'.$_SESSION['qr'].'">
						<img src="img/'.$_SESSION['qr'].'" alt="Código QR" style="width: 10em; max-width: 80%;">
					</a>
				</div>
			';

		if($dato = $sql->sacar('select * from transaccion where tid = '.$_SESSION['tid'].' and tiempo >= '.(time() - (2678400*$periodo)).' order by tiempo desc')){

			echo '
				<h1>'.$_SESSION['nombre'].' '.$_SESSION['apellido'].'</h1>
			';

			for ($i=0; $i < $periodo; $i++) {
				#Resumen
				$monto = 0;
				echo '
					<h2>'.$meses[(date('m', (time() ))- $i)].'</h2>
					<table>
						<tr>
							<th>Fecha</th>
							<th>Cuenta</th>
						</tr>
				';
				foreach ($dato as $key => $val) {
					if (date("m",$val['tiempo']) == (date("m",$t) - $i ) && date("Y",$val['tiempo']) == (date("Y",$t))) {
						$monto = $monto + $val['monto'];
						echo '
							<tr>
								<td>'.date("y-m-d",$val['tiempo']).'</td>
								<td>S/. '.$val['monto'].'</td>
							</tr>
						';
					}
				}
				$comision1 = floor($monto / $objetivo);
				$comision2 = $monto % $objetivo;

				$comision = ceil(($comision1*1000) + $comision2/14);

				echo '
					</table>
					<h4>Acumulado = S/. '.$monto.'</h4>
					<h4>Comisión = S/. '.$comision.'</h4>
				';
			}


		}
		else{
			echo '<h2>Aún no hay venta.</h2>';
		}
	}

#ADMINISTRADOR
	elseif($_SESSION['tipo'] == 1){
		require_once('inc/sql.php');
		$sql = new sql;

		if (isset($_GET['tid']) && isset($_GET['nombre'])) {

			echo '
				<form class="s-flex s12 s-h-10 fondo-img-1" action="index.php" method="POST">
					<div class="s12 s-pad-2">
						<input class="boton" id="submit" type="submit" value="Volver"/>
					</div>
				</form>
			';

			if($dato = $sql->sacar('select * from transaccion where tid = '.$_GET['tid'].' and tiempo >= '.(time() - (2678400*$periodo)).' order by tiempo desc')){


				echo '
					<h1>'.$_GET['nombre'].'</h1>
				';

				for ($i=0; $i < $periodo; $i++) {
					#Resumen
					$monto = 0;
					echo '
						<h2>'.$meses[(date('m', (time() ))- $i)].'</h2>
						<table>
							<tr>
								<th>Fecha</th>
								<th>Cuenta</th>
							</tr>
					';
					foreach ($dato as $key => $val) {
						if (date("m",$val['tiempo']) == (date("m",$t) - $i ) && date("Y",$val['tiempo']) == (date("Y",$t))) {
							$monto = $monto + $val['monto'];
							echo '
								<tr>
									<td>'.date("y-m-d",$val['tiempo']).'</td>
									<td>S/. '.$val['monto'].'</td>
								</tr>
							';
						}
					}
					$comision1 = floor($monto / $objetivo);
					$comision2 = $monto % $objetivo;

					$comision = ceil(($comision1*1000) + $comision2/14);

					echo '
						</table>
						<h4>Acumulado = S/. '.$monto.'</h4>
						<h4>Comisión = S/. '.$comision.'</h4>
					';
				}
			}
			else{
				echo '<h2>Aún no hay ventas.</h2>';
			}
		}
		else{
			if($venta = $sql->sacar('select * from transaccion where tiempo >= '.(time() - (2678400*$periodo)).' order by tid desc, tiempo desc')){
				if($promotor = $sql->sacar('select * from trabajador where tipo = 2 order by tid desc')){
					for ($i=0; $i < $periodo; $i++) {
						#Resumen
						$monto = 0;
						echo '
							<h2>'.$meses[(date('m', (time() ))- $i)].'</h2>
							<table>
								<tr>
									<th>Nombre</th>
									<th>Venta</th>
									<th>Comision</th>
								</tr>
						';

						foreach ($promotor as $key1 => $val1) {
							$monto = 0;
							foreach ($venta as $key2 => $val2) {
								if ($val1['tid'] == $val2['tid']) {
									if (date("m",$val2['tiempo']) == (date("m",$t) - $i ) && date("Y",$val2['tiempo']) == (date("Y",$t))) {
										$monto = $monto + $val2['monto'];
									}
								}
							}
							$comision1 = floor($monto / $objetivo);
							$comision2 = $monto % $objetivo;
							$comision = ceil(($comision1*1000) + $comision2/14);

							echo '
								<tr>
									<th><a href="index.php?tid='.$val1['tid'].'&nombre='.$val1['nombre'].' '.$val1['apellido'].'">'.$val1['nombre'].' '.$val1['apellido'].'</a></th>
									<th>'.$monto.'</th>
									<th>'.$comision.'</th>
								</tr>
							';
						}
						echo '</table>';
					}
				}
				else{
					echo '<h2>Aún no hay ventas.</h2>';
				}
			}
			else{
				echo '<h2>Aún no hay ventas.</h2>';
			}
		}
	}

	echo '
		<form class="s-flex s12 s-h-10 fondo-img-1" action="clave.php" method="POST">
			<div class="s12 s-pad-2">
				<input class="boton" id="submit" type="submit" value="Cambiar Mi Contraseña"/>
			</div>
		</form>
	';
?>