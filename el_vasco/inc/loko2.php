<?php

require_once('loki.php');
$loki = new loki;

if (isset($_GET['origen']) && !empty($_GET['origen'])) {
	if(!$loki->session_rev('tid') && !$loki->cookie_rev('tid') && isset($_POST['tid']) && !empty($_POST['tid'])){
		if( $loki->login_form_procesar()){
			if( $loki->login_session()){
				redir($_GET['origen']);
			}
			else{
				redir('loko1.php', array('origen'=>$_GET['origen']));
			}
		}
		else{
			redir('loko1.php', array('origen'=>$_GET['origen']));
		}
	}
	else{
		redir('loko1.php', array('origen'=>$_GET['origen']));
	}
}
else{
	redir('../index.php');
}
?>