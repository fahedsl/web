<?php
require_once('loki.php');
$loki = new loki;
if (isset($_GET['origen']) && !empty($_GET['origen'])) {
	if($loki->session_rev()) {
		echo "ses";
		$loki->session_unset();
	}
	if($loki->cookie_rev()) {
		echo "coo";
		$loki->cookie_unset();
	}
	else{
		echo "nocoo";
	}
	redir($_GET['origen']);
}
else{
	redir('../index.php');
}
?>