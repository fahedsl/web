 <?php

/***********************************************************************************************************************************

REQUIERE: SQL.PHP, SSL.PHP, FUNCION.PHP
REDIRIGE: LOKO1.PHP, LOKO2.PHP, LOKO3.PHP

CLASE LOCK (bloqueo y login)

***********************************************************************************************************************************/
class loki{
	private $sql, $ssl;

	function __construct(){
		require_once('ssl.php');
		require_once('sql.php');
		require_once('funcion.php');
		$this->ssl = new ssl;
		$this->sql = new sql;
	}


/* COOKIE */
/***********************************************************************************************************************************/
/*
	function cookie_work($nombre='galletitadeprueba'){
		if(count($_COOKIE) <= 0) {
			setcookie($nombre, "prueba", time() + 3600, '/');
			if(count($_COOKIE) > 0) {
				return(TRUE);
			}
			else {
				return(FALSE);
			}
		}
		else{
			return(TRUE);
		}
	}
*/

	function cookie_rev($nombre=NULL){
		if(!empty($nombre)) {
			if(isset($_COOKIE[$nombre])) {
				return(TRUE);
			}
			else {
				return(FALSE);
			}
		}
		else{
			if(isset($_COOKIE) && !empty($_COOKIE)) {
				return(TRUE);
			}
			else{
				return(FALSE);
			}
		}
	}

	function cookie_set($nombre='galleta', $valor='galleta', $horas = (30*24)){
		if (isset($nombre) && isset($valor) && isset($horas)) {
			if(setcookie($nombre, $valor, time() + (3600 * $horas), "/")){
				return(TRUE);
			}
			else {
				return(FALSE);
			}
		}
		else {
			return(FALSE);
		}
	}

	function cookie_unset($nombres = NULL){
		if($nombres !== NULL){
			if(is_array($nombres)){
				foreach ($nombres as $key) {
					if(isset($_COOKIE[$key])){
						setcookie($key, NULL, -1,'/');
					}
				}
			}
			else{
				if(isset($_COOKIE[$nombres])){
					setcookie($nombres, NULL, -1,'/');
				}
			}
			return(TRUE);
		}
		else{
			foreach ($_COOKIE as $key=>$val) {
				if(setcookie($key, NULL, -1,'/')){
				}
				if (count($_COOKIE) <= 0) {
					return(TRUE);
				}
			}
		}
	}


/* SESION */
/***********************************************************************************************************************************/
	function session_rev($nombre = NULL){
		if (session_status() == PHP_SESSION_NONE) { 
			session_start();
		}

		if (isset($nombre) && !empty($nombre)) {
			if (isset($_SESSION[$nombre]) && !empty($_SESSION[$nombre])){
				return(TRUE);
			}
			return(FALSE);
		}
		else {
			if (isset($_SESSION) && !empty($_SESSION)){
				return(TRUE);
			}
			return(FALSE);
		}
	}

	function session_set($nombre='dato', $valor='dato'){
		if (session_status() == PHP_SESSION_NONE) { 
			session_start();
		}

		if($_SESSION[$nombre] = $valor){
			return(TRUE);
		}
		else{
			return(FALSE);
		}
	}

	function session_unset($nombres = NULL){
		if (session_status() == PHP_SESSION_NONE) { 
			session_start();
		}

		if($nombres !== NULL){
			if(is_array($nombres)){
				foreach ($nombres as $key) {
					if(isset($_SESSION[$key])){
						unset($_SESSION[$key]);
					}
				}
			}
			else{
				if(isset($_SESSION[$nombres])){
					unset($_SESSION[$nombres]);
				}
			}
			return(TRUE);
		}
		else{
			if(isset($_SESSION) && !empty($_SESSION)){
				foreach ($_SESSION as $key=>$val) {
					unset($_SESSION[$key]);
					if (count($_SESSION) <= 0) {
						return(TRUE);
					}
				}
			}
		}
	}


/* ENTRADA */
/***********************************************************************************************************************************/


	function loki_lock($boton1=TRUE, $boton2=TRUE, $origen=NULL, $nombre =NULL){
		if (empty($origen)) {
			$origen = $_SERVER['REQUEST_URI'];
		}
		if($this->session_rev('tid') || $this->cookie_rev('tid')){
			if($this->cookie_rev('tid')){
				$this->login_cookies();
			}
			if($this->session_rev('tid')) {
				$this->login_session();
			}
			$nombre = 'Salir';
			$num = 3;
			echo'
				<form class="s-flex s12 s-h-10 fondo-img-1" action="inc/loko'.$num.'.php?origen='.$origen.'" method="POST">
					<div class="s12 s-pad-2">
						<input class="boton" id="submit" type="submit" value="'.$nombre.'"/>
					</div>
		 		</form>
			';
		}
		else{
			$nombre = 'Ingresar';
			$num = 1;
			if ($boton1 == TRUE) {
				echo'
					<form class="s-flex s12 s-h-10 fondo-img-1" action="inc/loko'.$num.'.php?origen='.$origen.'" method="POST">
						<div class="s12 s-pad-2">
							<input class="boton" id="submit" type="submit" value="'.$nombre.'"/>
						</div>
			 		</form>
				';
			}
			else{
				redir('inc/loko1.php', array('origen'=>$origen));
				exit();
			}
		}
	}

	function login_session(){
		if(isset($_SESSION['tid'])){
			if($tid = $this->ssl->encriptar($_SESSION['tid'])){
				if($this->cookie_set('tid',$tid)){
					return(TRUE);
				}
				return(FALSE);
			}
			return(FALSE);
		}
	}

	function login_cookies(){
		if($tid = $this->ssl->desencriptar($_COOKIE['tid'])){
			if($datos = $this->sql->sacar('select * from trabajador where tid = '.$tid)){
 				foreach ($datos[0] as $key=>$val) {
					$this->session_set($key, $val);
				}
				return(TRUE);
			}
			return(FALSE);
		}
		return(FALSE);
	}

	function login_form_procesar(){
		if(!$this->session_rev() && !$this->cookie_rev('pase') && isset($_POST['tid']) && !empty($_POST['tid'])){
			if($datos = $this->sql->sacar('select * from trabajador where tid = '.$_POST['tid'])){
				foreach ($datos[0] as $key=>$val) {
					$this->session_set($key, $val);
				}
				return(TRUE);
			}
			return(FALSE);
		}
		return(FALSE);
	}

	function login_form($origen=NULL){
		if ($origen === NULL) {
			$origen = $_SERVER['PHP_SELF'];
		}
		echo'
			<body style="width:100vw; height:100vh;">
				<form class="s-flex s12 s-h-10 fondo-img-1" action="loko2.php?origen='.$origen.'" method="POST">
					<div class="s12 s-pad-2">
						<input class="entrada" id="tid" name="tid" type="text" required="" placeholder="Usuario"/></div>
					<div class="s12 s-pad-2">
						<input class="entrada" id="pase" name="pase" type="password" required="" placeholder="PIN"/></div>
					<div class="s12 s-pad-2">
						<input class="boton" id="submit" type="submit" value="Entrar"/>
					</div>
		 		</form>
			</body>
		';
		exit();
	}
}

?>