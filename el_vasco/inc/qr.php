<?php

class qr{

	function __construct(){
		require_once ('qrgen/qrlib.php');
	}

	/* Crea un código QR en formato PNG */
	function crear($contenido, $nombre, $carpeta = 'img/qr'){
		if(isset($contenido) && isset($nombre)){
			$png_dir = dirname(__FILE__).'/..'.DIRECTORY_SEPARATOR.$carpeta.DIRECTORY_SEPARATOR;
			if (!file_exists($png_dir)){
				mkdir($png_dir);
			}

			$filename = $png_dir.$nombre.'.png';

			QRcode::png($contenido, $filename, 'H', 10, 2);

			return($filename);
		}
		return(FALSE);
	}

}

?>