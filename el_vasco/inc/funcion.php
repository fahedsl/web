<?php
	function redir($direccion = 'index.php',$gets=NULL){
		if (isset($gets) && !empty($gets)) {
			$direccion .='?';
			if (is_array($gets)) {
				foreach($gets as $getsNom => $getsVal){
					$direccion .='&'.$getsNom.'='.$getsVal;
				}
			}
			elseif (!is_array($gets)) {
				$direccion .= $gets;
			}
		}
		/*echo $direccion;*/
		header('location:'.$direccion);
	}

	function validar_entrada($texto, $max=1000){
		if(isset($texto)){
			if(strlen($texto) <= $max){
				$texto = filter_var($texto, FILTER_SANITIZE_STRING);
				$texto = htmlspecialchars($texto);
				return $texto;
			}
		}
	}
?>