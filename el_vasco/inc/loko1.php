<?php
require_once('loki.php');
$loki = new loki;

if (isset($_GET['origen']) && !empty($_GET['origen'])) {
	if(!$loki->session_rev('tid') && !$loki->cookie_rev('tid')){
		$loki->login_form($_GET['origen']);
	}
	else{
		redir($_GET['origen']);
	}
}
else{
	redir('../index.php');
}
?>