<?php

class sql{

	private $server, $user, $pass, $db, $conn;


	function __construct(){
		if(empty($this->server)){ $this->server = 'localhost'; }
		if(empty($this->user)){ $this->user = 'f'; }
		if(empty($this->pass)){ $this->pass = '123'; }
		if(empty($this->db)){ $this->db = 'vasco'; }
		$this->conn = new mysqli($this->server, $this->user, $this->pass, $this->db);
		if ($this->conn->connect_error) {die($this->conn->connect_error);}
	}


	function __destruct(){
		$this->conn->close();
	}


	function sacar($pedido){
		if(!empty($pedido)){
			if ($result = $this->conn->query($pedido)){
				if ($result->num_rows > 0) {
					$sacado = array();
					while($row = $result->fetch_assoc()){
						$sacado [] = $row;
					}
					return($sacado);
				}
				else{ return(FALSE); }
			}
			else{ return(FALSE); }
		}
		else{ return(FALSE); }
	}


	function poner($pedido){
		if(!empty($pedido)){
			if ($this->conn->query($pedido)){
				$ultimo = $this->conn->insert_id;
				if($ultimo != 0){
					return($ultimo);
				}
				else{
					return(TRUE);
				}
			}
			else{ return(FALSE); }
		}
		else{ return(FALSE); }
	}

}

?>