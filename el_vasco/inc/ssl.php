<?php

class ssl{

	private $metodo, $llave, $iv;

	function __construct(){
		if(empty($metodo)){ $this->metodo = 'AES-256-CBC'; }
		if(empty($llave)){ $this->llave = 'ys2b7kmpkfandncqk'; }
		if(empty($iv)){ $this->iv = '1234567890123456'; }
	}

	function encriptar($entrada){
		try{
			$salida = openssl_encrypt($entrada, $this->metodo, $this->llave, FALSE, $this->iv);
			return($salida);
		}
		catch(Exception $e){
			return(FALSE);
		}
	}

	function desencriptar($entrada){
		try{
			$salida = openssl_decrypt($entrada, $this->metodo, $this->llave, FALSE, $this->iv);
			return($salida);
		}
		catch(Exception $e){
			return(FALSE);
		}
	}

}

?>