<?php
// Recibe el codigo QR y si es que existe, muestra el formulario para llenar los datos de la venta, si no existe o no es correcto, envia al inicio.

	require_once('inc/loki.php');
	$loki = new loki;
	$loki->loki_lock(0);

	if($_SESSION['tipo'] == 1 || $_SESSION['tipo'] == 3){
		if(isset($_GET['tid'])){
			require_once('inc/ssl.php');
			$ssl = new ssl;

			if($ssl->desencriptar($_GET['tid'])){
				echo'
					<body style="width:100vw; height:100vh;">
						<form action="qr1.php" method="POST">
							<input type="text" name="tid" value="'.$_GET['tid'].'" readonly="" hidden="">
							<input type="number" name="monto"  placeholder="Monto" required="">
							<input type="number" name="cliente" placeholder="DNI del cliente (opcional)" >
							<input type="submit">
						</form>
					</body>
				';

			}
			else{
				redir('index.php', array('msj'=>'Código NO desencriptado.'));
			}

		}
		else{
			redir('index.php', array('msj'=>'Código incorrecto.'));
		}
	}
	else{
		redir('index.php', array('msj'=>'No tiene permitido registrar ventas.'));
	}
		

?>
