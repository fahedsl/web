<?php
	require_once('inc/loki.php');
	$loki = new loki;
	$loki->loki_lock(0);

	if($_SESSION['tipo'] == 1 || $_SESSION['tipo'] == 3){
		if (isset($_POST['tid']) && !empty($_POST['tid']) && isset($_POST['monto']) && !empty($_POST['monto'])) {
			require_once('inc/ssl.php');
			require_once('inc/sql.php');
			require_once('inc/funcion.php');
			$ssl = new ssl;
			$sql = new sql;

			$_POST['monto'] = validar_entrada($_POST['monto']);
			$monto = $_POST['monto'] * 0.8;

			if (!isset($_POST['cliente']) || empty($_POST['cliente'])) {
				$_POST['cliente'] = 0;
			}
			else{
				$_POST['cliente'] = validar_entrada($_POST['cliente']);
			}
			if($tid = $ssl->desencriptar($_POST['tid'])) {
				if($trid = $sql->poner('insert into transaccion (tid, cid, vid, monto, tiempo) values("'.$tid.'", "'.$_POST['cliente'].'", "'.$_SESSION['tid'].'", "'.$_POST['monto'].'", "'.time().'");')){
					if($trid = $ssl->encriptar($trid)){
						redir('qr_confirmacion.php', array('trid'=>$trid));
					}
					else{
						redir('index.php', array('msj'=>'La transacción no se pudo finalizar.'));
					}
				}
				else{
					redir('index.php', array('msj'=>'La transacción no se pudo registrar.'));
				}
			}
			else{
				redir('index.php', array('msj'=>'El código no es correcto.'));
			}
		}
	}

?>
</body>
</html>
