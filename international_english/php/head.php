<?php
	if (session_status() == PHP_SESSION_NONE) { 
		session_start();
		if (!isset($_SESSION['lang'])){
			$_SESSION['lang'] = 'es';
		}
	}
	$lang = $_SESSION['lang'];
	
	require('sql.php');

	$sql = 'select stop from info';
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		$row = $result->fetch_assoc();
		if ($row['stop'] == 'si') {
			header('location:mantenimiento.php');
		}
	}

	require('info.php');
	$dir = $URL;
	$uri = $_SERVER['REQUEST_URI'];
	$url = $dir.$uri;
	echo'
<!DOCTYPE html>
<html lang="es" 
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
	<head>
		<!--Meta tagss-->
		<link rel="canonical" href="'.$url.'" />
		<meta charset="UTF-8"/>
		<meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
		<meta name="title" content="'.$titulo.'" />
		<meta name="description" content="'.$contenido.'" />
		<meta name="ROBOTS" content="INDEX,FOLLOW">
		<title>'.$titulo.'</title>

		<link rel="icon" href="'.$icono.'"/>
		<link rel="shortcut icon" href="'.$icono.'">
		<link rel="image_src" href="'.$URL.'/'.$icono.'" />

		<!--Open graph-->
		<meta property="og:title" content="'.$titulo.'"/>
		<meta property="og:type" content="website"/>
		<meta property="og:url" content="'.$url.'">
		<meta property="og:image" content="'.$URL.'/'.$icono.'"/>
		<meta property="og:description" content="'.$contenido.'"/>
		<meta property="fb:app_id" content="'.$facebookid.'"/>
		<!--Twitter cards-->
		<meta name="twitter:card" content="summary" />
		<meta name="twitter:title" content="'.$titulo.'">
		<meta name="twitter:description" content="'.$contenido.'">
		<meta name="twitter:image" content="'.$URL.'/'.$icono.'">
		<meta name="twitter:site" content="'.$twitter.'">
		<!--Pinterest-->
		<meta name="p:domain_verify" content="'.$pinterest.'"/>

		<!--Estilos-->
		<link rel="stylesheet" type="text/css" href="css/estilo.css"/>
		<link rel="stylesheet" type="text/css" href="css/slider.css"/>

		<!--JS-->
		<script src="js/pre.js"></script>
		<script src="https://code.jquery.com/jquery-2.2.3.min.js"></script>
	';
?>
		<script src="js/responsiveslides.min.js"></script>
		<script>
			// You can also use "$(window).load(function() {"
			$(function () {

				// Slideshow 1
				$("#slider1").responsiveSlides({
					auto: true,
					speed: 300,
					pager: true,
					nav: true,
					timeout: 4500,
					maxwidth: 7000,
					namespace: "large-btns"
				});
			});
		</script>
	</head>