<?php
//REQUIERE SQL.PHP

	if ($lang == 'es') {
		$m_slogan = $row['slogan_es'];
		$m_nosotros = 'Nosotros';
		$m_cursos = 'Cursos';
		$m_servicios = 'Servicios';
		$m_contacto = 'Contacto';
	}
	elseif ($lang =='en') {
		$m_slogan = $row['slogan_en'];
		$m_nosotros = 'About Us';
		$m_cursos = 'Courses';
		$m_servicios = 'Services';
		$m_contacto = 'contact us';
	}
	echo'				<nav class="">
							<ul class="row menu vidrio" id="menu-resp">
								<div class="centro">
									<li class="nav-item">
										<a class="nav-link" href="nosotros.php" >
											'.$m_nosotros.'
										</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="cursos.php" >
										'.$m_cursos.'
										</a>
										<ul>';
		$sql = 'select * from cursos';
	//	echo $sql;
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()){
				if ($lang == 'es') {
					echo'					<li>
												<a class="nav-link" href="cursos.php?i='.$row['id'].'" >
													'.$row['tit_es'].'
												</a>
											</li>';
				}
				elseif ($lang =='en') {
					echo'					<li>
												<a class="nav-link" href="cursos.php?i='.$row['id'].'" >
													'.$row['tit_en'].'
												</a>
											</li>';
				}
			}
		}
		echo '							</ul>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="servicios.php">
											'.$m_servicios.'
										</a>
										<ul>';
		$sql = 'select * from servicios';
	//	echo $sql;
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()){
				if ($lang == 'es') {
					echo'					<li>
												<a class="nav-link" href="servicios.php?i='.$row['id'].'" >
													'.$row['tit_es'].'
												</a>
											</li>';
				}
				elseif ($lang =='en') {
					echo'					<li>
												<a class="nav-link" href="servicios.php?i='.$row['id'].'" >
													'.$row['tit_en'].'
												</a>
											</li>';
				}
			}
		}
		echo'							</ul>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="contacto.php" >
										'.$m_contacto.'
										</a>
									</li>

									<li class="nav-item">
										<a target="_blank" href="'.$facebook_sitio.'">
											<img class="icono1" src="img/web/ico_fb.svg">
										</a>
									</li>
									<li class="nav-item">
											<a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.internationalenglish.org.pe%2F&amp;src=sdkpreparse">
												<img class="icono1" src="img/web/compartir.svg">
											</a>
										
									</li>
								</div>
							</ul>
						</nav>
		';
?>
