<?php
	echo '			</section>
					<div class="pre-footer"></div>
					<footer id="footer">
						<div id="footer-nav" class="content-block">
							<nav id="footer-nav-links">
	';
	echo '						<a class="link" href="nosotros.php">
	';
	if ($lang == 'es') { echo '			Nosotros'; }
	elseif ($lang == 'en') { echo '		About us'; }
	echo '
								</a>
								<a class="link" href="cursos.php">
	';
	if ($lang == 'es') { echo '			Cursos'; }
	elseif ($lang == 'en') { echo '		Courses'; }
	echo '
								</a>
								<a class="link" href="servicios.php">
	';
	if ($lang == 'es') { echo '			Servicios'; }
	elseif ($lang == 'en') { echo '		Services'; }
	echo '
								</a>
								<a class="link" href="contacto.php">
	';
	if ($lang == 'es') { echo '			Contacto'; }
	elseif ($lang == 'en') { echo '		Contact us'; }
	echo '
								</a>
								<a class="link" href="work.php?i=1">
									Work With Us
								</a>
								<a class="link" href="work.php?i=2">
									Total Rewards Program
								</a>
	';
	echo '					</nav>';
	if (!is_null($facebook_sitio) && $facebook_sitio != "") {
		echo '				
							<div id="footer-shares">
								<a class="fts-label" class="link" href="'.$facebook_sitio.'" target="_blank">
									<span  class="icon-facebook se-icon">
										<span  class="icon-facebook sei-icon"></span>
									</span>
								</a>
							</div>
		';
	}
	echo '					<div class="footer-copy" style="font-size: 1rem">';
	if (!is_null($telefono1) && $telefono1 != "") {
		echo '					<a class="link" href="tel:'.$telefono1.'" style="margin: 1rem;">
									<span class="icon-fono">
										<img src="img/web/fono.svg" style="height:1.5rem; width:1.5rem;">
									</span>
									<span class="phone">'.$telefono1.'</span>
								</a>
		';
	}
	if (!is_null($telefono2) && $telefono2 != "") {
		echo '					<a class="link" href="tel:'.$telefono2.'" style="margin: 1rem;">
									<span class="icon-fono">
										<img src="img/web/fono.svg" style="height:1.5rem; width:1.5rem;">
									</span>
									<span class="phone">'.$telefono2.'</span>
								</a>
		';
	}
	if (!is_null($telefono3) && $telefono3 != "") {
		echo '					<a class="link" href="tel:'.$telefono3.'" style="margin: 1rem;">
									<span class="icon-fono">
										<img src="img/web/fono.svg" style="height:1.5rem; width:1.5rem;">
									</span>
									<span class="phone">'.$telefono3.'</span>
								</a>
		';
	}
	if (!is_null($email) && $email != "") {
		echo '					<a class="link" href="mailto:'.$email.'" style="margin: 1rem;">
									<span class="icon-mail">
										<img src="img/web/msn.svg" style="height:1.5rem; width:1.5rem;">
									</span>
									<span>'.$email.'</span>
								</a>
		';
	}
	echo '					</div>
						</div>
						<div style="padding:0.3rem 0.3rem 0.2rem;">
							<p style="font-size: 0.5rem; color: white;">Hecho por <a href="https://www.facebook.com/sincorbatamkt/" target="_blank" style="color: #C85DA5;">Sin Corbata Marketing</a> 2016</p>
						</div>
					</footer>
				</div>
			</body>
		</html> 
	';

	if(isset($conn)){
			$conn -> close()
			unset($conn);
	}
?>