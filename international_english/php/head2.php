<?php
	if (session_status() == PHP_SESSION_NONE) { 
		session_start();
		if (!isset($_SESSION['pase'])){
			$_SESSION['pase'] = '0';
		}
	}
	$pase = $_SESSION['pase'];
	require('sql.php');
	require('../info.php');
	$dir = $URL;
	$uri = $_SERVER['REQUEST_URI'];
	$url = $dir.$uri;
	echo'
<!DOCTYPE html>
	<head>
		<!--Meta tagss-->
		<link rel="canonical" href="'.$url.'" />
		<meta charset="UTF-8"/>
		<meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
		<meta name="title" content="'.$titulo.'" />
		<meta name="description" content="'.$contenido.'" />
		<meta name="ROBOTS" content="INDEX,FOLLOW">
		<title>'.$titulo.'</title>

		<link rel="icon" href="../'.$icono.'"/>
		<link rel="shortcut icon" href="../'.$icono.'">

		<!--Estilos-->
		<link rel="stylesheet" type="text/css" href="../css/estilo1.css"/>
	</head>
	<body>
	';
	require('popup.php');
?>