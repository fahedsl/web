<?php
	require('check.php');
	require('../php/sql.php');
	require('func.php');
?>
	<div style="display:flex; justify-content: space-between;">
		<h1 style="display: inline-flex;">
		Imágenes del slider
		</h1>
		<a style="display: inline-flex;" href="index.php">Volver</a>
	</div>
<?php
	if(!isset($_GET['slide'])){
		$sql = 'select * from slider';
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()){
				echo'
					<a href="slider.php?slide='.$row['id'].'">
						<div>
							<h2>Slide '.$row['id'].'</h2>
							<h4>URL: '.$row['url'].'</h4>
						</div>
						<div>
							<div>
								<img style="max-width:60%; max-height:20rem;" src="../'.$row['imgg'].'">
								<img style="max-width:20%; max-height:20rem;" src="../'.$row['imgc'].'">
							</div>
						</div>
					</a>';
			}
		}
		echo' 		
					<a href="slider.php?slide=slide">
						<div style="margin-top: 2rem;">
							<h2>
								Slide Nuevo
							</h2>
						</div>
					</a>';
	}
	elseif (isset($_GET['slide'])) {
		if ($_GET['slide'] != 'slide') {
			$sql = 'select * from slider where id = '.$_GET['slide'];

			$result = $conn->query($sql);
			if ($result->num_rows > 0) {
				$row = $result->fetch_assoc();
				//grande
				echo' <form style="height:100%; width:100%; color:white;" action="envio.php?fich=slide&cont='.$_GET['slide'].'&part=imgg&red='.$_SERVER['PHP_SELF'].'" method="POST" enctype="multipart/form-data">';
				echo' <h3>Imagen grande</h3>';
				echo' 	<div>
							<div>
								<img src="../'.$row['imgg'].'" style="margin:1rem; display: inline-block; max-width:60%; max-height: 10rem;">';
				echo'		</div>';
				echo'		<div style="margin: 1rem 1rem 0;"> ';
				archivo('imgg');
				echo'		</div>
						</div>';
				boton('Cambiar imagen');
				echo' </form>';
				//chica
				echo' <form style="height:100%; width:100%; color:white;" action="envio.php?fich=slide&cont='.$_GET['slide'].'&part=imgc&red='.$_SERVER['PHP_SELF'].'" method="POST" enctype="multipart/form-data">';
				echo' <h3>Imagen chica</h3>';
				echo' 	<div>
							<div>
								<img src="../'.$row['imgc'].'" style="margin:1rem; display: inline-block; max-width:20%; max-height: 10rem;">';
				echo'		</div>';
				echo'		<div style="margin: 1rem 1rem 0;"> ';
				archivo('imgc');
				echo'		</div>
						</div>';
				boton('Cambiar imagen');
				echo' </form>';
				//URL
				echo' <form style="height:100%; width: 100%; color:white;" action="envio.php?fich=slide&cont='.$_GET['slide'].'&part=url&red='.$_SERVER['PHP_SELF'].'" method="POST">';
				entrada('t', 'url', 'URL', $row['url']);
				boton('Editar');
				echo' 	</form>';

				echo' 	<form style="display:inline-block; background-color:#EC234A;" action="borrar.php?fich=slide&cont='.$_GET['slide'].'&red='.$_SERVER['PHP_SELF'].'" method="POST">';
				boton('ELIMINAR');
				echo' 	</form>';
			}
		}
		elseif($_GET['slide'] == 'slide'){
			echo' <form style="height:100%; width: 100%; color:white;" action="envio.php?fich=slide&cont=slide&red='.$_SERVER['PHP_SELF'].'" method="POST" enctype="multipart/form-data">';
			echo'	<h2>Nuevo Slide</h2>';
			echo' 	<div style="padding:1rem;">
						<h3>Imagen grande</h3>';
			archivo('imgg');
			echo'	</div>';
			echo' 	<div style="padding:1rem;">
						<h3>Imagen chica</h3>';
			archivo('imgc');
			echo'	</div>';
			entrada('t', 'url', 'URL', '');
			boton('Registrar');
			echo' 	</form>';

		}
	}

?>
</body>