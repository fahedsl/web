<?php
	function entrada($tipo, $nombre, $tit, $cont, $requerido= TRUE){
		if (isset($tipo) && isset($nombre)) {

			if ($tipo == 'n') {$tipo = 'number';}
			elseif ($tipo == 't') {$tipo = 'text';}
			elseif ($tipo == 'e') {$tipo = 'email';}
			elseif ($tipo == 'r') {$tipo = 'range';}
			elseif ($tipo == 'a') {$tipo = 'area';}

			if ($requerido == TRUE) {$req = 'required=""';}
			else{$req = '';}

			if (is_null($cont)) {$cont = '';}

			if ($tipo == 'area'){
				echo'
					<div>
						<h4 class="titulo">'.$tit.'</h4>
						<textarea class="entrada" style="min-height: 5rem; color:black;" id="'.$nombre.'" name="'.$nombre.'" rows="5" placeholder=" '.$tit.'" '.$req.'>'.$cont.'</textarea>
					</div>';
			}
			else{
				echo'
					<div>
						<h4 class="titulo">'.$tit.'</h4>
						<input class="entrada" id="'.$nombre.'" name="'.$nombre.'" type="'.$tipo.'" placeholder="'.$tit.'" value="'.$cont.'" '.$req.'>
					</div>';
			}
		}
	}

function archivo($nombre,$muestra = 1,$requerido = TRUE){
	if ($requerido == TRUE) {$requerido = 'required=""';}
	elseif($requerido == FALSE){$requerido = '';}
	echo '		<input type="file" name="'.$nombre.'"  id="'.$nombre.'" '.$requerido.'>
				<p id="muestra'.$muestra.'"></p>';
}

function boton($texto){
	echo'
		<div>
			<input class="boton" id="submit" type="submit" value="'.$texto.'">
		</div>';
}


////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

function img_val($origen, $destino, $tammax, $horiz, $vert, $calidad, $name){

	if(getimagesize($origen) == TRUE){
		$imgdat = getimagesize($origen);
		$tam = $_FILES[$name]['size'];
		$tipo = $imgdat['mime'];

		if(isset($tam) && !isset($redir)){
			if($tam > $tammax){
				$msj = 'La imagen excede el tamaño máximo ('.($tammax/1000000).'Mb).';
				$redir = 1;
			}
		}

		if(isset($tipo) && !isset($redir)){
			if($tipo != 'image/jpeg'){
				$msj = 'La imagen tiene un formato no válido (Solo se acepta .jpeg).';
				$redir = 1;
			}
		}

		if(!isset($redir)){
			$Thoriz = $imgdat['0'];
			$Tvert = $imgdat['1'];

			// Calculo de dimensiones finales
			if($Thoriz >= $Tvert){
				if($Thoriz > $horiz){
					$Tvert = ceil(($horiz * $Tvert) / $Thoriz);
					$Thoriz = $horiz;
				}
			}
			elseif($Tvert > $Thoriz){
				$msj = 'La imagen debe ser horizontal o cuadrada.';
				$redir = 1;
			}

			//Guarda la imagen JPEG en servidor (CAL:100%)
			if	($tipo == 'image/jpeg'){
				$img  = imagecreatefromjpeg($origen);
			}
			elseif	($tipo == 'image/png'){
				$img  = imagecreatefrompng($origen);
			}
			imagejpeg($img, $destino, 100);
			imagedestroy($img);

			// Redimension
			$imag1 = imagecreatefromjpeg($destino);
			$imag2 = imagecreatetruecolor($Thoriz, $Tvert);
			imagecopyresampled($imag2, $imag1, 0, 0, 0, 0, $Thoriz, $Tvert, $imgdat['0'], $imgdat['1']);
			imagejpeg($imag2, $destino, $calidad);
			imagedestroy($imag1);
			if(imagedestroy($imag2)){
				$msj = 'Subida exitosa';
			}
		}
	}
	elseif(!$imgdat){
		$msj = 'El archivo no es una imagen.';
		$redir = 1;
	}
	
	if(isset($msj)){return $msj;}
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function svg_val($origen, $destino, $tammax, $name){

		$imgdat =  mime_content_type($origen);
		$tam = $_FILES[$name]['size'];
		$tipo = $_FILES[$name]['type'];

		if(isset($tam) && !isset($redir)){
			if($tam > $tammax){
				$msj = 'La imagen excede el tamaño máximo ('.($tammax/1000).'Kb).';
				$redir = 1;
			}
		}

		if(isset($tipo) && !isset($redir)){
			if($tipo != 'image/svg+xml'){
				$msj = 'La imagen tiene un formato no válido (Se acepta solo .svg).';
				$redir = 1;
			}
		}

		if(!isset($redir)){
			if(move_uploaded_file($_FILES[$name]['tmp_name'],$destino)){
				$msj = 'Subido exitosamente.';
			}
		}

	if(isset($msj)){return $msj;}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function file_val($origen, $destino, $tammax, $name){

		$imgdat =  mime_content_type($origen);
		$tam = $_FILES[$name]['size'];

		if(isset($tam) && !isset($redir)){
			if($tam > $tammax){
				$msj = 'Error: Tamaño máximo ('.($tammax/1000).'Kb).';
				$redir = 1;
			}
		}

		if(!isset($redir)){
			if(move_uploaded_file($_FILES[$name]['tmp_name'],$destino)){
				$msj = 'Correcto.';
			}
		}

	if(isset($msj)){return $msj;}
}


?>