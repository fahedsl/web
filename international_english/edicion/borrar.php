<?php

	function borrar($sql, $con){
		if($con->query($sql)){
			$msj = 'Eliminado';
			if(isset($con)){$con->close();}
			$dir = $_GET['red'].'?msj='.$msj;
			header('location:'.$dir);
		}
	}


	require('check.php');
	require('../php/sql.php');
	require('func.php');
	$msj = 'Ha ocurrido un error.';
	if (isset($_GET['fich']) && isset($_GET['cont']) && isset($_GET['red'])) {

//SLIDER
		if ($_GET['fich'] == 'slide') {
			if ($_GET['cont'] != 'slide' && $_GET['cont'] >= 0) {
				$sql = 'select * from slider where id = '.$_GET['cont'];
				$result = $conn->query($sql);
				if ($result->num_rows > 0) {
					$row = $result->fetch_assoc();
					$destino1 = '../'.$row['imgg'];
					$destino2 = '../'.$row['imgc'];
					if(unlink($destino1) && unlink($destino2)){					
						$sql = 'delete from slider where id = '.$_GET['cont'];
						borrar($sql, $conn);
					}
				}
			}
		}

//CURSOS
		if ($_GET['fich'] == 'curso') {
			if ($_GET['cont'] != 'curso' && $_GET['cont'] >= 0) {
				$sql = 'delete from cursos where id = '.$_GET['cont'];
			}
		}

//SERVICIOS
		if ($_GET['fich'] == 'servicio') {
			if ($_GET['cont'] != 'servicio' && $_GET['cont'] >= 0) {
				$sql = 'delete from servicios where id = '.$_GET['cont'];
			}
			borrar($sql, $conn);
		}

//WORK
		if ($_GET['fich'] == 'work') {
			if ($_GET['cont'] != 'work' && $_GET['cont'] >= 0) {
				$sql = 'delete from w_faq where id = '.$_GET['cont'];
			}
			borrar($sql, $conn);
		}

//REWARDS
		if ($_GET['fich'] == 'reward') {
			if ($_GET['cont'] != 'reward' && $_GET['cont'] >= 0) {
				$sql = 'delete from w_rewards where id = '.$_GET['cont'];
			}
			borrar($sql, $conn);
		}

//PROFESORES
		if ($_GET['fich'] == 'profesores') {
			if ($_GET['cont'] != 'profesores' && $_GET['cont'] >= 0) {
				$sql = 'select foto from profesores where id = '.$_GET['cont'];
				$result = $conn->query($sql);
				if ($result->num_rows > 0) {
					$row = $result->fetch_assoc();
					$destino1 = '../'.$row['foto'];
					if(unlink($destino1)){					
						$sql = 'delete from profesores where id = '.$_GET['cont'];
						borrar($sql, $conn);
					}
				}
			}
		}

//INICIO
		if ($_GET['fich'] == 'inicio') {
			if ($_GET['cont'] != 'inicio' && $_GET['cont'] >= 0) {
				$sql = 'delete from inicio where id = '.$_GET['cont'];
			}
			borrar($sql, $conn);
		}
	}
?>