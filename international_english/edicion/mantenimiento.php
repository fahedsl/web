<?php
	require('../php/head2.php');
	require('../php/sql.php');
	require('func.php');
?>
	<div style="display:flex; justify-content: space-between;">
		<h1 style="display: inline-flex;">
			Mantenimiento
		</h1>
		<a style="display: inline-flex;" href="index.php">Volver</a>
	</div>
<?php
	
	$sql = 'select stop from info';
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		$row = $result->fetch_assoc();
		if ($row['stop'] == 'si') {
			$mensaje = 'La página está en mantenimiento.';
		}
		elseif ($row['stop'] == 'no' ) {
			$mensaje = 'La página está activa.';
		}
	}
	if (isset($mensaje)) {
	echo' <form style="height:100%; width: 100%; color:white;" action="envio.php?fich=mantenimiento&cont='.$row['stop'].'&red='.$_SERVER['PHP_SELF'].'" method="POST">';
	boton('Cambiar Estado');
	echo' 	</form>';
	}
	
?>
</body>