<?php
	require('check.php');
	require('../php/sql.php');
	require('func.php');
?>
	<div style="display:flex; justify-content: space-between;">
		<h1 style="display: inline-flex;">
		Cursos
		</h1>
		<a style="display: inline-flex;" href="index.php">Volver</a>
	</div>
<?php
	if(!isset($_GET['curso'])){
		$sql = 'select * from cursos';
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()){
				echo' 	
					<a href="curso.php?curso='.$row['id'].'">
						<div style="">
							<h2>'.$row['tit_es'].'</h2>
						</div>
						<div>
							<p style="margin:1rem;">'.$row['cont_es'].'</p>
							<p style="margin:1rem; font-weight:300; text-align:left;">'.$row['d1'].' '.$row['h1'].'</p>
						</div>
					</a>';
			}
		}
		echo' 		
					<a href="curso.php?curso=curso">
						<div style="margin-top: 2rem;">
								<h2>
									<a href ="curso.php?curso=curso">
										Curso Nuevo
									</a>
								</h2>
						</div>
					</a>';
	}
	elseif (isset($_GET['curso'])) {
		if ($_GET['curso'] != 'curso') {
			$sql = 'select * from cursos where id = '.$_GET['curso'];
			$result = $conn->query($sql);
			if ($result->num_rows > 0) {
				$row = $result->fetch_assoc();
				echo' <form style="height:100%; width: 100%; color:white;" action="envio.php?fich=curso&cont='.$_GET['curso'].'&red='.$_SERVER['PHP_SELF'].'" method="POST">';
				echo'	<h2>'.$row['tit_es'].'</h2>';
				entrada('t', 'tit_es', 'Título en español', $row['tit_es']);
				entrada('t', 'tit_en', 'Título en inglés', $row['tit_en']);
				entrada('a', 'cont_es', 'Contenido en español', $row['cont_es']);
				entrada('a', 'cont_en', 'Contenido en inglés', $row['cont_en']);
				entrada('t', 'd1', 'Dias en español', $row['d1']);
				entrada('t', 'h1', 'Horas en español', $row['h1']);
				entrada('t', 'd2', 'Dias en inglés', $row['d2']);
				entrada('t', 'h2', 'Horas en inglés', $row['h2']);
				boton('Editar');
				echo' 	</form>';

				echo' 	<form style="display:inline-block; background-color:#EC234A;" action="borrar.php?fich=curso&cont='.$_GET['curso'].'&red='.$_SERVER['PHP_SELF'].'" method="POST">';
				boton('ELIMINAR');
				echo' 	</form>';
			}
		}
		elseif($_GET['curso'] == 'curso'){
			echo' <form style="height:100%; width: 100%; color:white;" action="envio.php?fich=curso&cont=curso&red='.$_SERVER['PHP_SELF'].'" method="POST">';
			echo'	<h2>Curso Nuevo</h2>';
			entrada('t', 'tit_es', 'Título en español', '');
			entrada('t', 'tit_en', 'Título en inglés', '');
			entrada('a', 'cont_es', 'Contenido en español', '');
			entrada('a', 'cont_en', 'Contenido en inglés', '');
			entrada('t', 'd1', 'Dias en español', '');
			entrada('t', 'h1', 'Horas en español', '');
			entrada('t', 'd2', 'Dias en inglés', '');
			entrada('t', 'h2', 'Horas en inglés', '');
			boton('Crear');
			echo' 	</form>';

		}
	}

?>
</body>