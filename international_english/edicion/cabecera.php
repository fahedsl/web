<?php
	require('check.php');
	require('../php/sql.php');
	require('func.php');
?>
	<div style="display:flex; justify-content: space-between;">
		<h1 style="display: inline-flex;">
		Cabecera de página
		</h1>
		<a style="display: inline-flex;" href="index.php">Volver</a>
	</div>

<?php
	$sql = 'select * from cont';
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		$row = $result->fetch_assoc();
	}
	//SLOGAN
	echo'<h2>Slogan</h2>';
	echo' <form style="height:100%; width: 100%; color:white;" action="envio.php?fich=cabecera&cont=slogan&red='.$_SERVER['PHP_SELF'].'" method="POST">';
	entrada('t', 'slogan_es', 'Slogan en español', $row['slogan_es']);
	entrada('t', 'slogan_en', 'Slogan en inglés', $row['slogan_en']);
	boton('Editar');
	echo' </form>';

	//IMAGEN
	echo'<h2>Imagen 1</h2>
			<div>
				<p>Solo formato SVG.</p>
				<img src="../img/web/fondo1.svg" style="background-color: rgba(0,0,0,0.15); margin:1rem; display: inline-block; max-width:29.9%;">
				<p style="margin:1rem; display: inline-block;max-width:60%;">'.$row['texto_es'].'</p>
			</div>';
	echo' 	<form style="height:100%; width: 100%; color:white;" action="envio.php?fich=cabecera&cont=img&red='.$_SERVER['PHP_SELF'].'" method="POST" enctype="multipart/form-data">';
	archivo('imagen1',1);
	boton('Subir Imagen');
	echo' </form>';

	//LOGO
	echo'<h2>Logo</h2>
			<div>
				<p>Solo formato SVG.</p>
				<img src="../img/web/ie1.svg" style="background-color: rgba(0,0,0,0.15); margin:1rem; display: inline-block; max-width:29.9%;">
				<p style="margin:1rem; display: inline-block;max-width:60%;">'.$row['texto_es'].'</p>
			</div>';
	echo' 	<form style="height:100%; width: 100%; color:white;" action="envio.php?fich=cabecera&cont=logo&red='.$_SERVER['PHP_SELF'].'" method="POST" enctype="multipart/form-data">';
	archivo('logo', 2);
	boton('Subir Imagen');
	echo' </form>';

	if(isset($conn)){
		$conn -> close();
	}
?>
</body>