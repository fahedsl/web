<?php
	require('check.php');
	require('func.php');
?>
	<div style="display:flex; justify-content: space-between;">
		<h1 style="display: inline-flex;">
			Información de página
		</h1>
		<a style="display: inline-flex;" href="index.php">Volver</a>
	</div>
	<p>Modificar con cuidado. Esta es información importante de la página, que puede afectar su visibilidad en redes sociales y buscadores.</p>


<?php

	$sql = 'select * from info';
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		$row = $result->fetch_assoc();
	}

	echo' <form style="height:100%; width: 100%; color:white;" action="envio.php?fich=info&cont=info&red='.$_SERVER['PHP_SELF'].'" method="POST">';

	echo'<h2>Información de contacto</h2>';
	entrada('t', 'telefono1', 'Teléfono 1', $row['telefono1'], FALSE);
	entrada('t', 'telefono2', 'Teléfono 2', $row['telefono2'], FALSE);
	entrada('t', 'telefono3', 'Teléfono 3', $row['telefono3'], FALSE);
	entrada('t', 'email', 'E-mail', $row['email'], FALSE);
	echo'<h2>Informacion para pestañas y buscadores </h2>';
	entrada('t', 'URL', 'URL canónica', $row['URL']);
	entrada('t', 'titulo', 'Título de página', $row['titulo']);
	entrada('t', 'contenido', 'Descripción para buscadores', $row['contenido']);
	echo'<h2>Redes Sociales</h2>';
	entrada('t', 'facebook_sitio', 'Sitio Oficial en Facebook', $row['facebook_sitio']);
	entrada('t', 'facebookid', 'ID de aplicacion de Facebook (en caso de tener)', $row['facebookid'], FALSE);
	entrada('t', 'twitter', 'ID de aplicacion de Twitter (en caso de tener)', $row['twitter'], FALSE);
	entrada('t', 'pinterest', 'ID de aplicacion de Pinterest (en caso de tener)', $row['pinterest'], FALSE);
	echo'<input style="display:none" id="envio" name="envio" type="text" required="" value="info">';
	boton('Editar');

	echo' </form>';

	if(isset($conn)){
		$conn -> close();
	}
?>
</body>