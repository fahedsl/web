<?php
	require('check.php');
	require('../php/sql.php');
	require('func.php');
?>
	<div style="display:flex; justify-content: space-between;">
		<h1 style="display: inline-flex;">
		Profesores
		</h1>
		<a style="display: inline-flex;" href="index.php">Volver</a>
	</div>
<?php
	if(!isset($_GET['profesores'])){
		$sql = 'select * from profesores';
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()){
				echo'
					<a href="nosotros.php?profesores='.$row['id'].'">
						<div style="">
							<h2>'.$row['nombre'].' ('.$row['pais'].', '.$row['edad'].')</h2>
						</div>
						<div>
							<img src="../'.$row['foto'].'" style="margin:1rem; display: inline-block; max-width:29.9%;">
							<p style="margin:1rem; display: inline-block;max-width:60%;">'.$row['texto_es'].'</p>
						</div>
					</a>';
			}
		}
		echo' 		
					<a href="nosotros.php?profesores=profesores">
						<div style="margin-top: 2rem;">
							<h2>
								Profesor Nuevo
							</h2>
						</div>
					</a>';
	}
	elseif (isset($_GET['profesores'])) {
		if ($_GET['profesores'] != 'profesores') {
			$sql = 'select * from profesores where id = '.$_GET['profesores'];
			$result = $conn->query($sql);
			if ($result->num_rows > 0) {
				$row = $result->fetch_assoc();
				echo' <form style="height:100%; width: 100%; color:white;" action="envio.php?fich=profesores&cont='.$_GET['profesores'].'&part=foto&red='.$_SERVER['PHP_SELF'].'" method="POST" enctype="multipart/form-data">';
				echo'	<h2>'.$row['nombre'].'</h2>';
				echo' 	<div>
							<div>
								<img src="../'.$row['foto'].'" style="margin:1rem; display: inline-block; max-width:20%;">';
				echo'		</div>';
				echo'		<div style="margin: 1rem 1rem 0;"> ';
				archivo('foto',1);
				echo'		</div>
						</div>';
				boton('Cambiar imagen');
				echo' </form>';
				echo' <form style="height:100%; width: 100%; color:white;" action="envio.php?fich=profesores&cont='.$_GET['profesores'].'&part=info&red='.$_SERVER['PHP_SELF'].'" method="POST">';
				entrada('t', 'nombre', 'Nombre', $row['nombre']);
				entrada('n', 'edad', 'Edad', $row['edad']);
				echo'	<div>
							<h3 style="margin:1rem 1rem 0;">País</h3>
							<select class="entrada" id="pais" name="pais">';
				$sql1 = 'select * from pais';
				$result1 = $conn->query($sql1);
				if ($result1->num_rows > 0) {
					while($row1 = $result1->fetch_assoc()){
						if ($row['pais'] == $row1['pais_en']) {$selected = 'selected=""';}
						else{$selected = '';}
						echo'	<option value="'.$row1['pais_en'].'" '.$selected.'>'.$row1['pais_en'].'</option>';
					}
				}
				echo'		</select>
						</div>';
				entrada('a', 'texto_es', 'Descripción en español', $row['texto_es']);
				entrada('a', 'texto_en', 'Descripción en inglés', $row['texto_en']);
				boton('Editar');
				echo' 	</form>';

				echo' 	<form style="display:inline-block; background-color:#EC234A;" action="borrar.php?fich=profesores&cont='.$_GET['profesores'].'&red='.$_SERVER['PHP_SELF'].'" method="POST">';
				boton('ELIMINAR');
				echo' 	</form>';
			}
		}
		elseif($_GET['profesores'] == 'profesores'){
			echo' <form style="height:100%; width: 100%; color:white;" action="envio.php?fich=profesores&cont=profesores&red='.$_SERVER['PHP_SELF'].'" method="POST" enctype="multipart/form-data">';
			echo'	<h2>Nuevo Profesor</h2>';
			echo' 	<div style="padding:1rem;">
						<h3>Fotografía</h3>';
			archivo('foto');
			echo'	</div>';
			entrada('t', 'nombre', 'Nombre', '');
			entrada('n', 'edad', 'Edad', '');
			echo'	<div>
						<h3 style="margin:1rem 1rem 0;">País</h3>
						<select class="entrada" id="pais" name="pais">';
			$sql = 'select * from pais';
			$result = $conn->query($sql);
			if ($result->num_rows > 0) {
				while($row = $result->fetch_assoc()){
					echo'	<option value="'.$row['pais_en'].'">'.$row['pais_en'].'</option>';
				}
			}
			echo'		</select>
					</div>'; 
			entrada('a', 'texto_es', 'Descripción en español', '');
			entrada('a', 'texto_en', 'Descripción en inglés', '');
			boton('Registrar');
			echo' 	</form>';

		}
	}

?>
</body>