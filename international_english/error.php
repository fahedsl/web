<?php
	if (session_status() == PHP_SESSION_NONE) { 
		session_start();
		if (!isset($_SESSION['lang'])){
			$_SESSION['lang'] = 'es';
		}
	}
	$lang = $_SESSION['lang'];

	require('php/sql.php');
	require('info.php');

	echo'
	<html>
		<head>
			<!--Meta tagss-->
			<link rel="canonical" href="'.$url.'" />
			<meta charset="UTF-8"/>
			<meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
			<meta name="title" content="'.$titulo.'" />
			<meta name="description" content="'.$contenido.'" />
			<meta name="ROBOTS" content="INDEX,FOLLOW">
			<title>'.$titulo.'</title>

			<link rel="icon" href="'.$icono.'"/>
			<link rel="shortcut icon" href="'.$icono.'">
			<link rel="image_src" href="'.$URL.'/'.$icono.'" />

			<!--Open graph-->
			<meta property="og:title" content="'.$titulo.'"/>
			<meta property="og:type" content="website"/>
			<meta property="og:url" content="'.$url.'">
			<meta property="og:image" content="'.$URL.'/'.$icono.'"/>
			<meta property="og:description" content="'.$contenido.'"/>
			<meta property="fb:app_id" content="'.$facebookid.'"/>
			<!--Twitter cards-->
			<meta name="twitter:card" content="summary" />
			<meta name="twitter:title" content="'.$titulo.'">
			<meta name="twitter:description" content="'.$contenido.'">
			<meta name="twitter:image" content="'.$URL.'/'.$icono.'">
			<meta name="twitter:site" content="'.$twitter.'">
			<!--Pinterest-->
			<meta name="p:domain_verify" content="'.$pinterest.'"/>
			<!--Estilos-->
			<link rel="stylesheet" type="text/css" href="css/estilo.css"/>
		</head>

		<body style="margin: 0; padding:0;">
			<div style="width: 100%; height:100%; background-color: #0152A4; margin: 0;">
					<section style="min-height:80%; overflow:auto;display:flex; justify-content:space-around;">
						<div style="overflow:auto;display:flex; justify-content:space-around;min-height:15rem; bottom:0;">
							<div class="small-12 medium-12 column fondo2">
							<div style="">
									<h1 style="text-align:center; margin:2.8rem; padding:0.2rem; font-weigth:bolder; background-color:rgba(0,0,0,0.35); color:white;">';
	if ($lang == 'es') { echo '			La página que buscas no existe'; }
	elseif ($lang == 'en') { echo '		The page you are looking for doesn\'t exist'; }
	else{echo 'algo mas';}
	echo'							</h1>
								</div>
							</div>
						</div>';
	echo '			</section>
					<div class="pre-footer"></div>
					<footer id="footer">
						<div id="footer-nav" class="content-block">
							<nav id="footer-nav-links">
	';
	echo '						<a class="link" href="nosotros.php">
	';
	if ($lang == 'es') { echo '			Nosotros'; }
	elseif ($lang == 'en') { echo '		About us'; }
	echo '
								</a>
								<a class="link" href="cursos.php">
	';
	if ($lang == 'es') { echo '			Cursos'; }
	elseif ($lang == 'en') { echo '		Courses'; }
	echo '
								</a>
								<a class="link" href="servicios.php">
	';
	if ($lang == 'es') { echo '			Servicios'; }
	elseif ($lang == 'en') { echo '		Services'; }
	echo '
								</a>
								<a class="link" href="contacto.php">
	';
	if ($lang == 'es') { echo '			Contacto'; }
	elseif ($lang == 'en') { echo '		Contact us'; }
	echo '
								</a>
								<a class="link" href="work.php?i=1">
									Work With Us
								</a>
								<a class="link" href="work.php?i=2">
									Total Rewards Program
								</a>
	';
	echo '					</nav>';
	if (!is_null($telefono1) && $telefono1!= 0 && $telefono1 != "") {
		echo '				
							<div id="footer-shares">
								<a class="fts-label" class="link" href="'.$facebook_sitio.'" target="_blank">
									<span  class="icon-facebook se-icon">
										<span  class="icon-facebook sei-icon"></span>
									</span>
								</a>
							</div>
		';
	}
	echo '					<div class="footer-copy" style="font-size: 1rem">';
	if (!is_null($telefono1) && $telefono1!= 0 && $telefono1 != "") {
		echo '					<a class="link" href="tel:'.$telefono1.'" style="margin: 1rem;">
									<span class="icon-fono">
										<img src="img/web/fono.svg" style="height:1.5rem; width:1.5rem;">
									</span>
									<span class="phone">'.$telefono1.'</span>
								</a>
		';
	}
	if (!is_null($telefono2) && $telefono2!= 0 && $telefono2 != "") {
		echo '					<a class="link" href="tel:'.$telefono2.'" style="margin: 1rem;">
									<span class="icon-fono">
										<img src="img/web/fono.svg" style="height:1.5rem; width:1.5rem;">
									</span>
									<span class="phone">'.$telefono2.'</span>
								</a>
		';
	}
	if (true) {
		echo '					<a class="link" href="mailto:'.$email.'" style="margin: 1rem;">
									<span class="icon-mail">
										<img src="img/web/msn.svg" style="height:1.5rem; width:1.5rem;">
									</span>
									<span>'.$email.'</span>
								</a>
		';
	}
	echo '					</div>
						</div>
					</footer>
				</div>
			</body>
		</html> 
	';

	if(isset($conn)){
			$conn -> close();
	}
?>