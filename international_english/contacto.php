<?php
	require('php/headers.php');

	require('php/cargador.php');
?>

		<div class="marco">

<?php
		require('php/menu.php');
?>

			<div class="contenido">
<?php
	// pop up
	if(isset($_GET['msj'])){
		echo '	<div class="pop">
					<div class="popcont">'.$_GET['msj'].'</div>
				</div>';
	}
	echo'
		<section class="pagina">

			<div style="background-color: #0152A4">
				<form style="height:100%; width: 100%; color:white;" id="contacto" action="envio.php?fich=contacto&red='.$_SERVER['PHP_SELF'].'" method="POST">
					<div class="centrar">
						<div>
							<h6>CONTÁCTANOS</h6>
							<div>';

	//Iconos de contacto
	if (isset($telefono1) && !is_null($telefono1) && $telefono1!= 0 && $telefono1 != "") {
		echo '					<a class="link" href="tel:'.$telefono1.'" style="margin: 1rem;">
									<span class="icon-fono">
										<img src="img/web/fono.svg" style="height:1.5rem; width:1.5rem;">
									</span>
									<span class="phone">'.$telefono1.'</span>
								</a>';
	}
	if (isset($telefono2) && !is_null($telefono2) && $telefono2!= 0 && $telefono2 != "") {
		echo '					<a class="link" href="tel:'.$telefono2.'" style="margin: 1rem;">
									<span class="icon-fono">
										<img src="img/web/fono.svg" style="height:1.5rem; width:1.5rem;">
									</span>
									<span class="phone">'.$telefono2.'</span>
								</a>';
	}
	if (isset($telefono3) && !is_null($telefono3) && $telefono3!= 0 && $telefono3 != "") {
		echo '					<a class="link" href="tel:'.$telefono3.'" style="margin: 1rem;">
									<span class="icon-fono">
										<img src="img/web/fono.svg" style="height:1.5rem; width:1.5rem;">
									</span>
									<span class="phone">'.$telefono3.'</span>
								</a>';
	}
	if (isset($email) && !is_null($email) && $email != "") {
		echo '					<a class="link" href="mailto:'.$email.'" style="margin: 1rem;">
									<span class="icon-mail">
										<img src="img/web/msn.svg" style="height:1.5rem; width:1.5rem;">
									</span>
									<span>'.$email.'</span>
								</a>';
	}
?>
							</div>
						</div>
<?php

	//Formulario de contacto
	if ($lang == 'es') {
		$c_nombre = 'Tu nombre';
		$c_email = 'Tu correo electrónico';
		$c_mensaje = 'Lo que quieras decirnos';
		$c_boton = 'Enviar';
	}
	elseif ($lang == 'en') {
		$c_nombre = 'Your name';
		$c_email = 'Your email';
		$c_mensaje = 'Whatever you want to tell us';
		$c_boton = 'Send';
	}
	echo '
						<div class="">
							<input class="entrada" id="nombre" type="text" name="nombre" required="" placeholder="'.$c_nombre.'"/>
						</div>
						<div>
							<input class="entrada" id="email" type="email" name="email" required="" placeholder="'.$c_email.'"/>
						</div>
						<div>
							<textarea class="entrada" style="min-height: 5rem;" id="msj" name="msj" rows="5" required="" placeholder="'.$c_mensaje.'"></textarea>
						</div>
						<div>
							<input class="boton" id="submit" type="submit" value="'.$c_boton.'" style="width:auto; margin:2em;"/>
						</div>';
?>
					</div>
				</form>
			</div>
			<section>
			<div id="mapjam-1"></div><script type="text/javascript">window["Mapjam"] = window["Mapjam"] || [];window["MapJamHost"] ="//embeds.mapjam.com/v2/";window.Mapjam.push({app_url: "https://mapjam.com/",cdn_url: "//mapjamjson.global.ssl.fastly.net/",map_id: "internationalenglish",content_action:"side_popup",access_token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI1ODIyMzVkODIxMGNiZjM2MDA5ZmQ2YjAiLCJpc3MiOiJtYXBqYW0uY29tIn0.fMbesQsaVqAG8rGFUBRBVT0vH9lZDOxH_3DJJPo6VXo",map_width: "100%",map_height: "500px",container: "mapjam-1",map_lat_lng: [-16.402175,-71.548269],zoom:16});(function() {var mpjm = document.createElement("script");mpjm.type = "text/javascript";mpjm.async = true;mpjm.src = window["MapJamHost"] + "mapjam-iframe.min.js";(document.getElementsByTagName("head")[0] || document.getElementsByTagName("body")[0]).appendChild(mpjm);})();</script>
			</section>
		</section>

		<!--mapa-->
<?php
	require('php/pie.php');
?>