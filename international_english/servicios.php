<?php
	require('php/cabecera.php');

	if(!isset($_GET['i'])){
		echo '
					<div class="tag">';
		if ($lang == 'es') {
			echo '
						<h2>Servicios</h2>';
		}
		elseif ($lang == 'en') {
			echo '
						<h2>Services</h2>';
		}
		echo '
					</div>
					<div class="info">';

		$sql = 'select * from servicios';
	//	echo $sql;
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()){
				if ($lang == 'es') {$tit = $row['tit_es']; $cont = $row['cont_es']; $hora = $row['h1'];}
				elseif ($lang == 'en') {$tit = $row['tit_en']; $cont = $row['cont_en']; $hora = $row['h2'];}
				echo'	<div>
							<div style="margin:0">
								<a href="servicios.php?i='.$row['id'].'">
									<div class="tag2">
										<h3>'.$tit.'</h3>
									</div>
								</a>
							</div>
							<div style="margin:0 1rem 1rem;">
								<p>'.$cont.'</p>
							</div>
						</div>';
			}
		}
		echo'		</div>';
	}

	if(isset($_GET['i'])){
		$sql = 'select * from servicios where id ='.$_GET['i'];
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			$row = $result->fetch_assoc();
			echo '
						<div class="tag">';
			if ($lang == 'es') {
				echo '		<h2>'.$row['tit_es'].'</h2>';
			}
			elseif ($lang == 'en') {
				echo '		<h2>'.$row['tit_en'].'</h2>';
			}
			echo '
						</div>
						<div class="info">';

			if ($lang == 'es') {$cont = $row['cont_es']; $hora = $row['h1'];}
			elseif ($lang == 'en') {$cont = $row['cont_en']; $hora = $row['h2'];}
			echo'			<div>
								<p style="margin:0;">'.$cont.'</p>
								<p style="margin:1rem; font-weight:300; text-align:left;">'.$hora.'</p>
								<div style="margin:1rem">
									<h3 style="text-align:center">¡Me interesa, contáctenme!</h3>
									<form action="envio.php?fich=servicio&i='.$_GET['i'].'&red='.$_SERVER['PHP_SELF'].'" method="post">
										<input class="entrada small-12 medium-6 columns" style="display:inline-block;" type="email" name="email" placeholder="e-mail" required=""><br>
										<input style="display:none;" type="number" name="numero" value="'.$_GET['i'].'" required="">
										<input class="boton small-12 medium-5 columns" style="display:inline-block;" type="submit" value="Quiero saber más">
	  								</form>
								</div>
							</div>';
			echo'		</div>';
		}
	}

	require('php/pie.php');
?>