<?php
	require('php/sql.php');
	
	$sql = 'select stop from info';
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		$row = $result->fetch_assoc();
		if ($row['stop'] == 'no') {
			header('location:index.php');
		}
	}

	if (session_status() == PHP_SESSION_NONE) { 
		session_start();
		if (!isset($_SESSION['lang'])){
			$_SESSION['lang'] = 'es';
		}
	}
	$lang = $_SESSION['lang'];

	require('info.php');

	if ($lang == 'es') {$mensaje = 'Nuestra página está en mantenimiento,<br> pero... ¡Vuelve pronto!. :)';}
	if ($lang == 'en') {$mensaje = 'Our website is under manteinance,<br> but... Come back soon!. :)';}

	echo'
	<html>
		<head>
			<!--Meta tagss-->
			<link rel="canonical" href="'.$url.'" />
			<meta charset="UTF-8"/>
			<meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
			<meta name="title" content="'.$titulo.'" />
			<meta name="description" content="'.$contenido.'" />
			<meta name="ROBOTS" content="INDEX,FOLLOW">
			<title>'.$titulo.'</title>

			<link rel="icon" href="'.$icono.'"/>
			<link rel="shortcut icon" href="'.$icono.'">
			<link rel="image_src" href="'.$URL.'/'.$icono.'" />
			<link rel="stylesheet" type="text/css" href="css/estilo1.css"/>
		</head>

		<body style="display:flex; justify-content:space-around; width: 100%; height:100%; margin: 0; padding:0; background-color: #0152A4;">
			<div style="margin: 0;">
				<div style="display:block; width:100%;">
					<h1 style="color:white; text-align:center;">'.$mensaje.'</h1>
				</div>	
				<div style="display:flex; justify-content:space-around; width:auto; margin:auto;	">
					<img style="margin:3rem; width:15rem; height:15rem;" src="img/web/ie2.svg">
				</div>
			</div>
		</body>';

	if(isset($conn)){
			$conn -> close();
	}