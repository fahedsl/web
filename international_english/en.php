<?php
	if (session_status() == PHP_SESSION_NONE) { 
		session_start();
		if (!isset($_SESSION['lang']) || $_SESSION['lang'] == 'es'){
			$_SESSION['lang'] = 'en';
		}
	}
	header('location:'.$_SERVER['HTTP_REFERER']);
?>