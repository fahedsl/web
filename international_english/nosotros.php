<?php
	require('php/cabecera.php');

	//Nosotros
	if ($lang == 'es') {
		$tag1 = 'Nosotros';
		$profes = 'Nuestros Profesores';
	}
	elseif ($lang == 'en') {
		$tag1 = 'About Us';
		$profes = 'Our Teachers';
	}
	echo '		<div class="tag">
					<h2>'.$tag1.'</h2>
				</div>
				<div class="info">
					<p>';
	$sql = 'select nosotros_es,nosotros_en from cont where id = 1';
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()){
			if ($lang == 'es') {
				echo 	$row['nosotros_es'];
			}
			elseif ($lang == 'en') {
				echo 	$row['nosotros_en'];
			}
		}
	}
	echo'			</p>
				</div>
	';

	//Staff
	echo '
				<div class="tag">
					<h2>'.$profes.'</h2>
				</div>
				<div class="info">
					<p>';
					
	$sql = 'select staff_es, staff_en from cont where id = 1';
//	echo $sql;
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()){
			if ($lang == 'es') {
				echo 	$row['staff_es'];
			}
			elseif ($lang == 'en') {
				echo 	$row['staff_en'];
			}
		}
	}
	echo'			</p>
	';
	$sql = 'select * from profesores';
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()){
			$sql1 = 'select bandera from pais where pais_en = "'.$row['pais'].'"';
		//	echo $sql;
			$result1 = $conn->query($sql1);
			if ($result1->num_rows > 0) {
				$row1 = $result1->fetch_assoc();
			}
			echo '
					<div class="small-12 columns" style="margin:0.5rem 0;">
						<div class="profe small-12 medium-5 large-3 columns">
							<div>
								<img class="profe-foto" id="profe-foto" src="'.$row['foto'].'">
								<img class="profe-band" id="profe-foto" src="'.$row1['bandera'].'">
							</div>
						</div>
						<div class="small-12 medium-7 large-9 columns">
							<h3 style="text-align:center; text-transform:capitalize;">'.$row['nombre'].'</h3>';
			if ($lang == 'es') {
				echo '
							<p>'.$row['texto_es'].'</p>';
			}
			elseif ($lang == 'en') {
				echo '
							<p>'.$row['texto_en'].'</p>';
			}
			echo ' 		</div>
					</div>';
		}
	}
	echo'		</div>';

	require('php/pie.php');
?>