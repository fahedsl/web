<?php
	require('php/cabecera.php');

	if(!isset($_GET['i'])){
		echo '
					<div class="tag">';
		if ($lang == 'es') {
			echo '
						<h2>Cursos</h2>';
		}
		elseif ($lang == 'en') {
			echo '
						<h2>Courses</h2>';
		}
		echo '
					</div>
					<div class="info">';

		$sql = 'select * from cursos';
	//	echo $sql;
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()){
				if ($lang == 'es') {$tit = $row['tit_es']; $cont = $row['cont_es']; $dia = $row['d1']; $hora = $row['h1'];}
				elseif ($lang == 'en') {$tit = $row['tit_en']; $cont = $row['cont_en']; $dia = $row['d2']; $hora = $row['h2'];}
				echo'	<div>
							<div style="margin:0">
								<a href="cursos.php?i='.$row['id'].'">
									<div class="tag2">
										<h3>'.$tit.'</h3>
									</div>
								</a>
							</div>
							<div style="margin:0">
								<div class="tag3">
									<p style="margin:auto auto 0;">'.$dia.'</p>
									<p style="margin:0 auto auto;">'.$hora.'</p>
								</div>
							</div>
							<div style="margin:1rem;">
								<p style="margin:0;">'.$cont.'</p>
							</div>
						</div>';
			}
		}
		echo'		</div>';
	}

	elseif(isset($_GET['i'])){
			$sql = 'select * from cursos where id ='.$_GET['i'];
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			$row = $result->fetch_assoc();
			if ($lang == 'es') {$tit = $row['tit_es']; $cont = $row['cont_es']; $hora = $row['h1']; $dia = $row['d1'];}
			elseif ($lang == 'en') {$tit = $row['tit_en']; $cont = $row['cont_en']; $hora = $row['h2']; $dia = $row['d2'];}
			echo'		<div class="tag">
							<h2>'.$tit.'</h2>
						</div>
						<div>
							<div class="info">
								<p style="margin:1rem;">'.$cont.'</p>
								<p style="margin:1rem; font-weight:300; text-align:left;">'.$dia.' '.$hora.'</p>
								<div style="margin:1rem">
									<h3 style="text-align:center">¡Quiero inscribirme, contáctenme!</h3>
									<form action="envio.php?fich=curso&i='.$_GET['i'].'&red='.$_SERVER['PHP_SELF'].'" method="post">
										<input class="entrada small-12 medium-6 columns" style="display:inline-block;" type="email" name="email" placeholder="e-mail" required=""><br>
										<input style="display:none;" type="number" name="numero" value="'.$_GET['i'].'" required="">
										<input class="boton small-12 medium-5 columns" style="display:inline-block;" type="submit" value="Quiero Inscribirme">
									</form>
								</div>
							</div>
						</div>';
		}
	}

	require('php/pie.php');
?>