function cargado() {
    $("#loader-wrapper").fadeOut(1000);
}

function abre_menu() {
    document.getElementById("menu-resp").classList.remove("reduce");
    document.getElementById("menu-resp").className += (" expand");
    document.getElementById('menu-ico').setAttribute('onClick', 'cierra_menu()');
}

function cierra_menu() {
    document.getElementById("menu-resp").classList.remove("expand");
    document.getElementById("menu-resp").className += (" reduce");
    document.getElementById('menu-ico').setAttribute('onClick', 'abre_menu()');
}

function reset_menu() {
    var w = window.innerWidth;
    if (w >= 321) {
        document.getElementById("menu-resp").classList.remove("expand");
        document.getElementById("menu-resp").classList.remove("reduce");
        document.getElementById("menu-resp").style.height = "auto";
        document.getElementById('menu-ico').setAttribute('onClick', 'abre_menu()');
    } else {
        document.getElementById("menu-resp").style.height = "1px";
    }
}