<?php
	if (session_status() == PHP_SESSION_NONE) { 
		session_start();
		if (!isset($_SESSION['lang']) || $_SESSION['lang'] == 'en'){
			$_SESSION['lang'] = 'es';
		}
	}
	header('location:'.$_SERVER['HTTP_REFERER']);
?>