<?php
	require 'a_tiemp.php';

	if (session_status() == PHP_SESSION_NONE) { session_start(); }

	if (!isset($_SESSION['idp']) || is_null($_SESSION['idp']) || $_SESSION['idp'] == 0){
		if (!isset($_COOKIE['noche']) || is_null($_COOKIE['noche']) || $_COOKIE['noche'] == 0 ){
			$url = $_SERVER['REQUEST_URI'];
			$dir ='logueate.php';
			set_dir($dir, 1, 'url', $url);
		}
		else{
			require 'logueado.php';
			require 'a_headers.php';
		}
	}
	else{
		require 'a_headers.php';
	}
?>