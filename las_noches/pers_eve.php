<?php
	require "a_encabezado.php";
	require "a_sql.php";

	echo '		<div class="item">';
//Pases
	$sql = 'select pase.ide, nomev, cant, pase.cont from pase, evento WHERE evento.ide = pase.ide and pase.idp ='.$_SESSION['idp'];
//	echo $sql;
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()){
			if($row['cant'] == 1){$pase = 'pase';}
			elseif($row['cant'] > 1){$pase = 'pases';}
			if($row['cont'] <= 0 ){
			echo '	<div class="parte">
					<div class="bloq">
						<div class="cont1">
							Tienes '.$row['cant'].' '.$pase.' a 
							<a href="pers_eve_desc.php?n='.$row['ide'].'">
								'.$row['nomev'].'
							</a>
						</div>';
			echo '			<div class="cont1">
							<a href="pers_eve_bor.php?c='.$row['cant'].'&e='.$row['ide'].'">
								<button class="boton">
									Anular '.$row['cant'].' Pase(s)
								</button>
							</a>
						</div>
					</div>
				</div>';
			}
		}
	}
	else{
		echo '		<div class="parte">
					<div class="bloq">
						<div class="cont1">
							No tienes pases registrados. 
						</div>
					</div>
				</div>';
	}


// Eventos
	echo '			<div class="parte">
					<div class="bloq">';
	$sql = 'select * from evento where tipo1 > 0 and estado = 1';
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()){
			echo '			<div class="flota">';
			if($row['tipo1'] > 0){
				$sqlb = 'select nomloc from local where idl = '.$row['idl'];
				$resultb = $conn->query($sqlb);
				if ($resultb->num_rows > 0) {
					$rowb = $resultb->fetch_assoc();
				}
				echo '			<a href="pers_eve_desc.php?a='.$row['ide'].'">';
				echo '				<div class="elem1">
									<div class="flot">
										<div class="bloq">
											<div class="bloq">
												<h5 class="ev_h">
													'.$row['nomev'].'
												</h5>
											</div>';
				echo '							<div class="bloq">
												<h6 class="ev_h">
													'.$rowb['nomloc'].'
												</h6>
											</div>';
				if($row['hora']>=10){$hora = $row['hora'].':00';}
				else{$hora = '0'.$row['hora'].':00';}
				echo '							<div class="bloq">
												<h6>
													'.$row['fecha'].', '.$hora.' horas
												</h6>
											</div>
										</div>
									</div>';
// Imagen
				echo '					<div class="flot_img">
										<div class="flot_img2">
											<div class="flot_img3">';
				if(!is_null($row['imagen']) && $row['imagen'] !== 0 && $row['imagen'] <> ''){
					echo '							<img src="'.$row['imagen'].'" style="max-height:10em;">';
				}
				else{
					$sqll = 'select logo from local where idl = '.$row['idl'] ;
					$resultl = $conn->query($sqll);
					if ($resultl->num_rows > 0) {
						while($rowl = $resultl->fetch_assoc()){
					echo '							<img src="'.$rowl['logo'].'" style="max-height:10em;">';
						}
					}
				}
				echo '							</div>
										</div>
									</div>
								</div>
							</a>
						</div>';
			}
		}
	}
	echo '				</div>';
	echo '			</div>
			</div>';

	$conn -> close();
	require "a_pie.php";
?>