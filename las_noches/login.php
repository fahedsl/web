<?php
	require 'a_tiemp.php';
	require 'a_ssl.php';
	require 'a_headers.php';
	require 'a_fb_config.php';
	require 'fb/functions.php';
?>

	<body>
	<div class='wrap'>
		<div class="navbar">

<?php
	if(!$fbuser){
		$fbuser = null;
		$loginUrl = $facebook->getLoginUrl(array('redirect_uri'=>$homeurl,'scope'=>$fbPermissions));
		$output = '<a href="'.$loginUrl.'"><img src="fb/fb_login.png"></a>'; 	
	}
	else{
		$user_profile = $facebook->api('/me?fields=id,first_name,last_name,email,gender,picture');
		$user = new Users();
		$user_data = $user->checkUser($user_profile['id'],$user_profile['first_name'],$user_profile['last_name'],$user_profile['email'],$user_profile['gender'],$user_profile['picture']['data']['url']);
		if(!empty($user_data)){
		if (session_status() == PHP_SESSION_NONE) { session_start(); }

		$_SESSION['idp'] = $user_data['idp'];
		$_SESSION['idfb'] = $user_data['idfb'];
		$_SESSION['nom'] = $user_data['nombrepersona'];
		$_SESSION['apel'] = $user_data['apellidopersona'];
		$_SESSION['sexo'] = $user_data['sexo'];
		$_SESSION['email'] = $user_data['email'];
		$_SESSION['edad'] = $user_data['edad'];
		$_SESSION['foto'] = $user_data['foto'];
		$_SESSION['tipo'] = $user_data['tipo'];
		$_SESSION['qr'] = $user_data['qr'];
		$_SESSION['ref'] = $user_data['ref'];
		$_SESSION['empresa'] = $user_data['empresa'];
		$_SESSION['deempresa'] = $user_data['deempresa'];
		$_SESSION['deevento'] = $user_data['deevento'];
		$_SESSION['cont'] = $user_data['cont'];
		$_SESSION['contemp'] = $user_data['contemp'];

//CREA COOKIE
		if( isset($_SESSION['idfb']) && !is_null($_SESSION['idfb']) && $_SESSION['idfb'] <> ''){
			$encrip = openssl_encrypt($_SESSION['idfb'], $metodo, $llave, FALSE, $iv);
			$cnom = 'noche';
			$cval = $encrip;
			setcookie($cnom, $cval, (time() + 3600), '/', '.lasnoches.party');
		}

// CREA QR
		require 'a_creador_qr.php';

//redir
			$dir = 'index.php';
			set_dir($dir, 1, 'url', $url);
		}
	}
	
	if( isset($output) ){
		echo $output;
	}

	echo '	</div>
		<div class="cuerpo">';
// Pop Up
		if(isset($_GET['msj'])){
			echo '	<div class="pop">
					<div class="popcont">'.$_GET['msj'].'</div>
				</div>';
		}
// TITULO DE LA PAGINA
		require 'a_titulo.php';
	echo '		<div class="contenido">';

	require 'a_pie.php';
?>