<?php
	function valida_tex($texto, $max){
		if(isset($texto)){
			if(strlen($texto) <= $max){
				$texto = filter_var($texto, FILTER_SANITIZE_STRING);
				$texto = htmlspecialchars($texto);
				return $texto;
			}
		}
	}
	
	function reemp_tex($texto, $texviejo, $texnuevo){
		$texto= str_replace($texviejo, $texnuevo, $texto);
		return $texto;
	}
?>