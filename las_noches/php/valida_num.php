<?php
	function valida_num($num, $min, $max, $reemp = 100 ){
		if(isset($num)){
			$numero = $num;
			$numero = htmlspecialchars($numero);
			$numero = filter_var($numero, FILTER_SANITIZE_NUMBER_INT);
			$numero = (int)$numero;
			if($numero >= $min && $numero <= $max){
				return $numero;
			}
			else{
				return $reemp;
			}
		}
	}
?>