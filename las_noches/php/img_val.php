<?php
	function img_val($origen, $destino, $tammax, $horiz, $vert, $calidad, $name = 'imgsubir'){

		$imgdat = getimagesize($origen);
		if($imgdat){	
			$tam = $_FILES[$name]['size'];
			$tipo = $imgdat['mime'];
		}

// Verificar tamaño maximo
		if(!$imgdat){
			$msj = 'El archivo no es una imagen.';
			$redir = 1;
		}
		elseif(isset($tam)){
			if($tam > $tammax){
				$msj = 'La imagen excede el tamaño máximo (4Mb).';
				$redir = 1;
			}
		}
		elseif(isset($tipo)){
			if($tipo != 'image/jpeg' && $tipo != 'image/png'){
				$msj = 'La imagen tiene un formato no válido (Se aceptan .png y .jpeg).';
				$redir = 1;
			}
		}

		if(!isset($redir)){
			$Tvert = $imgdat['1'];
			$Thoriz = $imgdat['0'];

// Calculo de dimensiones finales
			if($Thoriz >= $Tvert){
				if($Thoriz > $horiz){
					$Tvert = ceil(($horiz * $Tvert) / $Thoriz);
					$Thoriz = $horiz;
				}
			}
			elseif($Tvert > $Thoriz){
				if($Tvert > $vert){
					$Thoriz = ceil(($Thoriz * $vert) / $Tvert);
					$Tvert = $vert;
				}
			}

//Guarda la imagen JPEG en servidor (CAL:100%)
			if	($tipo == 'image/jpeg'){
				$img  = imagecreatefromjpeg($origen);
			}
			elseif	($tipo == 'image/png'){
				$img  = imagecreatefrompng($origen);
			}
			imagejpeg($img, $destino, 100);
			imagedestroy($img);
		
// Redimensionar imagen y reducir calidad
	// Redimension
			$imag1 = imagecreatefromjpeg($destino);
			$imag2 = imagecreatetruecolor($Thoriz, $Tvert);
			imagecopyresampled($imag2, $imag1, 0, 0, 0, 0, $Thoriz, $Tvert, $imgdat['0'], $imgdat['1']);
			imagejpeg($imag2, $destino, $calidad);
			imagedestroy($imag1);
			imagedestroy($imag2);
		}
		if(isset($msj)){return $msj;}
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	function img_reduc($origen, $destino ,$horiz, $vert, $calidad){
		
		$imgdat = getimagesize($origen);
		$tam = $_FILES['imgsubir']['size'];
		$tipo = $imgdat['mime'];
		$Tvert = $imgdat['1'];
		$Thoriz = $imgdat['0'];

// Calculo de dimensiones finales
		if($Thoriz >= $Tvert){
			if($Thoriz > $horiz){
				$Tvert = ceil(($horiz * $Tvert) / $Thoriz);
				$Thoriz = $horiz;
			}
		}
		elseif($Tvert > $Thoriz){
			if($Tvert > $vert){
				$Thoriz = ceil(($Thoriz * $vert) / $Tvert);
				$Tvert = $vert;
			}
		}

// Redimensionar imagen y reducir calidad
	// Redimension
		$imag1 = imagecreatefromjpeg($origen);
		$imag2 = imagecreatetruecolor($Thoriz, $Tvert);
		imagecopyresampled($imag2, $imag1, 0, 0, 0, 0, $Thoriz, $Tvert, $imgdat['0'], $imgdat['1']);
		imagejpeg($imag2, $destino, $calidad);
		imagedestroy($imag1);
		imagedestroy($imag2);
	}
?>