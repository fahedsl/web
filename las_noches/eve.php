<?php
	require "a_encabezado.php";
	require 'php/valida_tex.php';
	require "a_sql.php";

	$sql = 'select * from trabs where idp = '.$_SESSION['idp'].' and idem = '.$_SESSION['deempresa'].' and eve = 1';
	//echo $sql;
	$result = $conn->query($sql);
	if ( (($result->num_rows > 0) && ($_SESSION['tipo'] == 9)) ||  ($_SESSION['tipo'] == 10) ){
		echo '			<div class="item">';
		if($_SESSION['tipo'] == 10){
			$sql = 'select * from evento where idem = '.$_SESSION['deempresa'].' and estado = 1';
		}
		elseif($_SESSION['tipo'] == 9){
			$sql = 'select * from evento where idem = '.$_SESSION['deempresa'].' and estado = 1';
		}	
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()){
			//nombre de evento
				echo '		<div class="bloq">
							<h3>
								'.$row['nomev'].'
							</h3>
								<a href="eve_ed.php?a='.$row['ide'].'">
									<button class="boton">Editar</button>
								</a>
						</div>';
			//descripcion de evento
				$sqla = 'select nomloc from local where idl = '.$row['idl'];
				$resulta = $conn->query($sqla);
				if ($resulta->num_rows > 0) {
					$rowa = $resulta->fetch_assoc();
					$descrip = reemp_tex($row['descrip'], '::', '<br>');
	
					echo'	<div class="bloq">';
					echo '		<div class="cont1">
								<p>
									'.$descrip.'
								</p>
							</div>
						</div>';
				}
			//fecha y hora
				if($row['hora'] < 10){ $hora = '0'.$row['hora'].':00'; }
				else{ $hora = $row['hora'].':00'; }
				echo '		<div class="bloq">';
				echo '			<div class="cont1">
								<p>
									'.$row['fecha'].' a las '.$row['hora'].':00 horas.
								</p>
							</div>
						</div>';
			//nombre de local
					echo '	<div class="bloq">
							<div class="cont1">
								<p>
									'.$rowa['nomloc'].'
								</p>
							</div>
						</div>';
			//imagen de evento
				if( isset($row['imagen']) && !is_null($row['imagen']) && $row['imagen'] !== 0 && $row['imagen']<>''){
					echo '	<div class="bloq">
							<a href="eve_img.php?a='.$row['ide'].'">
								<div class="eve_img">
									<img src="'.$row['imagen'].'">
								</div>
							</a>
						</div>';
				}
				else{
					echo '	<div class="bloq">
							<a href="eve_img.php?a='.$row['ide'].'">
								<button class="boton">Agregar Imagen</button>
							</a>
						</div>';

				}
			}
		}
		echo '				<div class="item_bot">
							<a href="eve_ed.php">
								<button class="boton">Crear Nuevo Evento</button>
							</a>
						</div>
					</div>';
	}
	else{
		$zapoz = $_SERVER['REQUEST_URI'];
		$sqlputos='insert into zapoz(idp, pag) values('.$_SESSION['idp'].',"'.$zapoz.'")';
		$conn->query($sqlputos);
		$conn -> close();
		$msj = 'Usted no tiene acceso a la página requerida.';
		set_dir('index.php', 1, 'msj', $msj);
	}
	
	$conn->close();
	require 'a_pie.php';
?>