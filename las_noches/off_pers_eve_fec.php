<?php
	require "off_encabezado.php";
	require "a_sql.php";
//Conectar a BD
	$conn = new mysqli($server, $user, $pass, $db);
	if ($conn->connect_error) { die($conn->connect_error); }

	echo '	<div class="item" style="min-height:auto;">';
	echo '		<div class="bloq">
				<div class="centrar">';
//Obtiene info de la entrada de acuerdo a tipo
	$sqlc = 'select * from evento where tipo1 >0 and fecha = "'.$_GET['d'].'" and estado = 1';
//	echo $sqlc;
	$resultc = $conn->query($sqlc);
	if ($resultc->num_rows > 0) {
		while($rowc = $resultc->fetch_assoc()){
			echo '		<div class="flot">';
			if($rowc['tipo1'] > 0){
				echo '		<a href="off_pers_eve_desc.php?e='.$rowc['ide'].'">';
			}
			echo '				<div class="elem1">
								<div class="flot">
									<div class="bloq">
										<div class="bloq">
											<div class="tit2"
											style="white-space:nowrap; overflow:hidden; 
											text-overflow:ellipsis; -o-text-overflow:ellipsis;">
												'.$rowc['nomev'].'
											</div>
										</div>
										<div class="bloq" style="max-height:3.8em;overflow:hidden;">
											<div class="cont1" style="width:100%;">
												'.$rowc['descrip'].'
											</div>
										</div>';
			if($rowc['hora']>=10){$hora = $rowc['hora'].':00';}
			else{$hora = '0'.$rowc['hora'].':00';}
			echo '							<div class="bloq">
											<div class="tit2">
												'.$rowc['fecha'].', '.$hora.' horas
											</div>										
										</div>
									</div>
								</div>';
// Imagen
			echo '					<div class="flot_img">
									<div class="flot_img2">
										<div class="flot_img3">';
			if(!is_null($rowc['imagen']) && $rowc['imagen'] !== 0 && $rowc['imagen'] <> ''){
				echo '							<img src="'.$rowc['imagen'].'" style="max-height:10em;">';
			}
			else{
				$sqll = 'select logo from local where idl = '.$rowc['idl'] ;
				$resultl = $conn->query($sqll);
				if ($resultl->num_rows > 0) {
					while($rowl = $resultl->fetch_assoc()){
				echo '							<img src="'.$rowl['logo'].'" style="max-height:10em;">';
					}
				}
			}
			echo '							</div>
									</div>
								</div>
							</div>
						</a>
					</div>';
		}
	}
	else{
		echo '			<div class="bloq">
						<h4>
							<p>
								No hay eventos para esta fecha.</br>
								Intenta con otra. :3
							</p>
						</h4>
					</div>';
	}
	
	echo '			</div>
			</div>
		</div>';
		
	require "a_cont1.php";
	require "a_pie.php";
?>