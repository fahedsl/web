<?php
	require 'php/servidor.php';

// direccion donde redirigir
	if(isset($_GET['url'])){
		$url = $_GET['url'];
	}
// si hay cookie
	if(isset($_COOKIE['noche']) || isset($_SESSION['idp'])){
		require 'logueado.php';
		if(isset($url)){
			$dir = $url;

			header('location:'.$dir);
		}
		if(!isset($_SESSION['tipo']))	{ $dir = 'pers_eve.php'; }
		elseif($_SESSION['tipo'] == 1)	{ $dir = 'pers_eve.php'; }
		elseif($_SESSION['tipo'] == 7)	{ $dir = '4dm1_ini.php'; }
		elseif($_SESSION['tipo'] == 9)	{ $dir = 'pers_eve.php'; }
		elseif($_SESSION['tipo'] == 10)	{ $dir = 'emp.php'; }
		if(isset($dir)){
			require $dir;
		}
	}
// si no hay cookie
	else{
		require 'login.php';
	}
?>