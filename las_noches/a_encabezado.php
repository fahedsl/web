<?php
	require 'php/servidor.php';
	
	if(isset($_COOKIE['noche']) && session_status() == PHP_SESSION_NONE){
		require 'logueado.php';
		require 'a_tiemp.php';
		require 'a_headers.php';
	}
	else{
		$url = $_SERVER["REQUEST_URI"];
		require 'a_ses_tiemp.php';
	}

	echo '	<body>
			<div class="wrap">
				<div class="navbar">';

//SQL
	if(session_status() != PHP_SESSION_NONE || isset($_COOKIE['noche'])){
		if( $_SESSION['tipo'] == 9 ){
			require "a_sql.php";
//query 1
			$sql1 = 'select jefe, col, eve, bounc from trabs where idp = '.$_SESSION['idp'];
			$result1 = $conn->query($sql1);
			if ($result1->num_rows > 0) {
				$jefe = 0;
				$col = 0;
				$eve = 0;
				$bounc = 0;
				while($row1 = $result1->fetch_assoc()){
					if ($row1['jefe'] == 1){
						$jefe = 1;
					}
					if ($row1['col'] == 1){
						$col = 1;
					}
					if ($row1['eve'] == 1){
						$eve = 1;
					}
					if (!is_null($row1['bounc'])){
						$bounc = 1;
					}
				}
			}
			$conn->close();
		}
// Elementos del menú
		echo '		<div class="navitem">
					<a href="index.php">
						<img class="navitem" src="'.$_SESSION['foto'].'">
					</a>
				</div>';
	// Cliente eventos
		if ( $_SESSION['tipo'] == 1 || $_SESSION['tipo'] == 2 || $_SESSION['tipo'] == 9 ||  $_SESSION['tipo'] == 7 ){
			echo '	<div class="navitem">
					<a href="pers_eve.php">
						<img class="navimg" src="imgweb/ico_ticket.png">
						<p class="navtex">
							Eventos
						</p>
					</a>
				</div>';
	// Cliente QR
			echo '	<div class="navitem">
					<a href="pers_qr.php">
						<img class="navimg" src="imgweb/ico_qr.png">
						<p class="navtex">
							Mi QR
						</p>
					</a>
				</div>';
		}
	// Dueño empresa
		if ( $_SESSION['tipo'] == 10 ){
			echo '	<div class="navitem">
					<a href="emp.php">
						<img class="navimg" src="imgweb/ico_emp.png">
						<p class="navtex">
							Empresa
						</p>
					</a>
				</div>';
		}
	// Dueño marcas
		if ( $_SESSION['tipo'] == 10 && $_SESSION['empresa'] !== 0 && $_SESSION['empresa'] <> NULL ){
			echo '	<div class="navitem">
					<a href="marc.php">
						<img class="navimg" src="imgweb/ico_marc.png">
						<p class="navtex">
							Marcas
						</p>
					</a>
				</div>';
		}
	// Dueño colaboradores
		if ( ( $_SESSION['tipo'] == 10 && $_SESSION['empresa'] !== 0 && $_SESSION['empresa'] <> NULL ) || ($_SESSION['tipo'] == 9 && $col == 1 ) ){
			echo '	<div class="navitem">
					<a href="col.php">
						<img class="navimg" src="imgweb/ico_col.png">
						<p class="navtex">
							Colaboradores
						</p>
					</a>
				</div>';
		}
	// Colabs Editar eventos
		if ( ( $_SESSION['tipo'] == 10 && $_SESSION['empresa'] !== 0 && $_SESSION['empresa'] <> NULL ) || ($_SESSION['tipo'] == 9 && $eve == 1 ) ){
			echo '	<div class="navitem">
					<a href="eve.php">
						<img class="navimg" src="imgweb/ico_eve.png">
						<p class="navtex">
							Editar Eventos
						</p>
					</a>
				</div>';
		}
	// Colabs Puerta
		if ( ( $_SESSION['tipo'] == 10 && $_SESSION['empresa'] !== 0 && $_SESSION['empresa'] <> NULL ) || ($_SESSION['tipo'] == 9 && $bounc == 1 ) ){
			echo '	<div class="navitem">
					<a href="bounc.php">
						<img class="navimg" src="imgweb/ico_puerta.png">
						<p class="navtex">
							Puerta
						</p>
					</a>
				</div>';
		}
	// Cliente Referencias
		if ( ($_SESSION['tipo'] == 1 && $_SESSION['ref']<>1 ) ){
			echo '	<div class="navitem">
					<a href="ref.php">
						<img class="navimg" src="imgweb/ico_qr.png">
						<p class="navtex">
							Referencia
						</p>
					</a>
				</div>';
		}
	// Salir
		echo '		<div class="navitem">
					<a href="logout.php?logout">
						<img class="navimg" src="imgweb/ico_salir.png">
						<p class="navtex">
							Salir
						</p>
					</a>
				</div>
			</div>';					
		echo '	<div class="cuerpo">';
// pop up
		if(isset($_GET['msj'])){
			echo '	<div class="pop">
					<div class="popcont">'.$_GET['msj'].'</div>
				</div>';
		}
// TITULO DE LA PAGINA
		echo '		<div class="contenido">';
		
		require 'a_titulo.php';
	}
?>