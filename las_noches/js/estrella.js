 // Max number of stars
	var limit=25, cuerpo = document.getElementById('titulo');
	loop={
		//initilizing
		start:function(){
			for (var i=0; i <= limit; i++) {
				var star=this.newStar();
				star.style.marginTop =this.rand() * $('#titulo').height() +'px';
				star.style.left = this.rand() * $('#titulo').width() +'px';
				star.style.webkitAnimationDelay=this.rand()+'s';
				star.style.mozAnimationDelay=this.rand()+'s';
				cuerpo.appendChild(star);
			};
		},
		
		//to get random number
		rand:function(){
			return Math.random();
		},
		
		//creating html dom for star
		newStar:function(){
			var d = document.createElement('div');
			d.innerHTML = '<p class="star">*</p>';
			return d.firstChild;
		},
	};
	loop.start();