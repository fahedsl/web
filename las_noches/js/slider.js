$(function ($) {
	function corre(){
		setInterval(function () {
			moveRight();
		}, 3000);
	}
	window.onload = corre();
	
	var slideCount = $('#slider ul li').length;
	if(screen.width >= 900){
		var slideWidth = (2/3)*screen.width;
	}
	else if(screen.width < 900){
		var slideWidth = (9/10)*screen.width;
	}
	var slideHeight = (1/3)*slideWidth;
	var slideUlWidth = slideCount * slideWidth;

	$('#slider').css({ width: slideWidth, height: slideHeight });

	$('#slider ul').css({ width: slideUlWidth });

	$('#slider ul li').css({ width: slideWidth, height: slideHeight});

	$('#slider ul li:last-child').prependTo('#slider ul');

	$('a.control_prev').click(function () {
		moveLeft();
	});

	$('a.control_next').click(function () {
		moveRight();
	});

	function moveLeft() {
		$('#slider ul').animate({
			left: + slideWidth
			}, 200, function () {
			$('#slider ul li:last-child').prependTo('#slider ul');
			$('#slider ul').css('left', '');
		});
	};
	
	function moveRight() {
		$('#slider ul').animate({
			left: - slideWidth
			}, 200, function () {
			$('#slider ul li:first-child').appendTo('#slider ul');
			$('#slider ul').css('left', '');
		});
	};

});    