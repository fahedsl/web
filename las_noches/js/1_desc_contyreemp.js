$(document).ready(function() {
	var tex_max = 1000;
// conteo de letras
		$('#descrip').keyup(function(){
		var tex_larg = $('#descrip').val().length;
		var tex_rest = tex_max - tex_larg;
		$('#desccont1').html('Quedan ' + tex_rest + ' caracteres.');
// reemplazo de caracteres
		var tex_cont = $('#descrip').val();
		var tex_final = tex_cont.replace(/::/g, '<br>');
		$('#descmost').html(tex_final);
	});
});