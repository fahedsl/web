function calendario(){
	$(function() {
		$('#fecha1').datepicker({
			dateFormat: 'yy-mm-dd',
			maxDate: "+1m",
			minDate: "1",
			firstDay: "0",
			prevText: "Mes Anterior",
			nextText: "Mes Siguiente",
			monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre" ],
			monthNamesShort: [ "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dic" ],
			dayNames: [ "Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado" ],
			dayNamesShort: [ "Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab" ],
			dayNamesMin: [ "Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab" ],
		});
		$('#fecha1').datepicker('setDate', '1');
	});
}

calendario();