 // Max number of stars
		var limit=15, body=document.body;
    loop={
     //initilizing
     start:function(){
      for (var i=0; i <= limit; i++) {
       var star=this.newStar();
       star.style.top=this.rand()*100+"%";
       star.style.left=this.rand()*100+"%";
       star.style.webkitAnimationDelay=this.rand()+"s";
       star.style.mozAnimationDelay=this.rand()+"s";
       body.appendChild(star);
      };
     },
     //to get random number
     rand:function(){
      return Math.random();
     },
     //creating html dom for star
     newStar:function(){
      var d = document.createElement('div');
      d.innerHTML = '<figure class="star"><figure class="star-top"></figure><figure class="star-bottom"></figure></figure>';
       return d.firstChild;
     },
    };
    loop.start();