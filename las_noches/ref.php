<?php
	require 'a_encabezado.php';

	if ( $_SESSION['ref'] == 1 ){
		$msj = 'Ya tienes una referencia registrada.';
		$dir = 'index.php';
		set_dir($dir, 1, 'msj', $msj);
	}
	else{
		echo '	<div class="item">
				<form action="ref_dec.php" method="post">
					<h4>Ingresa tu Código de Referencia:</h4>
					<input class="llenar" type="text" name="ref" style="min-width:auto; width:50%;">
					<div class="item_bot">
						<input class="boton" type="submit" value="Ingresar Referencia">
					</div>
				</form>
			</div>
			<div class="aviso">
				<h3>Si tienes un código QR, escanéalo con tu <a href="https://play.google.com/store/apps/details?id=com.arara.q">Lector QR preferido</a>.</h3>
			</div>';
	}

	require 'a_pie.php';
?>