<?php
	include_once("a_fb_config.php");
	if(array_key_exists('logout',$_GET))
	{
		session_unset();
		$facebook->destroySession();
		unset($_SESSION['userdata']);
		setcookie('PHPSESSID', '', time() - 3600, '/', '.lasnoches.party'); 
		session_destroy();
		header("Location:index.php");
		setcookie('noche', '', time() - 3600, '/', '.lasnoches.party');
	}
?>