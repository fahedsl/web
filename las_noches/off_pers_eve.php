<?php
	require "off_encabezado.php";
	require "a_sql.php";
//Conectar a BD	
	$conn = new mysqli($server, $user, $pass, $db);
	if ($conn->connect_error) { die($conn->connect_error); }
// Obtiene info de pases
	$sqla = ' select lunas, cont, pases, eventos from persona where idp = '.$_SESSION['idp'].' ';
//	echo $sqla;
	$resulta = $conn->query($sqla);
	if ($resulta->num_rows > 0) {
		while($rowa = $resulta->fetch_assoc()){
			$sqlb = ' select * from derechos where min <= '.$rowa['lunas'].' and max >= '.$rowa['lunas'].' ';
//			echo $sqlb;
			$resultb = $conn->query($sqlb);
			if ($resultb->num_rows > 0) {
				$rowb = $resultb->fetch_assoc();
				$la = $rowa['eventos'];
				$lb = $rowb['eventos'];
				$pa = $rowa['pases'];
				$pb = $rowb['pases'];
				$eventos= $lb - $la;
				$pases	= $pb - $pa;
				$lunas	= $rowa['lunas'];
//				echo $lb.' '.$la.' '.$pb.' '.$pa;
			}
		}
	}
	
//Inicio ITEM
	echo '	<div class="item">';
	echo '		<div class="bloq" style="margin-bottom:0.5em;">
				<div class="centrar">
					<div class="tit2">
						Lunas:
					</div>
					<div class="cont1">
						'.$lunas.'
					</div>
				</div>
				<div class="centrar">
					<div class="tit2">
						Pases Disponibles:
					</div>
					<div class="cont1">
						'.$pases.'/'.$rowb['pases'].'
					</div>
				</div>
				<div class="centrar">
					<div class="tit2">
						Eventos:
					</div>
					<div class="cont1">
						'.$eventos.'/'.$rowb['eventos'].'
					</div>
				</div>
			</div>
			<div class="bloq">
				<div class="centrar">';

//Obtiene info de la entrada de acuerdo a tipo
	if ($_SESSION['tipo'] == 1){
		$sqlc = 'select * from evento where tipo1 >0 and estado = 1';
	}
	elseif ($_SESSION['tipo'] == 2){
		$sqlc = 'select *  from evento where tipo2 >0 or tipo1 >0 and estado = 1';
	}
//	elseif ($_SESSION['tipo'] == 3){
//		$sqlc = 'select *  from evento where tipo3 >0 or tipo2 >0 or tipo1 >0 and estado = 1';
//	}
	else{
		$sqlc = 'select * from evento where tipo1 >0 and estado = 1';
	}
	$resultc = $conn->query($sqlc);
	if ($resultc->num_rows > 0) {
		while($rowc = $resultc->fetch_assoc()){
			echo '		<div class="flot">';
			if ($_SESSION['tipo'] == 2){
				if(($rowc['tipo2'] > 0) or ($rowc['tipo1'] > 0)){
					echo '		<a href="evini.php?n='.$rowc['ide'].'">';
				}
			}
//			elseif ($_SESSION['tipo'] == 3){
//				if(($rowc['tipo3'] > 0) or ($rowc['tipo2'] > 0) or ($rowc['tipo1'] > 0)){
//					echo '		<a href="evini.php?n='.$rowc['ide'].'">';
//				}
//			}
			else{
				if($rowc['tipo1'] > 0){
					echo '		<a href="pers_eve_desc.php?n='.$rowc['ide'].'">';
				}
			}
			echo '				<div class="elem1">
								<div class="flot">
									<div class="bloq">
										<div class="bloq">
											<div class="tit2"
											style="white-space:nowrap; overflow:hidden; 
											text-overflow:ellipsis; -o-text-overflow:ellipsis;">
												'.$rowc['nomev'].'
											</div>
										</div>
										<div class="bloq" style="max-height:3.8em;overflow:hidden;">
											<div class="cont1" style="width:100%;">
												'.$rowc['descrip'].'
											</div>
										</div>';
			if($rowc['hora']>=10){$hora = $rowc['hora'].':00';}
			else{$hora = '0'.$rowc['hora'].':00';}
			echo '							<div class="bloq">
											<div class="tit2">
												'.$rowc['fecha'].', '.$hora.' horas
											</div>										
										</div>
									</div>
								</div>';
// Imagen
			echo '					<div class="flot_img">
									<div class="flot_img2">
										<div class="flot_img3">';
			if(!is_null($rowc['imagen']) && $rowc['imagen'] !== 0 && $rowc['imagen'] <> ''){
				echo '							<img src="'.$rowc['imagen'].'" style="max-height:10em;">';
			}
			else{
				$sqll = 'select logo from local where idl = '.$rowc['idl'] ;
				$resultl = $conn->query($sqll);
				if ($resultl->num_rows > 0) {
					while($rowl = $resultl->fetch_assoc()){
				echo '							<img src="'.$rowl['logo'].'" style="max-height:10em;">';
					}
				}
			}
			echo '							</div>
									</div>
								</div>
							</div>
						</a>
					</div>';
		}
	}
	echo '			</div>
			</div>
		</div>';
		
	require "a_pers_cont1.php";
	require "a_pie.php";
?>