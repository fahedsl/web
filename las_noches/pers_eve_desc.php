<?php
	require "a_encabezado.php";
	require 'php/valida_tex.php';
	require "a_sql.php";

	if(!isset($_GET['a'])){
		$conn -> close();
		$dir = 'pers_eve.php';
		set_dir($dir, 1);
	}
	else{
//Pases
	$sql = 'select pase.ide, nomev, cant, cont from pase, evento WHERE evento.ide = pase.ide and pase.idp ='.$_SESSION['idp'].' and (evento.estado = 1 or evento.estado = 2)';
//	echo $sql;
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()){
			if($row['cant'] == 1){$pase = 'pase';}
			elseif($row['cant'] > 1){$pase = 'pases';}
			if($row['cont'] != 0 ){
			echo '	<div class="parte">
					<div class="bloq">
						<div class="cont1">
							Tienes '.$row['cant'].' '.$pase.' a 
							<a href="pers_eve_desc.php?n='.$row['ide'].'">
								'.$row['nomev'].'
							</a>
						</div>';
			echo '			<div class="cont1">
							<a href="pers_eve_bor.php?c='.$row['cant'].'&e='.$row['ide'].'">
								<button class="boton">
									Anular '.$row['cant'].' Pase(s)
								</button>
							</a>
						</div>
					</div>
				</div>';
			}
		}
	}



	// puntos
		require 'a_puntos.php';

	//Informacion del evento
		$sqlc = "select nomev, descrip, hora, fecha, imagen from evento where ide =".$_GET['a'];
	
		$resultc = $conn->query($sqlc);
		if ($resultc->num_rows > 0) {
			$rowc = $resultc->fetch_assoc();
	// Nombre evento
			echo '		<div class="bloq">
						<div class="centrar">
							<h3>
								'.$rowc['nomev'].'
							</h3>
						</div>
					</div>';
	
	//Imagen del evento
			echo '		<div class="bloq">
						<div class="centrar">
								<img src="'.$rowc['imagen'].'" style="max-width:100%;">
						</div>
					</div>';
	
	// fecha y hora evento
			if($rowc['hora']>=10){$hora = $rowc['hora'].':00';}
				else{$hora = '0'.$rowc['hora'].':00';}
			echo '		<div class="bloq">
						<div class="centrar">
							<h5>
								'.$rowc['fecha'].', '.$hora.' horas
							</h5>
						</div>
					</div>';
	// Descripcion evento
			$descrip = reemp_tex($rowc['descrip'], '::', '<br>');
			echo '		<div class="bloq" style="width:90%;">
						<div class="centrar">
							<div class="cont1">
								<p>
									'.$descrip.'
								</p>
							</div>
						</div>
					</div>';
		}
		
		$_GET['a'] = $_GET['a'];
	
	// Seleccionar entre boton de registro y notificacion (y boton de borrar)
		if( $pm > 0 ){
			echo '		<div class="bloq">
						<form action="pers_eve_env.php?a='.$_GET['a'].'" method="post">
							<div class="bloq">
								<div class="tit2">
									<p>Número de pases que deseas:</p>
								</div>
							</div>
							<div class="bloq">
								<div class="centrar">
									<input class="llenar" type="number" name="pases" value= "1" min="1" max="'.$pm.'">
								</div>
								<input class="boton" type="submit" value="Registrar">
							</div>
						</form>
					</div>';
		}
		
		if($pa > 0 ){
			$sqld = "select * from pase where idp =".$_SESSION['idp']." and ide=".$_GET['a'];
	//		echo $sqld;
			$resultd = $conn->query($sqld);
			if ($resultd->num_rows > 0) {
				while($rowd = $resultd->fetch_assoc()){
					echo '	<div class="cont1">
							Tienes <a href="pers_pas.php">'.$rowd['cant'].' pase(s)</a> a este evento.</br>
						</div>';
				}
			}
		}

		echo '	<div class="bloq">
				<a href="pers_eve.php">
					<button class="boton">Volver</button>
				</a>
			</div>';

		require 'a_pie.php';
	}
?>