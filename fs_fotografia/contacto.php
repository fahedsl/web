<?php
	require('php/x-cabecera.php');
?>

				<div class="negro fondo3 v100 h100 absoluto">
					<form style="height:100%; margin:auto; color:white;" id="contacto" action="enviar.php" method="POST">
						<div class="menu-cont2" style="padding-top:4em;">
							<div class="" style="color:#eee">
								<h2>CONTÁCTAME</h2>
								<div class="" style="margin: 1em;">
									<a href="tel:940235704">
										<em class="glyph fa-phone"></em>
										<span class="phone">940 235 704</span>
									</a>
								</div>
								<div class="" style="margin: 1em;">
									<a href="mailto:fahedsl@mail.com">
										<em class="glyph fa-envelope"></em>
										<span>fahedsl@mail.com</span>
									</a>
								</div>
							</div>
							<div class="">
								<input class="entrada" id="nombre" type="text" name="nombre" required="" placeholder="Nombre"/>
							</div>
							<div>
								<input class="entrada" id="email" type="email" name="email" required="" placeholder="Correo electrónico"/>
							</div>
							<div>
								<textarea class="entrada" id="msj" name="msj" rows="5" required="" placeholder="Mensaje"></textarea>
							</div>
							<div>
								<input class="boton" id="submit" type="submit" value="Enviar" style="width:auto; min-width:80px; margin:2em"/>
							</div>
						</div>
					</form>
				</div>

<?php
	require('php/x-pie.php');
?>