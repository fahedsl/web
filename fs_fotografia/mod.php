<?php
	require('php/x-cabecera.php');
	if(isset($_GET['mod'])){
		$modelo = $_GET['mod'];
		require('mod/'.$modelo.'.php');
		$archivo = 'img/modelos/'.$modelo;
		$imgs = scandir($archivo);

		echo '
			<div style="text-align:center; width:100%;">
				<h1>'.$modelo.'</h1>';
		echo '
				<p>
		';
		if(!is_null($estatura) && !is_null($peso) && !is_null($edad) && !is_null($pecho) && !is_null($cintura) && !is_null($cadera)){
			echo $modelo.' tiene '.$edad.' años, mide '.$estatura.'m, pesa '.$peso.'kg';
			if ($sexo == 'masc') {
			 	echo '.<br>';
			 } 
			elseif ($sexo == 'fem') {
			 	echo ' y sus medidas son '.$pecho.'/'.$cintura.'/'.$cadera.'.<br>';
			}
		}
		if(!is_null($zapato) && !is_null($pelo) && !is_null($longitud)){
			echo 'Tiene ojos '.$ojos.' y cabello '.$longitud.' de color '.$pelo.'.<br>
			';
		}
		if(!is_null($idiomas)){
			echo $modelo.' habla '.$idiomas.'.<br>
			';
		}
		if((!is_null($pasar)) || (!is_null($foto)) || (!is_null($anfit)) || (!is_null($anima))) {
			echo $modelo.' se desempeña bien en: <br>';
			if (!is_null($pasar)) {
				echo 'Pasarela   ';
			}
			if (!is_null($foto)) {
				echo 'Fotografía   ';
			}
			if (!is_null($anfit)) {
				echo 'Anfitrionaje   ';
			}
			if (!is_null($anima)) {
				echo 'Animación   ';
			}
		}
		if(!is_null($mas)){
			echo '<br>'.$mas;
		}
		echo '
				<p>
			</div>

			<div class="galeria">
				<div id="galeria" class="grid">
					<div class="grid-sizer"></div>
		';

		foreach ($imgs as $nombre) {
			if(($nombre != '.') && ($nombre != '..')){
				$thumb = $archivo.'/mini/'.$nombre;
				$imagen = $archivo.'/'.$nombre;
				$tags = exif_read_data($imagen);
				if (isset($tags['COMMENT'][0])) {
					$comentario = $tags['COMMENT'][0];
				}
				else{
					$comentario = '';
				}
				echo '
					<div class="grid-item" data-src="'.$imagen.'" data-sub-html="">
						<div class="img_info">
						</div>
						<img src="'.$thumb.'" alt="'.$comentario.'">
					</div>
				';
			}
		}

		echo '
				</div>
			</div>
		';

		echo '
			<script>
				var grid = document.querySelector(".grid");
				var msnry;

				imagesLoaded( grid, function() {
				  msnry = new Masonry( grid, {
					itemSelector: ".grid-item",
				  });
				});
			</script>

			<script>
			$(document).ready(function() {
				$("#galeria").lightGallery({
					download:false,
					loop:false,
				});
			});
			</script>
		';
	}

	require('php/x-pie.php');
?>