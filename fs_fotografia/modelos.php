<?php
	require('php/x-cabecera.php');
		echo '
			<div style="text-align:center; width:100%;">
				<h1>Modelos</h1>
			</div>

			<div class="galeria">
				<div id="galeria" class="grid">
					<div class="grid-sizer"></div>
		';

	$fotos = 'img/modelos';
	$pagina = 'mod';
	$modelo = scandir($fotos);
	foreach ($modelo as $nombre) {
		if(($nombre != '.') && ($nombre != '..')){
			$thumb = $fotos.'/'.$nombre.'/mini/1.jpg';
			$link = 'mod.php?mod='.$nombre;
			echo'
					<div class="grid-item">
						<div class="img_info" onclick="location.href=\''.$link.'\'">
							<div class="hmedio vmedio">
								<h4>'.$nombre.'</h4>
							</div>
						</div>
						<img src="'.$thumb.'">
					</div>
			';
			}
		}
?>
			</div>
		</div>
		<script>
			var grid = document.querySelector('.grid');
			var msnry;

			imagesLoaded( grid, function() {
			  msnry = new Masonry( grid, {
				itemSelector: '.grid-item',
			  });
			});
		</script>

		<script>
		$(document).ready(function() {
			$("#galeria").lightGallery({
				download:false,
				loop:false,
			});
		});
		</script>
<?php
	require('php/x-pie.php');
?>