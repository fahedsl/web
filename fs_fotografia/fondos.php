<?php
	require('php/x-cabecera.php');
?>

		<div class="galeria">
			<h3 style="text-align:center">Descarga tu fondo favorito:</h3>
			<h5 style="text-align:center">Solo selecciona tu imagen y toca la flecha en la esquina superior derecha.</h5>
		</div>
		<div class="galeria">
			<div id="galeria" class="grid">
				<div class="grid-sizer"></div>

<?php
	require('php/x-sql.php');

	$sql1 = 'select * from fondos order by id desc';
	$result = $conn->query($sql1);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()){
			if (!is_null($row['id']) && ($row['id'] != 'no')) {
				$foto = 'img/fondo/fondo'.$row['id'].'.jpg';
				$thumb = 'img/fondo/mini/fondo'.$row['id'].'.jpg';
				if (is_null($row['descrip'])){
					$descrip = $row['descrip'];
				}
				else{
					$descrip='';
				}
				echo '
					<div class="grid-item" data-src="'.$foto.'" data-sub-html="'.$descrip.'">
						<div class="img_info">
							<div class="hmedio vmedio">
							</div>
						</div>
						<img src="'.$thumb.'">
					</div>
				';
			}
		}
	}

	if(isset($conn)){
		$conn -> close();
	}
?>
			</div>
		</div>
		<script>
			var grid = document.querySelector('.grid');
			var msnry;

			imagesLoaded( grid, function() {
			  msnry = new Masonry( grid, {
				itemSelector: '.grid-item',
			  });
			});
		</script>

		<script>
		$(document).ready(function() {
			$("#galeria").lightGallery({
				loop:false,
			});
		});
		</script>
<?php
	require('php/x-pie.php');
?>