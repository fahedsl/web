<?php
	require('php/x-cabecera.php');
?>

		<div class="galeria">
			<div id="galeria" class="grid">
				<div class="grid-sizer"></div>

<?php
	$archivo = 'img/fotos';
	$imgs = scandir($archivo);
	foreach ($imgs as $nombre) {
		if(($nombre != '.') && ($nombre != '..')){
			$thumb = $archivo.'/mini/'.$nombre;
			$imagen = $archivo.'/'.$nombre;
			$tags = exif_read_data($imagen);
			$comentario = $tags['COMMENT'][0];
			echo '
				<div class="grid-item" data-src="'.$imagen.'" data-sub-html="">
					<div class="img_info">
						<div class="hmedio vmedio">
						</div>
					</div>
					<img src="'.$thumb.'" alt="'.$comentario.'">
				</div>
			';
			}
		}
?>

			</div>
		</div>
		<script>
			var grid = document.querySelector('.grid');
			var msnry;

			imagesLoaded( grid, function() {
			  msnry = new Masonry( grid, {
				itemSelector: '.grid-item',
			  });
			});
		</script>

		<script>
		$(document).ready(function() {
			$("#galeria").lightGallery({
				download:false,
				loop:true,
			});
		});
		</script>
<?php
	require('php/x-pie.php');
?>