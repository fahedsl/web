<?php
	$dir = '/foto/';
	$uri = $_SERVER['SCRIPT_NAME'];
?>
			<!--fondo del menu-->
			<div id="entraf" class="menu-cont1 entraf v100 z10n">
				<div class="menu-cont2">
<?php
	if ($uri != '/index.php') {
		echo '
					<div class="menu-item">
						<div class="in-block">
							<a class="link-a" href="index.php">
								<p>INICIO</p>
							</a>
						</div>
					</div>
		';
	}
?>
<?php
	if ($uri != '/portafolio.php') {
		echo '		
					<div class="menu-item">
						<div class="in-block">
							<a class="link-a" href="portafolio.php">
								<p>GALERÍA</p>
							</a>
						</div>
					</div>
		';
	}
?>
<?php
	if ($uri != '/contacto.php') {
		echo '		

					<div class="menu-item">
						<div class="in-block">
							<a class="link-a" href="contacto.php">
								<p>CONTACTO</p>
							</a>
						</div>
					</div>
		';
	}
?>
<?php
	if ($uri != '/acerca.php') {
		echo '		
					<div class="menu-item">
						<div class="in-block">
							<a class="link-a" href="acerca.php">
								<p>ACERCA</p>
							</a>
						</div>
					</div>
		';
	}
?>
<?php
	if ($uri != '/modelos.php') {
		echo '		
					<div class="menu-item" style="display:none">
						<div class="in-block">
							<a class="link-a" href="modelos.php">
								<p>MODELOS</p>
							</a>
						</div>
					</div>
		';
	}
?>
					<div class="menu-item">
						<div class="in-block">
							<div class="" style="padding-top:2em">
								<a class="" href="https://www.facebook.com/fahedstanicfoto/photos">
									<img style="max-width:30px;" src="img/web/ico_fb.png" alt="Enlace a página de Facebook.">
								</a>
								<a class="" href="https://500px.com/fahedsl">
									<img class="" style="max-width:30px;" src="img/web/ico_500px.png" alt="Enlace a página de 500px.">
								</a>
								<a class="" href="https://www.flickr.com/photos/fahedslfoto">
									<img style="max-width:30px;" src="img/web/ico_flickr.png" alt="Enlace a página de Flickr.">
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!--boton-->
			<div class="v20 h20 z30 vidrio fijo">
				<a id="boton" onclick="mi_func1()" class="z100 " href="javascript:void">
					<img id="boton_img" style="margin:2%; max-width:30px;" src="img/web/ico_menu.png" alt="Botón para abrir y cerrar el menú principal.">
				</a>
			</div>