<?php
	echo'
<!DOCTYPE html>
<html lang="es" 
xmlns:og="http://ogp.me/ns#"
xmlns:fb="http://www.facebook.com/2008/fbml">
	<head>
		<!--Meta tagss-->
		<link rel="canonical" href="http://www.fahedstanic.com" />
		<meta charset="UTF-8"/>
		<meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
		<meta name="title" content="Fahed Stanic&#39; Fotografía Arequipa" />
		<meta name="description" content="Fotógrafo de Arequipa, encantado de trabajar contigo en tu evento o negocio." />
		<meta name="ROBOTS" content="INDEX,FOLLOW">
		<title>Fahed Stanic&#39; Fotografía Arequipa</title>

		<link rel="icon" href="img/web/icono.png"/>
		<link rel="shortcut icon" href="img/web/icono.png">
		<link rel="image_src" href="http://www.fahedstanic.com/img/web/src_icono.png" />
		<!--Open graph-->
		<meta property="og:title" content="Fahed Stanic&#39; Fotografía Arequipa"/>
		<meta property="og:type" content="website"/>
	';

	$dir = 'http://www.fahedstanic.com';
	$uri = $_SERVER['REQUEST_URI'];
	$url = $dir.$uri;
	echo '<meta property="og:url" content="'.$url.'">';

	echo '
		<meta property="og:image" content="http://www.fahedstanic.com/img/web/src_icono.png"/>
		<meta property="og:description" content="Fotógrafo de Arequipa, encantado de trabajar contigo en tu evento o negocio."/>
		<meta property="fb:app_id" content="551267191730172"/>
		<!--Twitter cards-->
		<meta name="twitter:card" content="summary" />
		<meta name="twitter:title" content="Fahed Stanic&#39; Fotografía Arequipa">
		<meta name="twitter:description" content="Fotógrafo de Arequipa, encantado de trabajar contigo en tu evento o negocio.">
		<meta name="twitter:image" content="http://www.fahedstanic.com/img/web/src_icono.png">
		<meta name="twitter:site" content="@fahedstanic">
		<!--Pinterest-->
		<meta name="p:domain_verify" content="6635278ee438521293d4c0902ae900b4"/>

		<!--Estilos-->
		<link rel="stylesheet" type="text/css" href="css/estilo.css"/>
		<link rel="stylesheet" type="text/css" href="css/normal.css"/>
		<link rel="stylesheet" type="text/css" href="https://cdn.rawgit.com/sachinchoolur/lightgallery.js/master/dist/css/lightgallery.css"/>
		<!--JS-->
		<script src="https://code.jquery.com/jquery-2.2.3.min.js"></script>
		
		<script src="js/lightgallery.js"></script>

		<script src="js/imagesloaded.js"></script>
		<script src="js/masonry.js"></script>
		
		<script src="js/pre.js"></script>
		<script src="js/mi_func.js"></script>
	</head>
	';
?>
	<body onload="cargado()">
		<!--Facebook-->
		<script>
		  window.fbAsyncInit = function() {
			FB.init({
			  appId      : '551267191730172',
			  xfbml      : true,
			  version    : 'v2.7'
			});
		  };

		  (function(d, s, id){
			 var js, fjs = d.getElementsByTagName(s)[0];
			 if (d.getElementById(id)) {return;}
			 js = d.createElement(s); js.id = id;
			 js.src = "//connect.facebook.net/en_US/sdk.js";
			 fjs.parentNode.insertBefore(js, fjs);
		   }(document, 'script', 'facebook-jssdk'));
		</script>

<?php
	//Facebook;
	require('x-cargador.php');
?>

		<div class="marco">

<?php
	require('x-menu.php');
?>
			<div class="contenido">
<?php
	// pop up
	if(isset($_GET['msj'])){
		echo '	<div class="pop">
					<div class="popcont">'.$_GET['msj'].'</div>
				</div>';
	}
?>