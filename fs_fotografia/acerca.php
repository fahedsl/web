<?php
	require('php/x-cabecera.php');
?>

					<div class="negro fondo3 v100 h100 absoluto">
						<div>
							<h2 style="display:block; margin:2em auto 1em; text-align:center">ACERCA DE MI</h2>
							<div style="max-width:90%; margin:auto;">
								<img style="display:block;max-width:100%; max-height:100%; margin:1em auto 1em;" src="img/web/yo.jpg">
							</div>
							<div class="" style="padding-top:2em; padding-bottom:2em;text-align:center;">
								<p>Tipo raro de tiempo completo.</p>
								<p>Encantado por la fotografía.</p>
								<p>"Modelo" y fotógrafo. Intento de vegetariano.</p>
								<p>Malo cocinando y aún peor contando chistes.</p>
								<p>Amable y sociable.</p>
								<p>Creo que cada nueva sesión debe ser la mejor.</p>
								<p>Fan de los emojis :3</p>
							</div>
							<div style="display:block; margin:2em auto 1em; text-align:center">
								<a href="#">
									<em class="glyph fa-phone"></em>
									<span class="phone">940 235 704</span>
								</a>
							</div>
							<div style="display:block; margin:1em auto 2em; text-align:center">
								<a href="#">
									<em class="glyph fa-envelope"></em>
									<span>fahedsl@mail.com</span>
								</a>
							</div>
						</div>
					</div>

<?php
	require('php/x-pie.php');
?>
 
