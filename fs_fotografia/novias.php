<?php
	require('php/x-cabecera.php');
?>
		<div class="galeria">
			<div id="galeria" class="grid">
				<div class="grid-sizer"></div>
				<div class="grid-item" data-src="img/i1.jpg" data-sub-html="texto1">
					<div class="img_info">
						<div class="hmedio vmedio">
						</div>
					</div>
					<img src="img/i1.jpg">
				</div>
				<div class="grid-item" data-src="img/9.jpg" data-sub-html="texto1">
					<div class="img_info">
						<div class="hmedio vmedio">
						</div>
					</div>
					<img src="img/9.jpg">
				</div>
				<div class="grid-item" data-src="img/7.	jpg" data-sub-html="texto1">
					<div class="img_info">
						<div class="hmedio vmedio">
						</div>
					</div>
					<img src="img/7.jpg">
				</div>
				<div class="grid-item" data-src="img/i1.jpg" data-sub-html="texto1">
					<div class="img_info">
						<div class="hmedio vmedio">
						</div>
					</div>
					<img src="img/i1.jpg">
				</div>
				<div class="grid-item" data-src="img/i1.jpg" data-sub-html="texto1">
					<div class="img_info">
						<div class="hmedio vmedio">
						</div>
					</div>
					<img src="img/i1.jpg">
				</div>
			</div>
		</div>
		<script>
			var grid = document.querySelector('.grid');
			var msnry;

			imagesLoaded( grid, function() {
			  msnry = new Masonry( grid, {
				itemSelector: '.grid-item',
			  });
			});
		</script>

		<script>
		$(document).ready(function() {
			$("#galeria").lightGallery({
				download:false,
				loop:false,
			});
		});
		</script>
<?php
	require('php/x-pie.php');
?>