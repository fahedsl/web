--
-- Base de datos: `ericka`
--
CREATE DATABASE IF NOT EXISTS `ericka` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `ericka`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos`
--

CREATE TABLE `cursos` (
  `id` int(11) NOT NULL,
  `nombre` text COLLATE utf8_bin NOT NULL,
  `descrip` text COLLATE utf8_bin NOT NULL,
  `tipo` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `cursos`
--

INSERT INTO `cursos` (`id`, `nombre`, `descrip`, `tipo`) VALUES
(1, 'Beginners (A1+A2)', '<b>A1</b><br>\nCan understand and use familiar everyday expressions and very basic phrases aimed at the satisfaction of needs of a concrete type. Can introduce him/herself and others and can ask and answer questions about personal details such as where he/she lives, people he/she knows and things he/she has. Can interact in a simple way provided the other person talks slowly and clearly and is prepared to help.<br>\n<b>A2</b><br>\nCan understand sentences and frequently used expressions related to areas of most immediate relevance (e.g. very basic personal and family information, shopping, local geography, employment). Can communicate in simple and routine tasks requiring a simple and direct exchange of information on familiar and routine matters. Can describe in simple terms aspects of his/her background, immediate environment and matters in areas of immediate Basic need.<br>', 1),
(2, 'Pre-Intermediate (B1)', '<b>B1</b><br>\nCan understand the main points of clear standard input on familiar matters regularly encountered in work, school, leisure, etc. Can deal with most situations likely to arise whilst travelling in an area where the language is spoken. Can produce simple connected text on topics which are familiar or of personal interest. Can describe experiences and events, dreams, hopes and ambitions and briefly give reasons and explanations for opinions and plans.<br>', 1),
(3, 'Intermediate (B2)', '<b>B2</b><br>\nCan understand the main ideas of complex text on both concrete and abstract topics, including technical discussions in his/her field of specialisation. Can interact with a degree of fluency and spontaneity that makes regular interaction with native speakers quite possible without strain for either party. Can produce clear, detailed text on a wide range of subjects and explain a viewpoint on a topical issue giving the advantages and Independent disadvantages of various options.<br>', 1),
(4, '', '<p>\n	Courses for complete beginners through to advanced speakers.\n	Your will receive high quality and flexible Spanish lessons which can be: \n	1-to-1, Small groups, Face to face or Online.\n	<br>\n	All our courses are individually prepared for each student, depending on their requirement. \n	The length and frequency and timing of classes can be adjusted for each student.\n</p>\n<h3>Spanish for holiday</h3>\n<p>\n	If you are planning a holiday in Spain or visiting Latin America, this course is for you. Get ready to use your Spanish with confidence in  the typical situations of a holiday.\n</p>\n\n<h3>Conversational Spanish</h3>\n<p>\n	This classes are intended to improve your Spanish conversational skills using a variety of techniques and resources, including role-plays, games, videos, music and will be tailored to your level of Spanish from beginner to advanced. I will help you gain experience and confidence in the fun and enjoyable way.\n</p>\n\n<h3>Standard Spanish courses</h3>\n<p>These courses  are organised in four levels, fixing the goals for each based on our experience and in accordance with the Common European Framework of References.\n<p>', 2),
(5, 'Advanced (C1)', '<b>C1</b><br>Can understand a wide range of demanding, longer texts, and recognise implicit meaning.<br>Can express him/herself fluently and spontaneously without much obvious searching for expressions. Can use language flexibly and effectively for social, academic and professional purposes.<br>Can produce clear, well-structured, detailed text on complex subjects, showing controlled use of organisational patterns, connectors and cohesive devices.', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `info`
--

CREATE TABLE `info` (
  `id` int(11) NOT NULL,
  `pase` text COLLATE utf8_bin,
  `url` text COLLATE utf8_bin NOT NULL,
  `titulo` text COLLATE utf8_bin NOT NULL,
  `descripcion` text COLLATE utf8_bin NOT NULL,
  `facebookid` text COLLATE utf8_bin NOT NULL,
  `twitter` text COLLATE utf8_bin NOT NULL,
  `pinterest` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `info`
--

INSERT INTO `info` (`id`, `pase`, `url`, `titulo`, `descripcion`, `facebookid`, `twitter`, `pinterest`) VALUES
(1, 'holi', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `testimonios`
--

CREATE TABLE `testimonios` (
  `id` int(11) NOT NULL,
  `nombre` text COLLATE utf8_bin,
  `pais` text COLLATE utf8_bin NOT NULL,
  `foto` text COLLATE utf8_bin,
  `texto` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `testimonios`
--

INSERT INTO `testimonios` (`id`, `nombre`, `pais`, `foto`, `texto`) VALUES
(1, 'Stephanie Wilson', 'Canada', 'img/foto/1.png', 'I was fortunate to have Spanish lessons from Ericka, and would highly recommend her to anyone who wants to learn Spanish. Ericka was well-prepared with books, lessons, games, and media to help teach. This variety of methods made it quick and easy to learn. Her detailed and comprehensive explanations made it easy to learn and understand grammar, tenses and conjugations.<br>\nEricka was very flexible, both in scheduling the classes and in preparing or adjusting the content to suite my learning needs. She took extra time to go over difficult concepts, and didn\'t waste time on concepts I learned quickly.<br>\nClasses with Ericka were always fun and effective. I looked forward to attending classes.'),
(3, 'Gemma Burger', 'England', 'img/foto/2.png', 'My husband and I studied Spanish with Ericka for six months. She is a great teacher, who kept the lessons relevant and interesting. Our teacher is both professional and friendly making lessons a lot of fun. We went from speaking no Spanish at all to being able to happily converse in our daily lives.<br> We enjoyed learning Spanish. Thank you Ericka!'),
(4, 'Allison Bilas', 'USA', 'img/foto/3.png', 'What we have been able to learn is way beyond what I would have expected. The classes have been very organized and flexible to meet our needs. Ericka, our teacher has an amazing background and solid training in teaching the language.\nIn addition to speaking Spanish, we were able to learn about the Peruvian and Arequipenean culture first hand. The experience has been invaluable.'),
(5, 'Scott Bilas', 'USA', 'img/foto/4.png', 'Ericka, not only is the best Spanish teacher I have ever had, she has deep knowledge of her country\'s history and culture. She regularly integrated all of this into our Spanish lessons as learned.\nEricka is a talented teacher, has been flexible with our complicated schedules, and a good friend above all. I am very happy to have had this life-changing experience!'),
(6, 'Marianne Mathisen', 'Norway', 'img/foto/5.png', 'I am from Norway and I have lived in Arequipa, Perú for one year because I work here.   Since I work as a teacher for Norwegians I didn\'t have many opportunities to practice Spanish during my first year in Peru.<br>\r\nI am staying one more year in Peru, so I enrolled as a student with Ericka and I have learned a lot!! She is a very nice and gifted teacher who makes us learn through various activities from playing games to practical situations.  This Spanish course is great fun and very interesting, just perfect for me! I recommend Ericka as a teacher to everybody!   \r\nI am very, very satisfied.\r\n'),
(7, 'Tron Mathisen', 'Norway', 'img/foto/6.png', 'I am from Norway and I am a teacher.  I live in Arequipa and therefore I need to learn more Spanish. I feel that I understand the language to a certain extent, but I need to learn more.   With Ericka as my teacher I feel that I am learning a lot.<br>She "plays" the language in, and after my 30 years as a teacher I find it refreshing. I’ve learnt a lot from Ericka. She is a pleasant and very knowledgeable teacher.\nI recommend Ericka warmly as a teacher.'),
(8, 'Ross Cooper', 'Australia', 'img/foto/7.png', 'Ericka taught our family Spanish for six months while we were living in Peru.  She tailored her programs to match the ability and personality of each of each of us, including our three children (7, 5 and 4).  This made the lessons entertaining and enjoyable, and ensured that there was a high rate of retention of the material.  Not only did Ericka teach us Spanish, she also taught us a lot about Peru itself.<br>Muchas gracias Ericka por las clases en Español.'),
(9, 'Scott Smith', 'Canada', 'img/foto/8.png', 'Ericka Loayza was my Spanish teacher, she was very professional and very helpful in my studies.   She was always looking to customize our classes to help me with the areas that I needed to work on the most.<br>I would not hesitate to recommend Ericka as a teacher.'),
(10, 'George Hufford', 'USA', 'img/foto/9.png', 'Ericka was my Spanish tutor. She is patient and supportive of her students. She is fun to study with. She brings interesting and enjoyable topics to the studies. I would certainly recommend Ericka for any person needing personal instruction in Spanish.'),
(11, 'Elise Lovgren', 'Norway', 'img/foto/10.png', 'I am from Norway and am going to be a teacher in Peru for 2 years.\r\nMy husband and I have been attending taking a "survival course" for two weeks. We have learned a lot! Ericka, our teacher is so kind and helpful and she uses lots of different materials to make us understand. We have really enjoyed it! This course is great to everyone who wants to learn Spanish quickly I recommend it from my heart!'),
(12, 'Erlend Johansen', 'Norway', 'img/foto/11.png', 'I will be staying in Peru for two years and needed survival Spanish to start with. We have a very good teacher and provided me with a good foundation, from where I can continue learning. My teacher has been very patient with me and my poor language skills and I do highly recommend this course with her for anyone who wants to learn Spanish!'),
(13, 'James Grueneich', 'USA', 'img/foto/12.png', 'I received a week long Spanish Immersion course. I was skeptical at the beginning because I had been in Peru for only a very short time and did not speak any Spanish. Our teacher made us all feel comfortable and proceeded at a pace for all of us to learn and understand the basics of the language and culture.<br>\r\nBy the end of the week I was able to order at restaurants, give and ask for relevant information, make telephone calls, & do my shopping at the markets and stores without the help of other people. With the excellent course structure and patience of Ericka this course made my transition to Peru 100 times easier. Thank you.'),
(14, 'Jan Frode Ronning & Elisa Mastad', 'Norway', 'img/foto/13.png', 'We come from the beautiful country Norway and we haven\'t spoken Spanish before. We heard about Ericka through some friends, and we thought that studying Spanish would be something nice to do in our holiday. With the course we have learned a lot and are very satisfied.<br>\r\nWe will probably continue speaking Spanish to each other, and we already look forward to travel again to a Spanish speaking country.\r\nOur teacher has been very kind to us, extremely patient!<br>\r\nEricka is an excellent teacher.'),
(15, 'Meike Pfiel', 'Germany', 'img/foto/14.png', 'When I visited Arequipa I took 4 weeks 4 hours a day of Spanish lessons. They were very interesting and fun and I learned a lot. Ericka, is a very patient, experienced and informative teacher. The structure of the lessons and the exercises was very good, because two hours each day I had conversation and two hours each day I advanced my grammar and writing. My teacher presented very well structured lessons with good topics and exercises for my level of Spanish.<br>\r\nThe time with Ericka in class was a never ending barrel of good moments!');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `info`
--
ALTER TABLE `info`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `testimonios`
--
ALTER TABLE `testimonios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cursos`
--
ALTER TABLE `cursos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `info`
--
ALTER TABLE `info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `testimonios`
--
ALTER TABLE `testimonios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;--










-- Base de datos: `ie`
--
CREATE DATABASE IF NOT EXISTS `ie` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `ie`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cont`
--

CREATE TABLE `cont` (
  `id` int(11) NOT NULL,
  `nosotros_es` text COLLATE utf8_bin,
  `nosotros_en` text COLLATE utf8_bin,
  `staff_es` text COLLATE utf8_bin,
  `staff_en` text COLLATE utf8_bin,
  `slogan_es` text COLLATE utf8_bin,
  `slogan_en` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `cont`
--

INSERT INTO `cont` (`id`, `nosotros_es`, `nosotros_en`, `staff_es`, `staff_en`, `slogan_es`, `slogan_en`) VALUES
(1, 'Somos un Instituto de inglés. Nuestros fundadores-directores tienen 20 años de experiencia internacional combinada: en la educación del inglés como idioma extranjero (English as a Foreign Language - EFL) en todo América y Europa, enseñando a personas de 25 países.<br>Gracias a esto, contamos con las destrezas y habilidades suficientes para permitirnos enriquecer continuamente el conocimiento y experiencia de nuestros alumnos como la de nuestros profesores.', '(en inglés) Somos un Instituto de inglés. Nuestros fundadores-directores tienen 20 años de experiencia internacional combinada: en la educación del inglés como idioma extranjero (English as a Foreign Language - EFL) en todo América y Europa, enseñando a personas de 25 países.<br>Gracias a esto, contamos con las destrezas y habilidades suficientes para permitirnos enriquecer continuamente el conocimiento y experiencia de nuestros alumnos como la de nuestros profesores.', 'Contamos con personal docente que es, en su totalidad, extranjero y de habla inglesa. Cada uno de nuestros profesores, pasan por un riguroso proceso de selección y entrenamiento el cual es diseñado e impartido por uno de nuestros directores certificado por la Universidad de Cambridge de Inglaterra.', '(en inglés)Contamos con personal docente que es, en su totalidad, extranjero y de habla inglesa. Cada uno de nuestros profesores, pasan por un riguroso proceso de selección y entrenamiento el cual es diseñado e impartido por uno de nuestros directores certificado por la Universidad de Cambridge de Inglaterra.', 'Únicos en Arequipa con todos los <br>PROFESORES EXTRANJEROS.', 'The only one in Arequipa with only <br>FOREIGNER TEACHERS.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos`
--

CREATE TABLE `cursos` (
  `id` int(11) NOT NULL,
  `tit_es` text COLLATE utf8_bin,
  `cont_es` text COLLATE utf8_bin,
  `tit_en` text COLLATE utf8_bin,
  `cont_en` text COLLATE utf8_bin,
  `d1` text COLLATE utf8_bin,
  `h1` text COLLATE utf8_bin,
  `d2` text COLLATE utf8_bin,
  `h2` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `cursos`
--

INSERT INTO `cursos` (`id`, `tit_es`, `cont_es`, `tit_en`, `cont_en`, `d1`, `h1`, `d2`, `h2`) VALUES
(1, 'Curso Intensivo', 'Para todas las personas que necesitan seguir con su crecimiento profesional y sobre todo laboral, estudiando 20 meses dos horas diarias, de lunes a jueves en  grupos personalizados de 6 alumnos máximo por aula. ', 'Intensive Course', '(En ingles)Para todas las personas que necesitan seguir con su crecimiento profesional y sobre todo laboral, estudiando 20 meses dos horas diarias, de lunes a jueves y  grupos personalizados.', 'Lunes - Jueves', '(8h-12h; 17h-21h)', 'Monday - Thursday', '(8h-12h; 17h-21h)'),
(2, 'Curso Super Intensivo', 'Para todas las personas que necesitan seguir con su crecimiento profesional y sobre todo laboral, estudiando 10 meses cuatro (4 horas ) horas diarias de lunes a jueves en  grupos personalizados de 6 alumnos por aulas.\r\n<br>\r\n<br>\r\n La forma segura de aprender el idioma con profesores extranjeros', 'Intensive Course', '(En ingles)Para todas las personas que necesitan seguir con su crecimiento profesional y sobre todo laboral, estudiando 20 meses dos horas diarias, de lunes a jueves y  grupos personalizados.', 'Lunes - Jueves ', 'turno mañana, turno tarde, turno noche.', 'Monday - Thursday', 'turno mañana, turno tarde, turno noche.'),
(3, 'Curso Kids', 'Aprende de la manera más rápida y divertida, con profesores extranjeros calificados para la enseñanza de niños.\r\n<br>\r\n<br>\r\nMaterial y temas adecuerdo a la edad.\r\n<br>\r\n<br>\r\nPOR VERANO GRUPOS ESPECIALES \r\n<br>\r\n<br>\r\n* 8 alumnos máximo por aula\r\n<br>\r\n* Edades de 6 a 13 años\r\n<br>\r\n<br>\r\nAÑO ACADEMICO\r\n<br>\r\n<br>\r\nTodos los sábados \r\n<br>\r\n\r\n\r\n\r\n', 'Kids', ' ', 'Lunes a Jueves ', '(9h-11:30 1.3h - Verano)', 'Monday to Thursday', '(9h-11:30 1.3h - Verano)'),
(4, 'Preparación Examenes Oficiales', 'Conocidos también como examemes Internacionales por que son evaluaciones con valor internacional y donde se refleja el verdadero conocimiento de Listening, Reading, Speaking y Writing del idioma inglés. \r\n<br>\r\n<br>\r\nLos examenes Oficiales son requeridos por las Universidades locales e internacionales, centros de trabajo o embajadas de habla ingles.\r\n<br>\r\n<br>\r\nTrabajamos independientemente con cada alumno y en grupos de 4 alumnos por aula; logrando que cada alumno alcance el puntaje requerido según la meta que tenga.\r\n<br>\r\n<br>\r\nLos Examenes Oficiales tienen una vigencia de 2 años.', ' ', ' ', 'De lunes a Jueves', '8h-10h; 10h-12h; 17h-19h; 19h-21h ', ' ', '8h-10h; 10h-12h; 17h-19h; 19h-21h '),
(5, 'Curso para Ejecutivos', 'El Curso exclusivo para ejecutivos y/o profesionales es flexible y permite que el participante pueda aprender el inglés en un ambiente adecuado a su nivel. \r\n<br>\r\n<br>\r\nEl tiempo para completarlo es de dos años como máximo, contados a partir del primer Nivel del inglés. Si el participante ya tiene algún conocimiento el tiempo será menor. \r\n<br>\r\n<br>\r\nLos horarios son perfectos para personas que trabajan durante todo el día, y que no desean seguir postergando el aprendizaje del inglés por falta de tiempo.\r\n<br>\r\n<br>\r\nAprende de manera rápida y exigete\r\n', ' ', '  ', 'Lunes - Miércoles - Viernes / Martes - Jueves', '(19h-21h) / (18h-21h)', ' ', '(19h-21h) / (18h-21h)'),
(6, 'Clases Privadas ', 'El curso de Clases Particulares es el método más rápido para aprender un idioma, por que se trabaja según las necesidades, prioridades y la disponibilidad de tiempo del alumno.\r\n<br>\r\n<br>\r\nEl sistema y material de trabajo es dinámico que consta de un 50% de la clase en la gramática  y el otro 50% es la practica.\r\n<br>\r\n<br>\r\nEl alumno en sus Clases Privadas elige lo que desea aprender (Inglés General, preparación Exámenes Oficiales, Entrevistas de trabajo, reforzamiento de algún tema especifico, etc.)\r\n<br>\r\n<br>\r\nEn las Clases Privadas el alumno elige el horario y lugar para el dictado de clases', ' ', ' ', 'libre', ' ', ' ', ' ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `info`
--

CREATE TABLE `info` (
  `id` int(11) NOT NULL,
  `URL` text COLLATE utf8_bin,
  `titulo` text COLLATE utf8_bin,
  `contenido` text COLLATE utf8_bin,
  `icono` text COLLATE utf8_bin,
  `facebook_sitio` text COLLATE utf8_bin,
  `facebookid` text COLLATE utf8_bin,
  `twitter` text COLLATE utf8_bin,
  `pinterest` text COLLATE utf8_bin,
  `telefono1` text COLLATE utf8_bin,
  `telefono2` text COLLATE utf8_bin,
  `telefono3` text COLLATE utf8_bin,
  `email` text COLLATE utf8_bin,
  `pase` text COLLATE utf8_bin,
  `pase2` text COLLATE utf8_bin,
  `stop` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `info`
--

INSERT INTO `info` (`id`, `URL`, `titulo`, `contenido`, `icono`, `facebook_sitio`, `facebookid`, `twitter`, `pinterest`, `telefono1`, `telefono2`, `telefono3`, `email`, `pase`, `pase2`, `stop`) VALUES
(1, 'http://www.internationalenglish.org.pe', 'INTERNATIONAL ENGLISH', 'Instituto de inglés con todos los profesores extranjeros.', 'img/web/ie1.svg', 'https://www.facebook.com/internationalenglish.arequipa', '574751259400446', '', '', '051-54-253080', '959363615', '947693636', 'info@internationalenglish.org.pe', 'holi', 'holi', 'no');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inicio`
--

CREATE TABLE `inicio` (
  `id` int(11) NOT NULL,
  `img` text COLLATE utf8_bin,
  `url` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `inicio`
--

INSERT INTO `inicio` (`id`, `img`, `url`) VALUES
(1, 'img/inicio/1.jpg', 'http://www.internationalenglish.org.pe/cursos.php?i=2'),
(2, 'img/inicio/2.jpg', 'http://www.internationalenglish.org.pe/cursos.php?i=3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE `pais` (
  `id` int(11) NOT NULL,
  `pais_es` text COLLATE utf8_bin,
  `pais_en` text COLLATE utf8_bin,
  `bandera` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`id`, `pais_es`, `pais_en`, `bandera`) VALUES
(1, NULL, 'afghanistan', 'img/bandera/afghanistan.gif'),
(2, NULL, 'aland islands', 'img/bandera/aland islands.gif'),
(3, NULL, 'albania', 'img/bandera/albania.gif'),
(4, NULL, 'algeria', 'img/bandera/algeria.gif'),
(5, NULL, 'american samoa', 'img/bandera/american samoa.gif'),
(6, NULL, 'andorra', 'img/bandera/andorra.gif'),
(7, NULL, 'angola', 'img/bandera/angola.gif'),
(8, NULL, 'anguilla', 'img/bandera/anguilla.gif'),
(9, NULL, 'antarctica', 'img/bandera/antarctica.gif'),
(10, NULL, 'antigua and barbuda', 'img/bandera/antigua and barbuda.gif'),
(11, NULL, 'argentina', 'img/bandera/argentina.gif'),
(12, NULL, 'armenia', 'img/bandera/armenia.gif'),
(13, NULL, 'aruba', 'img/bandera/aruba.gif'),
(14, NULL, 'australia', 'img/bandera/australia.gif'),
(15, NULL, 'austria', 'img/bandera/austria.gif'),
(16, NULL, 'azerbaijan', 'img/bandera/azerbaijan.gif'),
(17, NULL, 'bahamas', 'img/bandera/bahamas.gif'),
(18, NULL, 'bahrain', 'img/bandera/bahrain.gif'),
(19, NULL, 'bangladesh', 'img/bandera/bangladesh.gif'),
(20, NULL, 'barbados', 'img/bandera/barbados.gif'),
(21, NULL, 'belarus', 'img/bandera/belarus.gif'),
(22, NULL, 'belgium', 'img/bandera/belgium.gif'),
(23, NULL, 'belize', 'img/bandera/belize.gif'),
(24, NULL, 'benin', 'img/bandera/benin.gif'),
(25, NULL, 'bermuda', 'img/bandera/bermuda.gif'),
(26, NULL, 'bhutan', 'img/bandera/bhutan.gif'),
(27, NULL, 'bolivia', 'img/bandera/bolivia.gif'),
(28, NULL, 'bonaire', 'img/bandera/bonaire.gif'),
(29, NULL, 'bosnia and herzegovina', 'img/bandera/bosnia and herzegovina.gif'),
(30, NULL, 'botswana', 'img/bandera/botswana.gif'),
(31, NULL, 'bouvet island', 'img/bandera/bouvet island.gif'),
(32, NULL, 'brazil', 'img/bandera/brazil.gif'),
(33, NULL, 'british indian ocean territory', 'img/bandera/british indian ocean territory.gif'),
(34, NULL, 'brunei', 'img/bandera/brunei.gif'),
(35, NULL, 'bulgaria', 'img/bandera/bulgaria.gif'),
(36, NULL, 'burkina faso', 'img/bandera/burkina faso.gif'),
(37, NULL, 'burundi', 'img/bandera/burundi.gif'),
(38, NULL, 'cambodia', 'img/bandera/cambodia.gif'),
(39, NULL, 'cameroon', 'img/bandera/cameroon.gif'),
(40, NULL, 'canada', 'img/bandera/canada.gif'),
(41, NULL, 'cape verde', 'img/bandera/cape verde.gif'),
(42, NULL, 'cayman islands', 'img/bandera/cayman islands.gif'),
(43, NULL, 'central african republic', 'img/bandera/central african republic.gif'),
(44, NULL, 'chad', 'img/bandera/chad.gif'),
(45, NULL, 'chile', 'img/bandera/chile.gif'),
(46, NULL, 'china', 'img/bandera/china.gif'),
(47, NULL, 'christmas island', 'img/bandera/christmas island.gif'),
(48, NULL, 'cocos islands', 'img/bandera/cocos islands.gif'),
(49, NULL, 'colombia', 'img/bandera/colombia.gif'),
(50, NULL, 'comoros', 'img/bandera/comoros.gif'),
(51, NULL, 'cook islands', 'img/bandera/cook islands.gif'),
(52, NULL, 'costa rica', 'img/bandera/costa rica.gif'),
(53, NULL, 'cote d Ivoire', 'img/bandera/cote d Ivoire.gif'),
(54, NULL, 'croatia', 'img/bandera/croatia.gif'),
(55, NULL, 'cuba', 'img/bandera/cuba.gif'),
(56, NULL, 'curacao', 'img/bandera/curacao.gif'),
(57, NULL, 'cyprus', 'img/bandera/cyprus.gif'),
(58, NULL, 'czech republic', 'img/bandera/czech republic.gif'),
(59, NULL, 'democratic republic of the congo', 'img/bandera/democratic republic of the congo.gif'),
(60, NULL, 'denmark', 'img/bandera/denmark.gif'),
(61, NULL, 'djibouti', 'img/bandera/djibouti.gif'),
(62, NULL, 'dominica', 'img/bandera/dominica.gif'),
(63, NULL, 'dominican republic', 'img/bandera/dominican republic.gif'),
(64, NULL, 'east timor', 'img/bandera/east timor.gif'),
(65, NULL, 'ecuador', 'img/bandera/ecuador.gif'),
(66, NULL, 'egypt', 'img/bandera/egypt.gif'),
(67, NULL, 'el salvador', 'img/bandera/el salvador.gif'),
(68, NULL, 'england', 'img/bandera/england.gif'),
(69, NULL, 'equatorial guinea', 'img/bandera/equatorial guinea.gif'),
(70, NULL, 'eritrea', 'img/bandera/eritrea.gif'),
(71, NULL, 'estonia', 'img/bandera/estonia.gif'),
(72, NULL, 'ethiopia', 'img/bandera/ethiopia.gif'),
(73, NULL, 'european union', 'img/bandera/european union.gif'),
(74, NULL, 'falkland islands', 'img/bandera/falkland islands.gif'),
(75, NULL, 'faroe islands', 'img/bandera/faroe islands.gif'),
(76, NULL, 'fiji', 'img/bandera/fiji.gif'),
(77, NULL, 'finland', 'img/bandera/finland.gif'),
(78, NULL, 'france', 'img/bandera/france.gif'),
(79, NULL, 'french guiana', 'img/bandera/french guiana.gif'),
(80, NULL, 'french polynesia', 'img/bandera/french polynesia.gif'),
(81, NULL, 'french southern territories', 'img/bandera/french southern territories.gif'),
(82, NULL, 'gabon', 'img/bandera/gabon.gif'),
(83, NULL, 'gambia', 'img/bandera/gambia.gif'),
(84, NULL, 'georgia', 'img/bandera/georgia.gif'),
(85, NULL, 'germany', 'img/bandera/germany.gif'),
(86, NULL, 'ghana', 'img/bandera/ghana.gif'),
(87, NULL, 'gibraltar', 'img/bandera/gibraltar.gif'),
(88, NULL, 'greece', 'img/bandera/greece.gif'),
(89, NULL, 'greenland', 'img/bandera/greenland.gif'),
(90, NULL, 'grenada', 'img/bandera/grenada.gif'),
(91, NULL, 'guadeloupe', 'img/bandera/guadeloupe.gif'),
(92, NULL, 'guam', 'img/bandera/guam.gif'),
(93, NULL, 'guatemala', 'img/bandera/guatemala.gif'),
(94, NULL, 'guernsey', 'img/bandera/guernsey.gif'),
(95, NULL, 'guinea bissau', 'img/bandera/guinea bissau.gif'),
(96, NULL, 'guinea', 'img/bandera/guinea.gif'),
(97, NULL, 'guyana', 'img/bandera/guyana.gif'),
(98, NULL, 'haiti', 'img/bandera/haiti.gif'),
(99, NULL, 'heard island and mcdonald islands', 'img/bandera/heard island and mcdonald islands.gif'),
(100, NULL, 'honduras', 'img/bandera/honduras.gif'),
(101, NULL, 'hong kong', 'img/bandera/hong kong.gif'),
(102, NULL, 'hungary', 'img/bandera/hungary.gif'),
(103, NULL, 'iceland', 'img/bandera/iceland.gif'),
(104, NULL, 'india', 'img/bandera/india.gif'),
(105, NULL, 'indonesia', 'img/bandera/indonesia.gif'),
(106, NULL, 'iran', 'img/bandera/iran.gif'),
(107, NULL, 'iraq', 'img/bandera/iraq.gif'),
(108, NULL, 'ireland', 'img/bandera/ireland.gif'),
(109, NULL, 'isle of man', 'img/bandera/isle of man.gif'),
(110, NULL, 'israel', 'img/bandera/israel.gif'),
(111, NULL, 'italy', 'img/bandera/italy.gif'),
(112, NULL, 'jamaica', 'img/bandera/jamaica.gif'),
(113, NULL, 'japan', 'img/bandera/japan.gif'),
(114, NULL, 'jersey', 'img/bandera/jersey.gif'),
(115, NULL, 'jordan', 'img/bandera/jordan.gif'),
(116, NULL, 'kazakhstan', 'img/bandera/kazakhstan.gif'),
(117, NULL, 'kenya', 'img/bandera/kenya.gif'),
(118, NULL, 'kiribati', 'img/bandera/kiribati.gif'),
(119, NULL, 'korea north', 'img/bandera/korea north.gif'),
(120, NULL, 'korea south', 'img/bandera/korea south.gif'),
(121, NULL, 'kosovo', 'img/bandera/kosovo.gif'),
(122, NULL, 'kuwait', 'img/bandera/kuwait.gif'),
(123, NULL, 'kyrgyzstan', 'img/bandera/kyrgyzstan.gif'),
(124, NULL, 'laos', 'img/bandera/laos.gif'),
(125, NULL, 'latvia', 'img/bandera/latvia.gif'),
(126, NULL, 'lebanon', 'img/bandera/lebanon.gif'),
(127, NULL, 'lesotho', 'img/bandera/lesotho.gif'),
(128, NULL, 'liberia', 'img/bandera/liberia.gif'),
(129, NULL, 'libya', 'img/bandera/libya.gif'),
(130, NULL, 'liechtenstein', 'img/bandera/liechtenstein.gif'),
(131, NULL, 'lithuania', 'img/bandera/lithuania.gif'),
(132, NULL, 'luxembourg', 'img/bandera/luxembourg.gif'),
(133, NULL, 'macao', 'img/bandera/macao.gif'),
(134, NULL, 'macedonia', 'img/bandera/macedonia.gif'),
(135, NULL, 'madagascar', 'img/bandera/madagascar.gif'),
(136, NULL, 'malawi', 'img/bandera/malawi.gif'),
(137, NULL, 'malaysia', 'img/bandera/malaysia.gif'),
(138, NULL, 'maldives', 'img/bandera/maldives.gif'),
(139, NULL, 'mali', 'img/bandera/mali.gif'),
(140, NULL, 'malta', 'img/bandera/malta.gif'),
(141, NULL, 'marshall islands', 'img/bandera/marshall islands.gif'),
(142, NULL, 'martinique', 'img/bandera/martinique.gif'),
(143, NULL, 'mauritania', 'img/bandera/mauritania.gif'),
(144, NULL, 'mauritius', 'img/bandera/mauritius.gif'),
(145, NULL, 'mayotte', 'img/bandera/mayotte.gif'),
(146, NULL, 'mexico', 'img/bandera/mexico.gif'),
(147, NULL, 'micronesia', 'img/bandera/micronesia.gif'),
(148, NULL, 'moldova', 'img/bandera/moldova.gif'),
(149, NULL, 'monaco', 'img/bandera/monaco.gif'),
(150, NULL, 'mongolia', 'img/bandera/mongolia.gif'),
(151, NULL, 'montenegro', 'img/bandera/montenegro.gif'),
(152, NULL, 'montserrat', 'img/bandera/montserrat.gif'),
(153, NULL, 'morocco', 'img/bandera/morocco.gif'),
(154, NULL, 'mozambique', 'img/bandera/mozambique.gif'),
(155, NULL, 'myanmar', 'img/bandera/myanmar.gif'),
(156, NULL, 'namibia', 'img/bandera/namibia.gif'),
(157, NULL, 'nauru', 'img/bandera/nauru.gif'),
(158, NULL, 'nepal', 'img/bandera/nepal.gif'),
(159, NULL, 'netherlands', 'img/bandera/netherlands.gif'),
(160, NULL, 'new caledonia', 'img/bandera/new caledonia.gif'),
(161, NULL, 'new zealand', 'img/bandera/new zealand.gif'),
(162, NULL, 'nicaragua', 'img/bandera/nicaragua.gif'),
(163, NULL, 'niger', 'img/bandera/niger.gif'),
(164, NULL, 'nigeria', 'img/bandera/nigeria.gif'),
(165, NULL, 'niue', 'img/bandera/niue.gif'),
(166, NULL, 'norfolk island', 'img/bandera/norfolk island.gif'),
(167, NULL, 'northern mariana islands', 'img/bandera/northern mariana islands.gif'),
(168, NULL, 'norway', 'img/bandera/norway.gif'),
(169, NULL, 'oman', 'img/bandera/oman.gif'),
(170, NULL, 'pakistan', 'img/bandera/pakistan.gif'),
(171, NULL, 'palau', 'img/bandera/palau.gif'),
(172, NULL, 'palestinian territory', 'img/bandera/palestinian territory.gif'),
(173, NULL, 'panama', 'img/bandera/panama.gif'),
(174, NULL, 'papua new guinea', 'img/bandera/papua new guinea.gif'),
(175, NULL, 'paraguay', 'img/bandera/paraguay.gif'),
(176, NULL, 'peru', 'img/bandera/peru.gif'),
(177, NULL, 'philippines', 'img/bandera/philippines.gif'),
(178, NULL, 'pitcairn islands', 'img/bandera/pitcairn islands.gif'),
(179, NULL, 'poland', 'img/bandera/poland.gif'),
(180, NULL, 'portugal', 'img/bandera/portugal.gif'),
(181, NULL, 'puerto rico', 'img/bandera/puerto rico.gif'),
(182, NULL, 'qatar', 'img/bandera/qatar.gif'),
(183, NULL, 'republic of china', 'img/bandera/republic of china.gif'),
(184, NULL, 'republic of the congo', 'img/bandera/republic of the congo.gif'),
(185, NULL, 'reunion', 'img/bandera/reunion.gif'),
(186, NULL, 'romania', 'img/bandera/romania.gif'),
(187, NULL, 'russia', 'img/bandera/russia.gif'),
(188, NULL, 'rwanda', 'img/bandera/rwanda.gif'),
(189, NULL, 'saint barthelemy', 'img/bandera/saint barthelemy.gif'),
(190, NULL, 'saint helena', 'img/bandera/saint helena.gif'),
(191, NULL, 'saint kitts and nevis', 'img/bandera/saint kitts and nevis.gif'),
(192, NULL, 'saint lucia', 'img/bandera/saint lucia.gif'),
(193, NULL, 'saint martin', 'img/bandera/saint martin.gif'),
(194, NULL, 'saint pierre and miquelon', 'img/bandera/saint pierre and miquelon.gif'),
(195, NULL, 'saint vincent and the grenadines', 'img/bandera/saint vincent and the grenadines.gif'),
(196, NULL, 'samoa', 'img/bandera/samoa.gif'),
(197, NULL, 'san marino', 'img/bandera/san marino.gif'),
(198, NULL, 'sao tome and principe', 'img/bandera/sao tome and principe.gif'),
(199, NULL, 'saudi arabia', 'img/bandera/saudi arabia.gif'),
(200, NULL, 'scotland', 'img/bandera/scotland.gif'),
(201, NULL, 'senegal', 'img/bandera/senegal.gif'),
(202, NULL, 'serbia', 'img/bandera/serbia.gif'),
(203, NULL, 'seychelles', 'img/bandera/seychelles.gif'),
(204, NULL, 'sierra leone', 'img/bandera/sierra leone.gif'),
(205, NULL, 'singapore', 'img/bandera/singapore.gif'),
(206, NULL, 'sint maarten', 'img/bandera/sint maarten.gif'),
(207, NULL, 'slovakia', 'img/bandera/slovakia.gif'),
(208, NULL, 'slovenia', 'img/bandera/slovenia.gif'),
(209, NULL, 'solomon islands', 'img/bandera/solomon islands.gif'),
(210, NULL, 'somalia', 'img/bandera/somalia.gif'),
(211, NULL, 'south africa', 'img/bandera/south africa.gif'),
(212, NULL, 'south georgia and the south sandwich islands', 'img/bandera/south georgia and the south sandwich islands.gif'),
(213, NULL, 'south sudan', 'img/bandera/south sudan.gif'),
(214, NULL, 'spain', 'img/bandera/spain.gif'),
(215, NULL, 'sri lanka', 'img/bandera/sri lanka.gif'),
(216, NULL, 'sudan', 'img/bandera/sudan.gif'),
(217, NULL, 'suriname', 'img/bandera/suriname.gif'),
(218, NULL, 'svalbard and jan mayen', 'img/bandera/svalbard and jan mayen.gif'),
(219, NULL, 'swaziland', 'img/bandera/swaziland.gif'),
(220, NULL, 'sweden', 'img/bandera/sweden.gif'),
(221, NULL, 'switzerland', 'img/bandera/switzerland.gif'),
(222, NULL, 'syria', 'img/bandera/syria.gif'),
(223, NULL, 'tajikistan', 'img/bandera/tajikistan.gif'),
(224, NULL, 'tanzania', 'img/bandera/tanzania.gif'),
(225, NULL, 'thailand', 'img/bandera/thailand.gif'),
(226, NULL, 'togo', 'img/bandera/togo.gif'),
(227, NULL, 'tokelau', 'img/bandera/tokelau.gif'),
(228, NULL, 'tonga', 'img/bandera/tonga.gif'),
(229, NULL, 'trinidad and tobago', 'img/bandera/trinidad and tobago.gif'),
(230, NULL, 'tunisia', 'img/bandera/tunisia.gif'),
(231, NULL, 'turkey', 'img/bandera/turkey.gif'),
(232, NULL, 'turkmenistan', 'img/bandera/turkmenistan.gif'),
(233, NULL, 'turks and caicos islands', 'img/bandera/turks and caicos islands.gif'),
(234, NULL, 'tuvalu', 'img/bandera/tuvalu.gif'),
(235, NULL, 'uganda', 'img/bandera/uganda.gif'),
(236, NULL, 'ukraine', 'img/bandera/ukraine.gif'),
(237, NULL, 'united arab emirates', 'img/bandera/united arab emirates.gif'),
(238, NULL, 'united kingdom', 'img/bandera/united kingdom.gif'),
(239, NULL, 'united states of america', 'img/bandera/united states of america.gif'),
(240, NULL, 'uruguay', 'img/bandera/uruguay.gif'),
(241, NULL, 'ussr', 'img/bandera/ussr.gif'),
(242, NULL, 'uzbekistan', 'img/bandera/uzbekistan.gif'),
(243, NULL, 'vanuatu', 'img/bandera/vanuatu.gif'),
(244, NULL, 'vatican city', 'img/bandera/vatican city.gif'),
(245, NULL, 'venezuela', 'img/bandera/venezuela.gif'),
(246, NULL, 'vietnam', 'img/bandera/vietnam.gif'),
(247, NULL, 'virgin islands british', 'img/bandera/virgin islands british.gif'),
(248, NULL, 'virgin islands us', 'img/bandera/virgin islands us.gif'),
(249, NULL, 'wales', 'img/bandera/wales.gif'),
(250, NULL, 'wallis and futuna', 'img/bandera/wallis and futuna.gif'),
(251, NULL, 'western sahara', 'img/bandera/western sahara.gif'),
(252, NULL, 'yemen', 'img/bandera/yemen.gif'),
(253, NULL, 'zambia', 'img/bandera/zambia.gif'),
(254, NULL, 'zimbabwe', 'img/bandera/zimbabwe.gif');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE `personas` (
  `id` int(11) NOT NULL,
  `tipo` int(1) DEFAULT NULL,
  `nombre` text COLLATE utf8_bin,
  `email` text COLLATE utf8_bin,
  `mensaje` text COLLATE utf8_bin,
  `telf` text COLLATE utf8_bin,
  `visto` varchar(2) COLLATE utf8_bin DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `personas`
--

INSERT INTO `personas` (`id`, `tipo`, `nombre`, `email`, `mensaje`, `telf`, `visto`) VALUES
(1, 1, 'logina', 'pranigol1@hotmail.com', 'blalabalala\r\n', '', 'no');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesores`
--

CREATE TABLE `profesores` (
  `id` int(11) NOT NULL,
  `nombre` text COLLATE utf8_bin,
  `foto` text COLLATE utf8_bin,
  `pais` text COLLATE utf8_bin,
  `edad` int(11) DEFAULT NULL,
  `texto_es` text COLLATE utf8_bin,
  `texto_en` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `profesores`
--

INSERT INTO `profesores` (`id`, `nombre`, `foto`, `pais`, `edad`, `texto_es`, `texto_en`) VALUES
(1, 'Diana ', 'img/profe/foto/1.jpg', 'england', 27, 'Mi nombre es Diana Oviedo. Soy de Arequipa, Perú. Aprendí el inglés estudiando y viviendo en Inglaterra. En Perú estudié el TEFL para ser profesora. Voy enseñando inglés más de 5 años. He tenido alumnos de distintas edades; desde niños de jardín hasta alumnos para preparación TOEFL y otros exámenes internacionales.\r\n<ver> \r\nMe gusta enseñar inglés porque la docencia es una carrera que siempre me ha llamado la atención; uno nunca para de aprender. El inglés es uno de los idiomas que más me atrae tanto por su pronunciación como por su gramática y enseñarlo es bastante divertido. \r\n', 'My name is Diana Oviedo. I am from Arequipa, Peru. I learnt English by studying and living in England. In Peru I studied TEFL to become a teacher. I have been teaching English for 5 years. I have had students of different ages; from kindergarten children to students for TOEFL and other international exams.\r\n<ver>\r\nI like to teach English because teaching is a profession which has always caught my attention; one never stops learning. English is one of the most attractive languages to me because of its pronunciation and grammar and teaching it is a lot of fun.\r\n\r\n\r\n'),
(2, 'SOMA ', 'img/profe/foto/2.jpg', 'hungary', 0, 'Makai-Boloni, de Hungría, estudió psicología en inglés en la Universidad de Groningen (Holanda), enseñó durante dos años (2014-2016) en la Universidad de Groningen escritura académica, pensamiento crítico y métodos de investigación.\r\n\r\nEstoy enseñando inglés en Perú durante mi licenciatura en psicología, creciendo significativamente tanto personal como profesionalmente. Estoy a punto de graduarme de una gran universidad y he conocido a gente increíble e inspiradora durante este viaje. Sin embargo, la única razón por la que estudiar en un país extranjero fue posible es porque tuve la suerte de aprender inglés desde temprana edad. En la sociedad contemporánea, hablar inglés sirve como una línea de base igual para muchas oportunidades y posibilidades. Por lo tanto, me gustaría dar un paso para ayudar a otros obtener estas oportunidades. Por otra parte, me gustaría ayudar a la gente lograr sus sueños y sacarlos de sus zonas de comodidad (ya que aprender un nuevo idioma puede ser muy frustrante). En este punto, puedo afirmar con seguridad que la enseñanza es una de mis grandes pasiones y que estoy tratando de convertirme en un maestro de tiempo completo más adelante en mi carrera. El inglés es una de las lenguas más habladas. Dominar este lenguaje, en la era de la globalización es aún más crucial de lo que ya ha sido antes. Estoy motivado para abordar esta cuestión en su esencia. La enseñanza de lenguas requiere un complejo conjunto de habilidades: los profesores tienen que desafiar constantemente e introducir nuevas ideas en las intuiciones profundamente arraigadas de los estudiantes. Aparte de eso, también hay mucho que yo debo aprender. Para resumir, el inglés debe ser para todos. Es un lenguaje bastante simple, que puede ser fácilmente fomentado en todo el mundo. Soy consciente de que no voy a cambiar el mundo solo, pero creo que podría tener un impacto positivo en las personas a quienes enseño.\r\n', 'Makai-Boloni, from Hungary, studied psychology in English at the University of Groningen (Holland), taught for two years (2014-2016) at the University of Groningen academic writing, critical thinking skills and research methods. \r\n\r\nTeaching English in Peru 1 Soma Makai-Boloni Teaching English During my bachelor in psychology, I grew significantly both personally and professionally. I am about to graduate from a great university and I met amazing, inspiring people during this journey. However, the only reason that studying in a foreign country was possible is that I was lucky enough to learn English from an early age on. In contemporary society, speaking English serves as an equal baseline for many opportunities and possibilities. Hence, I would like to take a step towards helping others to these equal opportunities. Moreover, I would like to assist people in achieving their dreams and in moving out of their comfort zones (since learning a new language can be quite frustrating). At this point, I can safely state that teaching is one of my great passions and that I am aiming at becoming a full time teacher later in my career. English is one of the most spoken languages. Mastering this language, in the era of globalization is even more crucial than it has already been before. We are overloaded with information, media coverages are almost entirely in English and most job recruiters (of course, it depends on the region) prefer an English speaking person over a non-speaking one. I am motivated to tackle this issue at its core. Language teaching requires a complex set of skills: teachers have to constantly challenge and introduce new ideas into students’ deeply rooted intuitions. Moreover, teachers should also possess effective group-management and communication skills. Since I have studied psychology, I would feel sensitive when introducing new material and I already have experience in group supervision as well as teaching. Furthermore, instructors ought to transpose complex linguistic ideas into small manageable chunks for their less experienced students. To ease this process I would monitor students’ progress, both on an individual as well as on a collective level. I would be enthusiastic, encouraging, patient and innovative. In addition to that, there is also a lot to learn for me, especially when it comes to working with beginner groups. To sum it up, English should be for everyone. It is a fairly simple language, which can be easily fostered across the globe. I am aware that I won’t change the world by myself, but I think I could have a positive impact on the people I will teach, on my work environment and on one of Arequipa’s small neighbourhoods. Speaking of which… Peru I am lead by two major motivational forces: the first being the people living in Peru, and secondly, the rich, historical culture of the country. I was always intrigued by Latin American Teaching English in Peru 2 Soma Makai-Boloni cultures. To begin with, the level of warmth and joy with which people live their lives is absolutely fascinating. In my opinion, I could easily fit in this environment. Also, I have met several people from the area and a common pattern emerged: people embraced a value system that puts friends and family above success and money. An outlook that makes living, laughing and loving in the moment rather than putting happiness on hold. Finally, in Peru I will be always surrounded by history and its effects on the culture, whether it is through visiting a museum, wandering around a town or village, or hiking to one of the hidden Inca ruins. As a history loving person, I will be more than happy explore Peru’s cultural diversity. I am convinced that there is much more to the story than what I have read in books, and the only way to find out about this is to experience it myself.\r\n'),
(3, 'Summer', 'img/profe/foto/3.jpg', 'united states of america', 0, 'Soy de Ocean Springs, Mississippi, Estados Unidos. Estudié ciencias en la Universidad de Alabama del Sur. Recibí un título de Licenciada en Ciencias. Más tarde pasé por un programa de enseñanza alternativa y recibí mi Certificado de Maestra en Texas. Mientras vivía en la frontera de México y Texas enseñé todas las ciencias de la escuela secundaria y educación física. La mayoría de mis estudiantes eran estudiantes de ESL (Inglés como segunda lengua). Lo que significa que eran aprendices del idioma inglés. Me enamoré ayudando a mis estudiantes a mejorar su inglés. Decidí despegarme de la enseñanza tradicional en los Estados Unidos y pasar un año viajando por Sudamérica con esperanzas de aprender español. Me mudé a Arequipa en octubre y comencé a enseñar inglés. Me encanta ayudar a la gente a aprender Inglés es lo más destacado de mi día.', 'I am from Ocean Springs, Mississippi, United States\r\nI studied science at the University of South Alabama. I received a Bachelors of Science degree. I later went through an alternative teaching program and received my Texas Teacher\'s Certificate. While living on the border of Mexico and Texas I taught all high school sciences, PE, and Track. The majority of my students were ESL students. Which means they were English Language Learners. I fell in love with helping my students improve their English. I decided to take off from traditional teaching in the states and spend a year traveling South America with hopes to learn Spanish. I moved to Arequipa in October and began teaching English. I love helping people learn English it is the highlight of my day. \r\n'),
(4, 'Markéta', 'img/profe/foto/4.jpg', 'czech republic', 0, 'Mi nombre es Markéta Grigarová. Crecí en la República Checa, un pequeño país en el corazón de Europa. Estudié ciencias sociales, trabajé como agente de viajes, guía turística (Alaska, Guatemala) y profesora privada de inglés. Hasta ahora he vivido y trabajado voluntariamente en varios lugares del mundo; de todos estos lugares me enamoré de Perú y Alaska. Me encanta todo sobre viajar; la naturaleza, música e idiomas. El inglés para mí es un instrumento que hace todo más fácil y más significativo. Yo creo que el poder comunicarse con otras personas de una manera natural ayuda a comprender y enriquecer las relaciones', 'My name is Markéta Grigarová. I grew up in the Czech Republic, a small country in the heart of Europe. I studied social sciences, worked as a travel agent, a tour guide (Alaska, Guatemala) and a private English teacher. So far I have lived, worked and volunteered in several places in the world. I felt in love with Peru and Alaska the most. I love all about travelling, nature, music and languages. English for me is an instrument that makes everything easier and more meaningful. I believe that being able to communicate with other people in a natural way helps with understanding and enriches relationships.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios`
--

CREATE TABLE `servicios` (
  `id` int(11) NOT NULL,
  `tit_es` text COLLATE utf8_bin,
  `cont_es` text COLLATE utf8_bin,
  `tit_en` text COLLATE utf8_bin,
  `cont_en` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `servicios`
--

INSERT INTO `servicios` (`id`, `tit_es`, `cont_es`, `tit_en`, `cont_en`) VALUES
(1, 'Inglés corporativo', '<p>En International English nuestros cursos están diseñados en base a las necesidades específicas de su empresa. Aprenderá inglés sin afectar sus horarios y en la comodidad de sus o nuestras instalaciones.</p>\r\n<ul>\r\n  <li>\r\n    Nuestras clases serán un apoyo esencial para todas sus áreas de negocios.\r\n </li>\r\n <li>\r\n    Contamos con cursos y niveles desde principiantes hasta avanzados.\r\n  </li>\r\n <li>\r\n    Ofrecemos dos tipos de cursos In-company:\r\n</p>\r\n<ul>\r\n <li>\r\n    ONE TO ONE\'S (Clases unipersonales)\r\n<br>\r\nProfesor - Alumno\r\n<br>\r\nEl avance es según el nivel desde 72 hasta 84 horas por alumnos\r\n<br>\r\nEl tiempo dependera de la disponibilidad del alumno semanal y mensualmente\r\n<br>\r\nUn alumno puede terminar un nivel hasta en un mes\r\n</p>\r\n<ul>\r\n <li>\r\n    CURSOS GRUPALES (Según la necesidad del cliente)\r\n<br>\r\nGrupos desde 2 Alumnos\r\n<br>\r\nEl avance es según el nivel desde 72 hasta 128 horas por grupo \r\n<br>\r\nEl tiempo dependera de la disponibilidad del grupo semanal y mensualmente\r\n<br>\r\nGeneralmente se establecen horarios para que el grupo trabaje uniformemente\r\n<br>\r\nEl horario es elegio por los clientes\r\n<br>\r\nLos aluumnos pueden terminar un nivel hasta en un mes\r\n', '(Inglés) Inglés Corporativo', ' '),
(2, 'Traducciones e Interpretaciones ', 'Traducciones e Interpretaciones profesionales especializadas en aportar a nuestros clientes soluciones integrales de traducciones Certificadas e Interpretaciones en Simultaneo en cualquier idioma, actividad profesional y sobre cualquier tipo de texto y/o Evento.\r\n<br>\r\n<br>\r\nIDIOMAS\r\n<br>\r\nInglés, francés, Italiano y portugués.', ' ', ' '),
(3, 'Simulacro de Exámenes Oficiales', 'Para tu mayor seguridad al momento de rendir tu Exámen Oficial, IE te ofrece rendir un exámen simulacro con nosotros identico al que rindes en los centros autorizados en Perú, así conoceras los tiempos y forma del exámen al momento de dar el oficial.', ' ', ' '),
(4, 'Curso para Ejecutivos ', 'El Curso exclusivo para ejecutivos y/o profesionales es flexible y permite que el participante pueda aprender el inglés en un ambiente adecuado a su nivel. \r\n<br>\r\n<br>\r\nEl tiempo para completarlo es de dos años como máximo, contados a partir del primer Nivel del inglés. Si el participante ya tiene algún conocimiento el tiempo será menor. \r\n<br>\r\n<br>\r\nLos horarios son perfectos para personas que trabajan durante todo el día, y que no desean seguir postergando el aprendizaje del inglés por falta de tiempo.\r\n<br>\r\n<br>\r\nAprende de manera rápida y exigete\r\n', '  ', '  ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `imgg` text COLLATE utf8_bin,
  `imgc` text COLLATE utf8_bin,
  `url` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `slider`
--

INSERT INTO `slider` (`id`, `imgg`, `imgc`, `url`) VALUES
(1, 'img/slider/imgg/1.jpg', 'img/slider/imgc/1.jpg', '#url'),
(2, 'img/slider/imgg/2.jpg', 'img/slider/imgc/2.jpg', 'http://www.facebook.com'),
(3, 'img/slider/imgg/3.jpg', 'img/slider/imgc/3.jpg', 'http://www.internationalenglish.org.pe/');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `w_faq`
--

CREATE TABLE `w_faq` (
  `id` int(11) NOT NULL,
  `q` text COLLATE utf8_bin,
  `a` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `w_faq`
--

INSERT INTO `w_faq` (`id`, `q`, `a`) VALUES
(1, 'What materials do you use?', 'For our general English courses for adult learners we use Inside Out as our primary text book.  For our young learners we use New Opportunities and the Ready For series as our primary text.  However, we have a library of other materials and activities to supplement the text books.  The text books are just a place to begin; we teach our students not the book'),
(2, 'What are the hours like?', 'We have classes in the mornings, afternoons and evenings.  We also have In-company courses and private classes that are may even be scheduled for weekends.  The average teacher has 20 contact hours per week.  There are usually more hours available for those who desire to work more.'),
(3, 'Do you require TEFL/ TESL/ TESOL/ CELTA certification?', 'We do not require any of these certifications, however it is preferred.  But more importantly we offer a free Internal Teacher Training course.  The first week is an intensive course that prepares you before entering classes.  After beginning classes a bi-weekly on-going training begins.\r\n'),
(4, 'Do you include accommodation?', 'Accommodation is not included, however we are very happy to help you get settled.  There are many different options available all depending on your budget, how long you plan to stay in Arequipa and your privacy needs.  Some of the options are: furnished/unfurnished rooms with private bath and entrance, home-stays, furnished/unfurnished apartments, sharing an apartment with another teacher, or with a Peruvian.  Once a decision has been made, we can begin making all the arrangements.  Prices can be seen in the City Cost Guide below.\r\n'),
(5, 'Do you pay airfare?', 'Airfare is currently not a part of our Total Awards Program.'),
(6, 'What is your pay?', 'Payment is based on your contact hours.  The rate of pay is based on skills, knowledge and abilities most commonly acquired from formal education and practical experience.   Please see the City Cost Guide below to calculate living expenses in Arequipa.\r\n\r\nOur base scale is 8 -12 soles per hour.  However, we offer shift differentials for In-company courses and for any classes taught on Saturdays.  Also, after completing our Internal Teacher Training course (approximately 3 months) you will be eligible for a review and an increase in pay.\r\n\r\nPlease see the link to our Total Awards Program for additional benefits.'),
(7, 'What kind of time commitment do you require?', 'We require a minimum of a 6 month commitment.'),
(8, 'What is Arequipa like?', 'Arequipa on Wikipedia \r\nPictures of Arequipa on Google\r\n\r\nPlease see the Work Life section of the Total Awards Program for more information on Arequipa, Peru.'),
(9, 'Do you offer Spanish classes?', 'We offer Spanish classes for all interested teachers.'),
(10, 'What are the ages of the students that I would be teaching?', 'We have classes for adults and young learners.  Our young learners are between 11-16 years old and our adult classes have students ranging from 17-65 (with the average age being 23).\r\n'),
(11, 'What kind of benefits do you offer?', 'See our Total Awards Program.'),
(12, 'What are the other teachers like?', 'We have teachers from many different backgrounds (ethnic and professional).  Some of our teachers are professional TEFL teachers that travel the world teaching English and learning new cultures.  Others are younger teachers with no TEFL background, but are looking for a new life experience before beginning their careers or between jobs.\r\n\r\nOur teachers come from Malta, England, Ireland, The Netherlands, United States, etc…');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `w_rewards`
--

CREATE TABLE `w_rewards` (
  `id` int(11) NOT NULL,
  `tit` text COLLATE utf8_bin,
  `cont` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `w_rewards`
--

INSERT INTO `w_rewards` (`id`, `tit`, `cont`) VALUES
(1, 'Compensation', '<b>Base Wages</b> \r\nOur base pay range is very competitive.  Pay is based on your skills, knowledge and abilities, including fluency of English, and your previous experience and/or education.  Our wages surpass local wages in other English schools by 30 – 80%.\r\n\r\n<br><b>Shift Differentials</b> We offer shift differentials for working weekends, private classes and for In-company courses.\r\n\r\n<br><b>Transportation</b> We pay for your transportation when working offsite in private classes or In-company courses.  ');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cont`
--
ALTER TABLE `cont`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `info`
--
ALTER TABLE `info`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inicio`
--
ALTER TABLE `inicio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `profesores`
--
ALTER TABLE `profesores`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `servicios`
--
ALTER TABLE `servicios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `w_faq`
--
ALTER TABLE `w_faq`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `w_rewards`
--
ALTER TABLE `w_rewards`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cont`
--
ALTER TABLE `cont`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `cursos`
--
ALTER TABLE `cursos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `info`
--
ALTER TABLE `info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `inicio`
--
ALTER TABLE `inicio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=255;
--
-- AUTO_INCREMENT de la tabla `personas`
--
ALTER TABLE `personas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `profesores`
--
ALTER TABLE `profesores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `servicios`
--
ALTER TABLE `servicios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `w_faq`
--
ALTER TABLE `w_faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `w_rewards`
--
ALTER TABLE `w_rewards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;--


















-- Base de datos: `sc`
--
CREATE DATABASE IF NOT EXISTS `sc` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `sc`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `info`
--

CREATE TABLE `info` (
  `id` int(11) NOT NULL,
  `url` text COLLATE utf8_bin NOT NULL,
  `icono` text COLLATE utf8_bin NOT NULL,
  `titulo` text COLLATE utf8_bin NOT NULL,
  `descripcion` text COLLATE utf8_bin NOT NULL,
  `facebookid` text COLLATE utf8_bin NOT NULL,
  `twitter` text COLLATE utf8_bin NOT NULL,
  `pinterest` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `info`
--

INSERT INTO `info` (`id`, `url`, `icono`, `titulo`, `descripcion`, `facebookid`, `twitter`, `pinterest`) VALUES
(1, 'www.mktsincorbata.com', 'img/web/sc1.svg', 'Marketing Sin Corbata', 'Bla bla bla bla', 'ya no tenemos', '', '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `info`
--
ALTER TABLE `info`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `info`
--
ALTER TABLE `info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;--










-- Base de datos: `vasco`
--
CREATE DATABASE IF NOT EXISTS `vasco` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `vasco`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajador`
--

CREATE TABLE `trabajador` (
  `tid` int(8) NOT NULL,
  `pase` text COLLATE utf8_bin NOT NULL,
  `email` text COLLATE utf8_bin NOT NULL,
  `telefono` text COLLATE utf8_bin NOT NULL,
  `nombre` text COLLATE utf8_bin NOT NULL,
  `apellido` text COLLATE utf8_bin NOT NULL,
  `qr` text COLLATE utf8_bin NOT NULL,
  `tipo` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `trabajador`
--

INSERT INTO `trabajador` (`tid`, `pase`, `email`, `telefono`, `nombre`, `apellido`, `qr`, `tipo`) VALUES
(12345678, '1', '2@2.2', '987654321', 'prom1', 'prom1', 'qr/prom1prom1.png', '2'),
(33333333, '1', 'q@q.q', '987654321', 'vend1', 'vend1', 'qr/vend1vend1.png', '3'),
(45623520, '1', 'm@m.m', '123456789', 'fahed', 'S', '', '3'),
(99999999, '1', 'q@q.q', '987654312', 'Adm1', 'Adm1', 'qr/Adm1Adm1.png', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transaccion`
--

CREATE TABLE `transaccion` (
  `trid` int(11) NOT NULL,
  `tid` int(11) NOT NULL,
  `vid` int(8) DEFAULT NULL,
  `cid` int(11) NOT NULL,
  `monto` int(11) NOT NULL,
  `tiempo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `transaccion`
--

INSERT INTO `transaccion` (`trid`, `tid`, `vid`, `cid`, `monto`, `tiempo`) VALUES
(11111112, 12345678, 45623520, 0, 250, 1509222266);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `trabajador`
--
ALTER TABLE `trabajador`
  ADD PRIMARY KEY (`tid`);

--
-- Indices de la tabla `transaccion`
--
ALTER TABLE `transaccion`
  ADD PRIMARY KEY (`trid`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `trabajador`
--
ALTER TABLE `trabajador`
  MODIFY `tid` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100000000;
--
-- AUTO_INCREMENT de la tabla `transaccion`
--
ALTER TABLE `transaccion`
  MODIFY `trid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11111113;