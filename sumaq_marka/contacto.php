<?php
	$titulo = 'Spanish teacher Ericka L. Hoxey - Contact';
	$h1 = 'Contact with Ericka L. Hoxey, Spanish Teacher';
	require('php/arriba.php');

	$telefono1	= '07910753049';
	$telefono2	= '';
	$email		= 'ericka@spanishteacher.org.uk';

	$c_nombre = 'Your Name';
	$c_email = 'Your e-mail';
	$c_mensaje = 'Tell me...';
	$c_boton = 'SEND';
?>


<div class="fondo-img-1 fondo-fijo primero">
	<div class="s11 m9 s-pad-v-6 contra">


<?php
	echo'
			<h2>CONTACT ME</h2>
			<div class="s12 s-pad-v-2">
				<p>
					You can find me here:
				</p>
				<div class="s-flex flex-around s-pad-2">';

	//Iconos de contacto
	if (isset($telefono1) && !empty($telefono1)) {
		echo '
					<div class="vinculo d-inline-block">
						<a class="d-flex flex-vert flex-centro" href="tel:'.$telefono1.'">
							<span class="d-flex flex-centro">
								<img alt="telephone 1" src="img/web/telf.svg" style="height:1.5rem; width:1.5rem; margin: 0 0.3em">
							</span>
							<span class="">'.$telefono1.'</span>
						</a>
					</div>
		';
	}
	if (isset($telefono2) && !empty($telefono2)) {
		echo '
					<div class="vinculo d-inline-block">
						<a class="d-flex flex-vert flex-centro" href="tel:'.$telefono2.'">
							<span class="d-flex flex-centro">
								<img alt="telephone 2" src="img/web/telf.svg" style="height:1.5rem; width:1.5rem; margin: 0 0.3em">
							</span>
							<span class="">'.$telefono2.'</span>
						</a>
					</div>
		';
	}
	if (isset($email) && !empty($email)) {
		echo '
					<div class="vinculo d-inline-block">
						<a class="d-flex flex-vert flex-centro" href="mailto:'.$email.'">
							<span class="e-mail d-flex flex-centro">
								<img alt="" src="img/web/email.svg" style="height:1.5rem; width:1.5rem; margin: 0 0.3em">
							</span>
							<span class="">'.$email.'</span>
						</a>
					</div>
		';
	}
	echo '
					<div class="vinculo d-inline-block">
						<a class="d-flex flex-vert flex-centro" href="Contacto - Ericka L. Hoxey.vcf">
							<span class="d-flex flex-centro">
								<img alt="e-Contact Card" src="img/web/tarjeta.svg" style="height:1.5rem; width:1.5rem; margin: 0 0.3em">
							</span>
							<span class="">VCard</span>
						</a>
					</div>
				</div>
				<p>Or send a message here:</p>
			</div>
	';

	//Formulario de contacto
	echo '
			<form class="s-pad-h-1 s-pad-t-2" id="contacto" action="contacto_envio.php?ficha=contacto&red='.$_SERVER['PHP_SELF'].'" method="POST">
				<div class="s12 s-pad-t-1 s-pad-b-0">
				</div>
				
				<div class="s12 s-pad-v-1">
					<input class="entrada" id="nombre" type="text" name="nombre" required="" placeholder="'.$c_nombre.'" title="How would you like to be addressed?"/>
				</div>
				<div class="s12 s-pad-v-1">
					<input class="entrada" id="email" type="email" name="email" required="" placeholder="'.$c_email.'"  title="Where to email you back?"/>
				</div>
				<div class="s12 s-pad-v-1">
					<textarea class="entrada" style="min-height: 5rem;" id="msj" name="msj" rows="5" required="" placeholder="'.$c_mensaje.'"  title="Any question?"></textarea>
				</div>
				<div class="s12 s-pad-v-1">
					<input class="boton" id="submit" type="submit" value="'.$c_boton.'">
				</div>
			</form>';
	echo '
		</div>
	</div>
	';

	require('php/abajo.php');
?>
