<?php
	$titulo = 'Spanish teacher Ericka L. Hoxey - Home';
	$h1 = 'This website\'s landing page';
	require('php/arriba.php');
?>
<div class=" primero s-flex fondo-color-1">
	<div class="s12 m9 s-pad-v-6 s-flex">

		<div class="s12 l6 s-flex">
			<div class="img-limite1">
				<img class="" src="img/foto/ericka.png">
			</div>
		</div>

		<div class="s12 l6 s-flex">
			<div class="s12 d-block tex-centrado">
				<h2>Ericka L. Hoxey</h3>
			</div>

			<div class="s12 d-block tex-justificado s-pad-h-2 m-pad-h-0">
				Hola!  Thank you for visiting my site.
				My name is Ericka L. Hoxey, I have been a professional teacher for 24 years and I am highly experienced in teaching Spanish as a foreign language to students from around the world. 
				I ran my own Spanish school in Peru which was one of my dreams come true. 
				Now I live in England, based in Cambridge and since my profession is something I will always love, here I am waiting for you to join me in this great adventure called Spanish language. Let me show you what I know to do well: <b style="display: block; text-align: center;">TEACHING SPANISH!</b>
			</div>
		</div>

	</div>
</div>

<div class="fondo-img-1">
	<div class="s12 m9 s-pad-v-6 contra s-flex">
		<h3 class="s12 d-block tex-justificado s-pad-h-2 m-pad-h-0">
			WHAT TO EXPECT
		</h3>
		<p class="s12 d-block tex-justificado s-pad-h-2 m-pad-h-0">
			Courses for complete beginners through to advanced speakers. Lessons face to face or online. High quality and flexible Spanish lessons:
		</p>
		<ul class="s12 d-block tex-justificado s-pad-h-2 m-pad-h-0">
			<li style="list-style: square;margin-left: 2rem;">1-to-1</li>
			<li style="list-style: square;margin-left: 2rem;">small groups</li>
		</ul>

		<p class="s12 d-block tex-justificado s-pad-h-2 m-pad-h-0">
			We can cover any type of Spanish  Courses, although the popular ones are:
		</p>
		<ul class="s12 d-block tex-justificado s-pad-h-2 m-pad-h-0">
			<li style="list-style: square;margin-left: 2rem;">Spanish for holiday</li>
			<li style="list-style: square;margin-left: 2rem;">Standard Spanish courses</li>
			<li style="list-style: square;margin-left: 2rem;">GCSE Revision</li>
			<li style="list-style: square;margin-left: 2rem;">A-level Revision</li>
			<li style="list-style: square;margin-left: 2rem;">Conversational Spanish</li>
		</ul>
	</div>
</div>

<div class="fondo-color-1" style="margin-bottom: -.5rem;">
	<div class="s12 contra">
<?php
	require('php/slider.php');
?>

	</div>
</div>
<?php
	require('php/contactame.php');
	require('php/abajo.php');
?>
