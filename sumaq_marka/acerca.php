<?php
	$titulo = 'Spanish teacher Ericka L. Hoxey - About me';
	$h1 = 'About me, the Spanish Teacher';
	require('php/arriba.php');
?>
<div class="fondo-color-1 primero">
	<div class="s12 m9 s-pad-v-6 contra">
		<div class="s12 l6 s-flex">
			<div class="img-limite1 s-pad-v-2">
				<img class="" style="max-width: 70%" src="img/foto/ericka.png">
			</div>
		</div>
		
		<div class="s-pad-h-3">
			<h3>ABOUT ME </h3>
			<p class="tex-justificado">
				I am a native Spanish speaker and was born in Peru. I graduated with a Bachelor’s degree from Universidad Nacional de San Agustín in Arequipa, Peru with a major in Pre-School Teaching, and studies in English and Spanish Tutoring.<br>

				I started my career as a kindergarten teacher, but for the last 19 years I have focused my work on language teaching.<br><br>

				While in Peru I spent many years teaching English and Spanish as a Foreign Language to professionals of several businesses including companies related to commerce, engineering, building and mining industry.
				My work consisted of all aspects of the Language Program development such as creating, customizing and delivering courses; preparing lessons, one to one and group tuition, evaluating <a href="testimonios.php">students</a> and most importantly focusing on a high quality learning experience.<br><br>
				More recently I established and ran my own Spanish School for foreign <a href="testimonios.php">students</a> in Arequipa – Peru, where I had management and teaching responsibilities,   <a target="_blank" href="http://sumaqmarka.com">Sumaq Marka</a>.<br>

				I created and customised lessons and learning material for a wide range of <a href="testimonios.php">students</a> including business people, professionals in various specialties, tourists, volunteers and children.<br><br>
				I pride myself on making the language learning experience as enjoyable as possible allowing students to develop language skills in a confident manner at an individual pace.<br><br>
				Today, living in Cambridge, England I am offering my experience as a Spanish teacher for all ages, levels of ability and previous knowledge, from complete beginners to experienced speakers who would like to brush up on the finer points of Spanish. All lessons are developed based on your own requirements and pace of learning.

			</p>
		</div>
	</div>
</div>

<?php
	require('php/contactame.php');
	require('php/abajo.php');
?>
