<?php
	$titulo = 'Spanish teacher Ericka L. Hoxey - Testimonies';
	$h1 = 'Testimonies from my former students';
	require('php/arriba.php');
?>
<div class="fondo-img-1 fondo-fijo primero">
	<div class="s12 m9 s-pad-v-6 contra">
	<h2>TESTIMONIES</h2>

<?php
	require_once('php/sql.php');
	$sql = 'select * from testimonios';
//	echo $sql;
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		$x = 1;
		while($row = $result->fetch_assoc()){
			if ($x == 1) {$item = 'item active';} else {$item = 'item';}
			echo'
					<div class="s12 s-pad-v-2 s-pad-3 s-flex flex-around testimonio ">

						<div class="s12 m6 l4 s-flex">
							<div class="img-limite2" style=" content:\'\'; background-image:url('.$row['foto'].');">
							</div>
						</div>

						<div class="s11 l8 s-flex">
								<h3 class="s12 tex-centrado">'.$row['nombre'].'</h3>
								<h5 class="s12 tex-centrado">'.$row['pais'].'</h5>
								<p class="s12 d-block tex-justificado s-pad-v-2 s-pad-v-0 s-pad-h-2 m-pad-h-0">
									'.$row['texto'].'
								</p>
						</div>

					</div>
			';
			$x = $x+1;
		}
	}
?>
	</div>
</div>

<?php
	require('php/contactame.php');
	require('php/abajo.php');
?>
