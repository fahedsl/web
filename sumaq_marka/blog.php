<?php
	$titulo = 'Spanish teacher Ericka L. Hoxey - Blog';
	$h1 = 'Spanish teacher Ericka L. Hoxey - Blog';
	require('php/arriba.php');

// EXISTE UNA ENTRADA ELEGIDA
	if(isset($_GET['entrada']) && !is_null($_GET['entrada'])){
		$dir = 'blog/'.$_GET['entrada'];
		$lista = scandir($dir);
		$titulofd = $dir.'/titulo.txt';
		$titulof = fopen($titulofd, 'r');
		$titulo = fread($titulof,filesize($titulofd));
		fclose($titulof);
		$contenidofd = $dir.'/contenido.txt';
		$contenidof = fopen($contenidofd, 'r');
		$contenido = fread($contenidof,filesize($contenidofd));
		fclose($contenidof);
		$socialdir = $_SERVER['HTTP_REFERER'].'?'.$_SERVER['QUERY_STRING'];
		$socialdir1 = urlencode($socialdir);
		$socialtitulo = urlencode($titulo);
		echo'
		<div class="fondo-img-1">
			<div class="blog s-pad-t-2 s-pad-b-4 s-pad-h-2 m-pad-h-10 l-pad-h-20">
				<div class="s-pad-v-1 m-pad-v-2 l-pad-v-3 s-pad-h-1 m-pad-h-3">
					<h1>'.$titulo.'</h1>
					'.$contenido.'
				</div>
			</div>
		</div>';
	}
// LISTA DE ENTRADAS EXISTENTES
	elseif(!isset($_GET['entrada']) || is_null($_GET['entrada'])) {
		echo'
		<div id="blog-lista" class="fondo-img-1 s-pad-b-4">';

		$dir = 'blog';
		$lista = scandir($dir);
		rsort($lista);
		for( $i = 0; $i < sizeof($lista); $i++ ){
			$item = $lista[$i];
			if ($item != '.' && $item != '..') {
				$titulofd = $dir.'/'.$item.'/titulo.txt';
				$titulof = fopen($titulofd, 'r');
				$titulo = fread($titulof,filesize($titulofd));
				fclose($titulof);
				$descripcionfd = $dir.'/'.$item.'/descripcion.txt';
				$descripcionf = fopen($descripcionfd, 'r');
				$descripcion = fread($descripcionf,filesize($descripcionfd));
				fclose($descripcionf);

				echo'
				<section class="sec m-pad-t-1 l-pad-t-2">
					<h3 style="display:none">'.$titulo.' - Blog - Marketing Sin Corbata</h3>
					<div class="s-pad-t-2">
						<a href="blog.php?entrada='.$item.'">
							<h3 class="tex-centrado">'.$titulo.'</h3>
							<p class="s-pad-h-2 m-pad-h-3 l-pad-h-4 s-pad-v-1 tex-centrado">'.$descripcion.'</p>
							<img alt="imagen de portada del artículo" style="max-height:80vh;" src="'.$dir.'/'.$item.'/principal.jpg">
						</a>
					</div>
				</section>';
			}
		}
		echo'
			</div>';
	}

	require('php/contactame.php');
	require('php/abajo.php');
?>
