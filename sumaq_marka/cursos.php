<?php
	$titulo = 'Spanish teacher Ericka L. Hoxey - Courses';
	$h1 = 'Our Spanish Courses';
	require('php/arriba.php');
?>
<div class="primero">
	<div class="s11 m9 s-pad-v-6 contra">

<?php
	require('php/sql.php');
	$sql = 'select descrip from cursos where tipo=2';
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		$row = $result->fetch_assoc();
		echo'
		<div class="d-block tex-justificado s-pad-h-2 m-pad-h-0">
			<h2>
				COURSES
			</h2>
			'.$row['descrip'].'
		</div>';
	}
	
	$sql = 'select * from cursos where tipo=1';
//	echo $sql;
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()){
			echo'
	<div class="botonx botonx-off" id="botonx'.$row['id'].'" onclick="despl_on(\'botonx'.$row['id'].'\',\'pestana'.$row['id'].'\')">
		<h4 style="display:inline">'.$row['nombre'].'</h4>
		<div class="flecha"></div>
	</div>
	<div class="pestana" id="pestana'.$row['id'].'">
		'.$row['descrip'].'
	</div>
			';
		}
	}
?>

	</div>
</div>

<?php
	require('php/contactame.php');
	require('php/abajo.php');
?>
