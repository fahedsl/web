function despl_on(boton1, pestana1, nombre1="botonx", nombre2="pestana") {
	var boton = document.getElementById(boton1);
	var pestana = document.getElementById(pestana1);

	boton.setAttribute("onClick", "despl_off('"+boton1+"','"+pestana1+"', '"+nombre1+"', '"+nombre2+"')");
	boton.classList.remove(nombre1+"-off");
	boton.classList+= (" "+nombre1+"-on");
	pestana.classList.remove(nombre2+"-off");
	pestana.classList+= (" "+nombre2+"-on");
}

function despl_off(boton1, pestana1, nombre1="botonx", nombre2="pestana") {
	var boton = document.getElementById(boton1);
	var pestana = document.getElementById(pestana1);

	boton.setAttribute("onClick", "despl_on('"+boton1+"','"+pestana1+"', '"+nombre1+"', '"+nombre2+"')");
	boton.classList.remove(nombre1+"-on");
	boton.classList+= (" "+nombre1+"-off");
	pestana.classList.remove(nombre2+"-on");
	pestana.classList+= (" "+nombre2+"-off");
}

//Cambia el texto del editor de blog
function texto_cambia(){
	var texto =	document.getElementById("texto").value;
	document.getElementById("muestra").innerHTML = texto;
}

//Cerrar el menu principal al apretar ESC
$(document).keyup(function(esc) {
	if (esc.keyCode == 27) {
		despl_off();
	}
});