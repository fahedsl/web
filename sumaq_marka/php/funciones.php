<?php
	function imagen_tamano_ajustar($horiz, $vert, $max_horiz, $max_vert) {
		if($horiz >= $vert){
			if($horiz > $max_horiz){
				$vert = ceil(($vert * $max_horiz) / $horiz);
				$horiz = ceil($max_horiz);
				$tamano = array($horiz, $vert);
				var_dump($tamano);
				return($tamano);
			}
		}
		else{
			if($vert > $max_vert){
				$horiz = ceil(($horiz * $max_vert) / $vert);
				$vert = ceil($max_vert);
				$tamano = array($horiz, $vert);
				return($tamano);
			}
		}
	}

	function imagen_crear_dejpg($origen, $destino, $calidad=100, $destino_horiz, $destino_vert) {
			$imagen = getimagesize($origen);
			// Redimension
			$imag1 = imagecreatefromjpeg($origen);
			$imag2 = imagecreatetruecolor($destino_horiz, $destino_vert);
			imagecopyresampled($imag2, $imag1, 0, 0, 0, 0, $destino_horiz, $destino_vert, $imagen[0], $imagen[1]);
			imagejpeg($imag2, $destino, $calidad);
			if(imagedestroy($imag2) && imagedestroy($imag1)){
				return(TRUE);
			}
			else{ return(FALSE); }
	}

	function imagen_crear_depng($origen, $destino, $calidad=100, $destino_horiz, $destino_vert) {
			$imagen = getimagesize($origen);
			// Redimension
			$imag1 = imagecreatefrompng($origen);
			$imag2 = imagecreatetruecolor($destino_horiz, $destino_vert);
			imagecopyresampled($imag2, $imag1, 0, 0, 0, 0, $destino_horiz, $destino_vert, $imagen[0], $imagen[1]);
			imagejpeg($imag2, $destino, $calidad);
			if(imagedestroy($imag2) && imagedestroy($imag1)){
				return(TRUE);
			}
			else{ return(FALSE); }
	}

	function borrar_dir($dirname)
	{
		// Sanity check
		if (!file_exists($dirname)) {
			return false;
		}

		// Simple delete for a file
		if (is_file($dirname) || is_link($dirname)) {
			return unlink($dirname);
		}

		// Loop through the folder
		$dir = dir($dirname);
		while (false !== $entry = $dir->read()) {
			// Skip pointers
			if ($entry == '.' || $entry == '..') {
				continue;
			}

			// Recurse
			borrar_dir($dirname . DIRECTORY_SEPARATOR . $entry);
		}

		// Clean up
		$dir->close();
		return rmdir($dirname);
	}
?>
?>