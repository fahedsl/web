		<div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="10000">
			<div class="carousel-inner" role="listbox">
<?php
	require_once('php/sql.php');
	$sql = 'select * from testimonios';
//	echo $sql;
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		$x = 1;
		while($row = $result->fetch_assoc()){
			if ($x == 1) {$item = 'item active';} else {$item = 'item';}
			echo'
				<div class="'.$item.' testimonio">
					<div class="s12 s-pad-v-6 s-pad-3 s-flex flex-around">

						<div class="s12 m5 s-flex">
							<div class="img-limite2" style=" content:\'\'; background-image:url('.$row['foto'].');">
							</div>
						</div>

						<div class="s11 m7 s-flex">
							<div class="s12 d-block tex-justificado s-pad-v-2 s-pad-v-0 s-pad-h-2 m-pad-h-0">
								<p class="">
									'.$row['texto'].'
								</p>
								<a style="font-size:2rem; font-weight:bold;" href="testimonios.php">More...</a>
							</div>
						</div>

					</div>
				</div>
			';
			$x = $x+1;
		}
	}
?>
				<!-- Left and right controls -->
				<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
					<span class="sr-only">Next</span>
				</a>

			</div>
		</div>