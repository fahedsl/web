<?php
	header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
	header("Pragma: no-cache"); // HTTP 1.0.
	header("Expires: 0"); // Proxies.
	require('elementos-menu.php');
	
	require('php/sql.php');
	$sql = 'select * from info';
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		$row = $result->fetch_assoc();
		if (empty($row['url']))		{ $row['url'] = ''; }
		if (empty($row['icono']))		{ $row['icono'] = ''; }
		if (empty($row['titulo']))		{ $row['titulo'] = ''; }
		if (empty($row['descripcion'])){ $row['descripcion'] = ''; }
		if (empty($row['facebookid']))	{ $row['facebookid'] = ''; }
		if (empty($row['twitter']))	{ $row['twitter'] = ''; }
		if (empty($row['pinterest']))	{ $row['pinterest'] = ''; }
		$tipo = 'website';

		if ($_SERVER['PHP_SELF'] == '/blog.php') {
			if(isset($_GET['entrada']) && !empty($_GET['entrada'])) {
				$titulofd = 'blog/'.$_GET['entrada'].'/titulo.txt';
				$titulof = fopen($titulofd, 'r');
				$titulo1 = fread($titulof,filesize($titulofd));
				fclose($titulof);
				$descripcionfd = 'blog/'.$_GET['entrada'].'/descripcion.txt';
				$descripcionf = fopen($descripcionfd, 'r');
				$descripcion = fread($descripcionf,filesize($descripcionfd));
				fclose($descripcionf);
			}
		}

		echo '

	<!DOCTYPE html>
		<html lang="es" xml:lang="es"
		xmlns="https://www.w3.org/1999/xhtml" 
		xmlns:og="https://ogp.me/ns#" 
		xmlns:fb="https://www.facebook.com/2008/fbml">

	<!-- CABECERA -->
	<head>
	<!-- cache -->
		<meta http-equiv="cache-control" content="max-age=0" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
		<meta http-equiv="pragma" content="no-cache" />
		
	<!-- seo -->
		<meta name="ROBOTS" content="INDEX,FOLLOW">
		<meta name="google" value="notranslate">

	<!-- titulo -->
		<meta name="title" content="'.$titulo.'">
		<title>'.$titulo.'</title>

	<!-- icono -->
		<meta name="viewport" content="width=device-width" />
		<link rel="icon" href="img/web/icono.svg">
		<link rel="shortcut icon" href="img/web/isotipo.svg">
		<link rel="image_src" href="http://spanishteacher.org.uk/img/web/isotipo.svg">

	<!-- Estilos -->
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="css/base.css">

	<!-- JS -->
		<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
		<script src="js/bootstrap.js"></script>
		<script src="js/base.js"></script>

	<!-- Graphs -->
		<!--Open graph-->
		<meta property="og:title" content="'.$row['titulo'].'"/>
		<meta property="og:type" content="'.$tipo.'"/>
		<meta property="og:url" content="'.$row['url'].'"/>
		<meta property="og:image" content="http://spanishteacher.org.uk/'.$row['icono'].'"/>
		<meta property="og:description" content="'.$row['descripcion'].'"/>
		';
		if (isset($row['facebookid']) && !empty($row['facebookid']) && isset($row['facebookpag']) && !empty($row['facebookpag'])) {
			echo'
		<meta property="fb:pages" content="'.$row['facebookpag'].'"/>
		<meta property="fb:app_id" content="'.$row['facebookid'].'"/>
			';
		}
		if (isset($row['twitter']) && !empty($row['twitter'])) {
			echo'
		<!--Twitter cards-->
		<meta name="twitter:card" content="summary" />
		<meta name="twitter:title" content="'.$row['titulo'].'">
		<meta name="twitter:description" content="'.$row['descripcion'].'">
		<meta name="twitter:image" content="'.$row['url'].'/'.$row['icono'].'">
		<meta name="twitter:site" content="'.$row['twitter'].'">
			';
		}
		if (isset($row['pinterest']) && !empty($row['pinterest'])) {
			echo'
		<!--Pinterest-->
		<meta name="p:domain_verify" content="'.$row['pinterest'].'"/>
		';
		}
	}
?>
</head>

<!-- CUERPO -->
<body class="fondo-color-1 fondo-fijo">
	<h1 style="display: none;"><?php if(!empty($h1)){echo $h1;}?></h1>
	<nav class="navbar navbar-default center" role="navigation">
		<div class="navbar-inner">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" id="boton-menu" onclick="despl_on('boton-menu', 'pestana-menu', 'boton-menu' ,'pestana-menu')">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="http://www.spanishteacher.org.uk/">
					Ericka L. Hoxey
				</a>
			</div>
   			<div class="pestana-menu-off" id="pestana-menu">
				<ul>

<?php
echo 	
					$item1
					.$item2
					.$item3
					.$item4
					.$item5
					.$item6
					.$item7
					.$item8
;
?>

				</ul>
			</div>
		</div>
	</nav>

<?php
	if(isset($_GET['msj'])){
		echo '	<div class="pop">
					<p>'.$_GET['msj'].'</p>
				</div>';
	}
?>

	<div class="portada p-relative s-pad-t-6">
		<h2 style="text-align: center;">Spanish Teacher</h2>
		<h3 style="text-align: center;">Based in UK, Cambridge</h3>
	</div>
	<div class="divisor"></div>
	<div class="cuerpo">
		<div class="d-block s-pad-b-1" style="height: 0px; margin:0; padding: 0;">
			<div class="social" style="">
<?php

if ($_SERVER['PHP_SELF'] == '/blog.php') {
	if(isset($_GET['entrada'])) {
		$socialdir = $_SERVER['HTTP_REFERER'].'?'.$_SERVER['QUERY_STRING'];
		$socialdir1 = urlencode($socialdir);
		$socialtitulo = urlencode($titulo1);
		echo'
		<!--Facebook-->
				<div class="rs facebook">
					<a target="_blank" href="https://www.facebook.com/sharer.php?u='.$socialdir1.'&t='.$socialtitulo.'"></a>
				</div>
		<!--Twitter-->
				<div class="rs twitter">
					<a target="_blank" href="https://twitter.com/intent/tweet?text='.$socialtitulo.'&url='.$socialdir1.'"></a>
				</div>
		<!--Google plus-->
				<div class="rs googleplus">
					<a target="_blank" href="https://plus.google.com/share?url='.$socialdir1.'"></a>
				</div>
		<!--Linkedin-->
				<div class="rs linkedin">
					<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url='.$socialdir1.'&title='.$socialtitulo.'&source=http://spanishteacher.org.uk/blog.php"></a>
				</div>
		<!--Pinterest-->
				<div class="rs pinterest">
					<a target="_blank" href="https://pinterest.com/pin/create/bookmarklet/?url='.$socialdir1.'&media=http://spanishteacher.org.uk/blog/'.$_GET['entrada'].'/principal.jpg&description='.$socialtitulo.'"></a>
				</div>
		<!--E-mail-->
				<div class="rs email">
					<a href="mailto:?subject='.$socialtitulo.'&body='.$socialdir1.'"></a>
				</div>
		';
	}
}
else{
	$socialdir = 'http://spanishteacher.org.uk';
	$socialdir1 = urlencode($socialdir);
	$socialtitulo = urlencode($titulo);
	echo'
		<!--Facebook-->
				<div class="rs facebook">
					<a target="_blank" href="https://www.facebook.com/sharer.php?u='.$socialdir1.'&t='.$socialtitulo.'"></a>
				</div>
		<!--Twitter-->
				<div class="rs twitter">
					<a target="_blank" href="https://twitter.com/intent/tweet?text='.$socialtitulo.'&url='.$socialdir1.'"></a>
				</div>
		<!--Google plus-->
				<div class="rs googleplus">
					<a target="_blank" href="https://plus.google.com/share?url='.$socialdir1.'"></a>
				</div>
		<!--Linkedin-->
				<div class="rs linkedin">
					<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url='.$socialdir1.'&title='.$socialtitulo.'&source=http://spanishteacher.org.uk/blog.php"></a>
				</div>
		<!--Pinterest-->
				<div class="rs pinterest">
					<a target="_blank" href="https://pinterest.com/pin/create/bookmarklet/?url='.$socialdir1.'&media=http://spanishteacher.org.uk/img/foto/ericka.png&description='.$socialtitulo.'"></a>
				</div>
		<!--E-mail-->
				<div class="rs email">
					<a href="mailto:?subject='.$socialtitulo.'&body='.$socialdir1.'"></a>
				</div>
	';
}

?>
			</div>
		</div>
		<div>