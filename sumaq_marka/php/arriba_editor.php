<?php
	header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
	header("Pragma: no-cache"); // HTTP 1.0.
	header("Expires: 0"); // Proxies.
	require('php/check.php');
	echo'
<!DOCTYPE html>
<html lang="es" xml:lang="es" xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Blog Ericka - EDITOR</title>
	<link rel="icon" href="img/web/sc1.svg">
	<meta name="ROBOTS" content="INDEX,NOFOLLOW">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0;">

<!-- estilo -->
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/base.css">

<!-- cache -->
	<meta http-equiv="cache-control" content="max-age=0" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
	<meta http-equiv="pragma" content="no-cache" />
<!-- JS -->
	<script src="js/base.js"></script>

</head>
	';
?>

<body class="editor fondo-img-1">
	<div class="editor-header">
		<a href="blog_editor.php" style="cursor: pointer;">
			<div>
				<div style="overflow:initial; ">
					<h1 style="text-align:center">Blog Ericka - EDITOR</h1>
					<p style="text-align:center">(Clic aquí para volver)</p>
				</div>	
			</div>	
		</a>
	</div>
	<?php
	if(isset($_GET['msj'])){
		echo '	<div class="pop">
					<p>'.$_GET['msj'].'</p>
				</div>';
	}
?>
	<div id="editor" class="editor">