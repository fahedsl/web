<?php
	require('php/check.php');
	require('php/sql.php');
	require('php/funciones.php');
	$msj = '';

//NUEVA ENTRADA
	if(isset($_GET['entrada']) && $_GET['entrada'] == 'nueva'){
		$dir = 'blog';
		$lista = scandir($dir);
		$numero = max($lista) + 1;
		mkdir($dir.'/'.$numero);
		$titulo = $dir.'/'.$numero.'/titulo.txt';
		$contenido = $dir.'/'.$numero.'/contenido.txt';
		$descripcion = $dir.'/'.$numero.'/descripcion.txt';
		$archivo1 = fopen($titulo, 'w');
		fwrite($archivo1 , "Título automático");
		fclose($archivo1);
		$archivo2 = fopen($contenido, 'w');
		fwrite($archivo2 , "Contenido automático");
		fclose($archivo2);
		$archivo2 = fopen($descripcion, 'w');
		fwrite($archivo2 , "Descripción automática");
		fclose($archivo2);
		chmod($dir.'/'.$numero , 0777);
		chmod($titulo , 0777);
		chmod($contenido , 0777);
		chmod($descripcion , 0777);
		header('location:editor.php?entrada='.$numero);
	}

//ENTRADA EXISTENTE
	elseif(isset($_GET['entrada']) && $_GET['entrada'] != 'nueva' && isset($_GET['ficha'])){
	//SUBIR IMÁGENES
		if (isset($_GET['ficha']) && $_GET['ficha'] == 'imagen') {
			if (isset($_FILES['imagen'])) {
			$cantidad_imagenes = sizeof($_FILES['imagen']['name']) -1;
				for ($i=0; $i<=$cantidad_imagenes; $i++) {
					//verificar problemas de nombre
					if($_FILES['imagen']['tmp_name'][$i] !== ''){
						//verificar formato de imagen
						$imagen = getimagesize($_FILES['imagen']['tmp_name'][$i]);
						if ($imagen['mime'] == 'image/jpeg' || $imagen['mime'] == 'image/png') {
							//IMAGEN VERIFICADA!
							//seleccionar nombre
							$dir = 'blog/'.$_GET['entrada'];
							$lista = scandir($dir);
							$borrar = array('principal.jpg', 'titulo.txt', 'contenido.txt', 'descripcion.txt', '..', '.');
							foreach ($borrar as $valor) {
								$key = array_search($valor, $lista);
								unset($lista[$key]);
							}
							
							$lista = max($lista);
							$destino = substr($lista, 0, 1);
							$destino ++;
							$destino = $dir.'/'.$destino.'.jpg';

							//calcular tamaño
							if ($tamano = imagen_tamano_ajustar($imagen[0], $imagen[1], 600, 600)) {
							echo "2";
							//crear imagen
								if ($imagen['mime'] == 'image/jpeg') {
									if (imagen_crear_dejpg($_FILES['imagen']['tmp_name'][$i], $destino, 75, $tamano[0], $tamano[1] )) {
										$msj .= 'Subida con exito ('.$_FILES['imagen']['name'][$i].')<br>	';
									}
								}
								if ($imagen['mime'] == 'image/png') {
									if (imagen_crear_depng($_FILES['imagen']['tmp_name'][$i], $destino, 75, $tamano[0], $tamano[1] )) {
										$msj .= 'Subida con exito ('.$_FILES['imagen']['name'][$i].')<br>	';
									}
								}
								else{
									$msj .= 'Error al subir la imagen '.$_FILES['imagen']['name'][$i].' <br>';
								}
							}
							else{
								$msj .= 'Error al subir la imagen '.$_FILES['imagen']['name'][$i].' <br>';
							}
						}
						else{
							$msj .= 'La imagen debe estar en formato ".jpg" o ".png". <br>';
						}
					}
					else{
						$msj .= 'Hubo un error con el archivo ( "'.$_FILES['imagen']['name'][$i].'" ) <br>';
					}
				}
			}
		}

	//IMAGEN PRINCIPAL
		if (isset($_GET['ficha']) && $_GET['ficha'] == 'imagened' && isset($_POST['principal'])) {
			if (isset($_GET['item']) && !is_null($_GET['item'])) {
				$dir = 'blog/'.$_GET['entrada'];
				$lista = scandir($dir);
				$existe = in_array('principal.jpg', $lista);
				$item = $_GET['item'];
				$principal = 'principal.jpg';
				if($existe === TRUE){
					$medio = 'medio';
					if(	rename('blog/'.$_GET['entrada'].'/'.$principal, 'blog/'.$_GET['entrada'].'/'.$medio) &&
						rename('blog/'.$_GET['entrada'].'/'.$item, 'blog/'.$_GET['entrada'].'/'.$principal) &&
						rename('blog/'.$_GET['entrada'].'/'.$medio, 'blog/'.$_GET['entrada'].'/'.$item)) {
						$msj .= 'Nombre cambiado correctamente.(PRESIONA F5)';
					}
				}
				else{
					if(	rename('blog/'.$_GET['entrada'].'/'.$item, 'blog/'.$_GET['entrada'].'/'.$principal)) {
						$msj .= 'Nombre cambiado correctamente.(PRESIONA F5)';
					}
				}
			}
		}


	//BORRAR IMÁGENES
		if (isset($_GET['ficha']) && $_GET['ficha'] == 'imagened' && isset($_POST['borrar'])) {
			if (isset($_GET['item']) && !is_null($_GET['item'])) {
				$borrar = 'blog/'.$_GET['entrada'].'/'.$_GET['item'];
				if(unlink($borrar)){
					$msj .= 'Borrado  correctamente.';
				}
			}
		}

	//EDITAR TITULO
		if (isset($_GET['ficha']) && $_GET['ficha'] == 'titulo' && isset($_POST['titulo']) && !empty($_POST['titulo'])){
			$titulo = $_POST['titulo'];
			$archivo = fopen('blog/'.$_GET['entrada'].'/titulo.txt', 'w');
			if(fwrite($archivo, $titulo)){
				$msj .= 'Título Cambiado.';
			}
		}

	//EDITAR DESCRIPCION
		if (isset($_GET['ficha']) && $_GET['ficha'] == 'descripcion' && isset($_POST['descripcion']) && !empty($_POST['descripcion'])){
			$descripcion = $_POST['descripcion'];
			$archivo = fopen('blog/'.$_GET['entrada'].'/descripcion.txt', 'w');
			if(fwrite($archivo, $descripcion)){
				$msj .= 'Descripción cambiada.';
			}
		}

	//EDITAR CONTENIDO
		if (isset($_GET['ficha']) && $_GET['ficha'] == 'contenido' && isset($_POST['contenido']) && !empty($_POST['contenido'])){
			$contenido = $_POST['contenido'];
			$archivo = fopen('blog/'.$_GET['entrada'].'/contenido.txt', 'w');
			if(fwrite($archivo, $contenido)){
				$msj .= 'Contenido Cambiado.';
			}
		}

	//BORRAR ARTÍCULO
		if (isset($_GET['ficha']) && $_GET['ficha'] == 'borrart'){
			require_once('php/funciones.php');
			if(borrar_dir('blog/'.$_GET['entrada'])) {
				$msj .= 'Artículo Borrado.';
			}
		}

	//REDIRECCIONAMIENTO
		if (empty($msj)){ unset($msj); }
		if (isset($msj)){ $redir = 'editor.php?entrada='.$_GET['entrada'].'&msj='.$msj; }
		elseif(!isset($msj)){ $redir = 'editor.php?entrada='.$_GET['entrada']; }
		header('location:'.$redir);
	}

//NO EXISTE GET ENTRADA
	else {
		$msj .= 'No indicó el artículo que desea modificar.';
		header('location:editor.php');
	}
?>