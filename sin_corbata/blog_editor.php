<?php
	require('php/arriba_editor.php');

//LISTA DE ENTRADAS
	if(!isset($_GET['entrada'])) {
		echo'
		<div id="blog-editor" class="s-pad-v-3 s-pad-h-1">';

		$dir = 'blog';
		$lista = scandir($dir);
		rsort($lista);
		echo '
			<form action="blog_envio.php?entrada=nueva&red='.$_SERVER['PHP_SELF'].'" method="POST">
				<div class="s12">
					<input class="boton" id="submit" type="submit" value="Crear Nueva"/>
				</div>
			</form>';
		for( $i = 0; $i < sizeof($lista); $i++ ){
			$item = $lista[$i];
			if ($item != '.' && $item != '..') {
				$titulofd = $dir.'/'.$item.'/titulo.txt';
				$titulof = fopen($titulofd, 'r');
				$titulo = fread($titulof,filesize($titulofd));
				fclose($titulof);
				$descripcionfd = $dir.'/'.$item.'/descripcion.txt';
				$descripcionf = fopen($descripcionfd, 'r');
				$descripcion = fread($descripcionf,filesize($descripcionfd));
				fclose($descripcionf);

				echo'
			<section id="sec'.($i+1).'" class="s-pad-v-2">
					<h1 style="display:none">'.$titulo.' - Blog - Marketing Sin Corbata</h1>
				<a href="blog_editor.php?entrada='.$item.'">
					<h3>'.$titulo.'</h3>
				</a>
				<a href="blog_editor.php?entrada='.$item.'">
					<img alt="Imagen Principal" style="height:100px;" src="'.$dir.'/'.$item.'/principal.jpg">
				</a>
				<a href="blog_editor.php?entrada='.$item.'">
					<p style="overflow: hidden; max-height:2.5rem; width:60%; margin:auto">'.$descripcion.'</p>
				</a>
			</section>';
			}
		}
		
		echo'
		</div>';
	}

//ENTRADA NUEVA
	elseif(isset($_GET['entrada']) && $_GET['entrada'] == 'nueva') {
		$dir = 'blog';
		$lista = scandir($dir);
		arsort($lista);
		print_r($lista);
		echo 'entrada nueva';
	}

//EDICION DE ENTRADA
	elseif(isset($_GET['entrada']) && !is_null($_GET['entrada']) && $_GET['entrada'] != 'nueva') {
		$entrada = $_GET['entrada'];

		echo'
		<div class="blog-entrada">
			<h3>IMAGENES</h3>';

//mostrar imagenes actuales
		if(isset($_GET['entrada']) && !is_null($_GET['entrada'])) {
			$dir = 'blog/'.$_GET['entrada'];
			$lista = scandir($dir);
			if (in_array("principal.jpg", $lista)) {
				echo'
			<form class="blog-img" action="blog_envio.php?ficha=imagened&entrada='.$_GET['entrada'].'&item=principal.jpg&red='.$_SERVER['PHP_SELF'].'" method="POST" enctype="multipart/form-data">
				<p>principal.jpg</h3>
				<div style="height:10rem; padding:0.25em;">
					<img alt="imagen" src="blog/'.$_GET['entrada'].'/principal.jpg">
				</div>
				<div class="s12">
					<input class="boton" id="submit" name="borrar" type="submit" value="borrar"/>
				</div>
			</form>';
			}
			
			for( $i = 0; $i < sizeof($lista); $i++ ){
				$item = $lista[$i];
				if ($item != '.' && $item != '..' && $item != 'contenido.txt' && $item != 'descripcion.txt' && $item != 'titulo.txt' && $item != 'principal.jpg') {
// Borrar y Principal
				echo'
			<form class="blog-img" action="blog_envio.php?ficha=imagened&entrada='.$_GET['entrada'].'&item='.$item.'&red='.$_SERVER['PHP_SELF'].'" method="POST" enctype="multipart/form-data">
				<p>'.$item.'</h3>
				<div style="height:10rem; padding:0.25em;">
					<img alt="imagen" src="'.$dir.'/'.$item.'">
				</div>
				<div class="s12">
					<input class="boton" id="submit" name="borrar" type="submit" value="borrar"/>
				</div>
				<div class="s12">
					<input class="boton" id="submit" name="principal" type="submit" value="principal"/>
				</div>
			</form>';
				}
			}
		}

//subir imagenes
			echo'
			<form action="blog_envio.php?ficha=imagen&entrada='.$_GET['entrada'].'&red='.$_SERVER['PHP_SELF'].'" method="POST" enctype="multipart/form-data">
				<div class="s12">
					<input class="entrada" id="imagen" name="imagen[]" type="file" multiple="multiple" accept="image/*"  required=""/>
				</div>
				<div class="s12">
					<input class="boton" id="submit" type="submit" value="Subir imagen(es)"/>
				</div>
			</form>
			';

//editor titulo
		$entrada_nombre = 'blog/'.$_GET['entrada'].'/titulo.txt';
		$entrada = fopen($entrada_nombre, 'r');
		$titulo = fread($entrada,filesize($entrada_nombre));
		fclose($entrada);
		$entrada_nombre = 'blog/'.$_GET['entrada'].'/contenido.txt';
		$entrada = fopen($entrada_nombre, 'r');
		$contenido = fread($entrada,filesize($entrada_nombre));
		fclose($entrada);
		$entrada_nombre = 'blog/'.$_GET['entrada'].'/descripcion.txt';
		$entrada = fopen($entrada_nombre, 'r');
		$descripcion = fread($entrada,filesize($entrada_nombre));
		fclose($entrada);
		echo'
			<h3>TITULO</h3>
			<form action="blog_envio.php?ficha=titulo&entrada='.$_GET['entrada'].'&red='.$_SERVER['PHP_SELF'].'" method="POST">
				<div class="s12">
					<input class="entrada" type="text" name="titulo" placeholder="Título del artículo" value ="'.$titulo.'" font-family:arial" required>
				</div>
				<div class="s12">
					<input class="boton" id="submit" type="submit" value="Guardar"/>
				</div>
			</form>
		';

//editor descripcion
		echo'
			<h3>DESCRIPCION</h3>
			<form action="blog_envio.php?ficha=descripcion&entrada='.$_GET['entrada'].'&red='.$_SERVER['PHP_SELF'].'" method="POST">
				<div class="s12">
					<textarea class="entrada" name="descripcion" placeholder="Sinopsis del artículo" style="min-height: 5rem; font-family:arial" rows="5" required > '.$descripcion.' </textarea>
				</div>
				<div class="s12">
					<input class="boton" id="submit" type="submit" value="Guardar"/>
				</div>
			</form>
		';

//editor contenido
		echo'
			<h3>CONTENIDO</h3>
			<div class="blog" id="muestra" style="padding-top: 1em;">'.$contenido.'</div>
			<form action="blog_envio.php?ficha=contenido&entrada='.$_GET['entrada'].'&red='.$_SERVER['PHP_SELF'].'" method="POST">
				<div class="s12">
					<textarea class="entrada" id="texto" name="contenido" placeholder="Contenido del artículo" style="min-height: 5rem; font-family:arial" rows="5" required onkeyup = "texto_cambia()"> '.$contenido.' </textarea>
				</div>
				<div class="s12">
					<input class="boton" id="submit" type="submit" value="Guardar"/>
				</div>
			</form>
		</div>';
	}

?>