<!DOCTYPE html>
<html lang="es" xml:lang="es" xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Marketing Sin Corbata EDITOR</title>
	<link rel="icon" href="img/web/sc1.svg">
	<meta name="ROBOTS" content="INDEX,NOFOLLOW">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0;">
	<link rel="stylesheet" type="text/css" href="css/base.min.css">
	<script src="https://code.jquery.com/jquery-2.2.3.min.js"></script>
	<script src="js/base.min.js"></script>
</head>

<body>
	<div class="editor-header">
		<a href="blog_editor.php">
			<div>
				<div style="overflow:initial; ">
					<img alt="Ada Salas - Marketing Sin Corbata" src="img/web/sc2.svg">
				</div>	
			</div>	
		</a>
	</div>
	<div id="cuerpo" class="cuerpo1">

<?php

	$telefono1	= '940235704';
	$telefono2	= '973699467';
	$email		= 'fahedsl@mail.com';

	$c_nombre = 'Tu nombre';
	$c_email = 'Tu correo electrónico';
	$c_mensaje = '¿Quieres información, una cotización, decirnos hola?';
	$c_boton = 'Enviar';

	echo'
	<section class="s12 fondo1">
		<div class="color2 s-pad-v-2 m-pad-v-3 l-pad-v-4 s-pad-h-1 m-pad-h-2">
			<h3>Fahed Stanic\'</h3>
			<div class="s12">
				<div class="s12 centrado">
					<div class="s-pad-v-1"  style="font-weight: bold">
						<img alt="caricatura de Fahed"  class="s-pad-b-1 m-pad-b-2 w-max-15" src="img/web/fahed.svg">
						<p class="centrado">
							"Quiero aprender francés."
						</p>
						<p class="justificado d-inline-block">
							-Marketing<br>
							-Programación Web<br>
							-Diseño Gráfico<br>
							-Fotografía<br>
							-Redes Sociales<br>
						</p>
					</div>
				</div>
			</div>
			<div class="s12">
				<div class="item4">';

	//Iconos de contacto
	if (isset($telefono1) && !is_null($telefono1) && !empty($telefono1)) {
		echo '
					<a class="vinculo" href="tel:'.$telefono1.'" title="Llamamé, abababa.">
						<span class="">
							<img alt="teléfono" src="img/web/telf.svg" style="height:1.5rem; width:1.5rem; margin: 0 0.3em">
						</span>
						<span class="">'.$telefono1.'</span>
					</a>';
	}
	if (isset($email) && !is_null($email) && !empty($email)) {
		echo '
					<a class="vinculo" href="mailto:'.$email.'" title="Envíanos un correo electrónico, o usa el formulario de abajo. :)">
						<span class="correo electrónico">
							<img alt="" src="img/web/email.svg" style="height:1.5rem; width:1.5rem; margin: 0 0.3em">
						</span>
						<span class="">'.$email.'</span>
					</a>';
	}
	echo '
					<a href="Contacto - Fahed Stanic.vcf" title="Descárgate mi tarjeta virtual de contacto.">
						<span class="">
							<img alt="Tarjeta de contacto" src="img/web/tarjeta.svg" style="height:1.5rem; width:1.5rem; margin: 0 0.3em">
						</span>
						<span class="">VCard</span>
					</a>
				</div>
			</div>';
	echo '
		</div>
	</section>';
?>
	</div>
</body>
</html>