<?php
	require('php/arriba.php');
?>
<!-- POR QUE SIN CORBATA -->
	<section class="sec s-pad-v-2 m-pad-v-3 l-pad-v-4 s-pad-h-1 m-pad-h-2">
		<div class="item1 s-pad-h-1 m-pad-h-2">
			<h3 class="s-marg-b-1 m-marg-b-2">La razon de ser "Sin Corbata"</h3>
			<p class="s-pad-1 m-pad-2 font-13" style="text-align: justify;">
				Sin corbata nació con el fin común de los fundadores de decirle adiós a la dificultad y poca efectividad en el trato y trabajo con los clientes. Hemos dejado en los armarios nuestras corbatas y toda la burocracia que traen con ellas, para hacerlo todo más sencillo, con soluciones efectivas y sin demasiado trámite.
			</p>
		</div>
	</section>
<!-- DESCRIPCION DE NOSOTROS -->
	<section class="sec fondo1">
		<div class="color4 t-color2 s-pad-v-2 m-pad-v-3 l-pad-v-4 s-pad-h-1 m-pad-h-2">
			<div class="s12">
				<div class="s12 m6 centrado">
					<div class="s-pad-v-1" style="font-weight: bold">
						<a href="ada.php">
							<img alt="caricatura de Ada"  class="s-pad-b-1 m-pad-b-2 w-max-15" src="img/web/ada.svg">
						</a>
						<a href="ada.php">
							<h5>ADA</h5>
						</a>
						<p class="centrado">
							"Me encanta usar Corel Draw."
						</p>
						<p class="justificado d-inline-block">
							-Marketing<br>
							-Diseño Gráfico<br>
							-Diseño de Marca<br>
							-R.R.P.P<br>
							-Redes Sociales<br>
						</p>
					</div>
				</div>
				<div class="s12 m6 centrado">
					<div class="s-pad-v-1"  style="font-weight: bold">
						<a href="fahed.php">
							<img alt="caricatura de Fahed"  class="s-pad-b-1 m-pad-b-2 w-max-15" src="img/web/fahed.svg">
						</a>
						<a href="fahed.php">
							<h5>FAHED</h5>
						</a>
						<p class="centrado">
							"Quiero aprender francés."
						</p>
						<p class="justificado d-inline-block">
							-Marketing<br>
							-Programación Web<br>
							-Diseño Gráfico<br>
							-Fotografía<br>
							-Redes Sociales<br>
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php
	require('php/abajo.php');
?>