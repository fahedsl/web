<?php
	require('php/check.php');
	require('php/sql.php');
	require('php/funciones.php');
	$msj = '';

//NUEVA ENTRADA
	if(isset($_GET['entrada']) && $_GET['entrada'] == 'nueva'){
		$dir = 'blog';
		$lista = scandir($dir);
		$numero = max($lista) + 1;
		mkdir($dir.'/'.$numero);
		$titulo = $dir.'/'.$numero.'/titulo.txt';
		$contenido = $dir.'/'.$numero.'/contenido.txt';
		$archivo1 = fopen($titulo, 'w');
		fwrite($archivo1 , "Título generico");
		fclose($archivo1);
		$archivo2 = fopen($contenido, 'w');
		fwrite($archivo2 , "Contenido Genérico");
		fclose($archivo2);
		chmod($dir.'/'.$numero , 0777);
		chmod($titulo , 0777);
		chmod($contenido , 0777);
		header('location:blog_editor.php?entrada='.$numero);
	}

//ENTRADA EXISTENTE
	elseif(isset($_GET['entrada']) && $_GET['entrada'] != 'nueva' && isset($_GET['ficha'])){
	//SUBIR IMÁGENES
		if (isset($_GET['ficha']) && $_GET['ficha'] == 'imagen') {
			if (isset($_FILES['imagen'])) {
			$cantidad_imagenes = sizeof($_FILES['imagen']['name']) -1;
				for ($i=0; $i<=$cantidad_imagenes; $i++) {
					//verificar tamaño de imagen
					if($_FILES['imagen']['size'][$i] < 50000000){
						//verificar problemas de nombre
						if($_FILES['imagen']['tmp_name'][$i] !== ''){
							//verificar formato de imagen
							$imagen = getimagesize($_FILES['imagen']['tmp_name'][$i]);
							if ($imagen['mime'] == 'image/jpeg') {
								//IMAGEN VERIFICADA!
								//seleccionar nombre
								$dir = 'blog/'.$_GET['entrada'];
								$lista = scandir($dir);
								$borrar = array('principal.jpg', 'titulo.txt', 'contenido.txt', '..', '.');
								foreach ($borrar as $valor) {
									$key = array_search($valor, $lista);
									unset($lista[$key]);
								}
								
								$lista = max($lista);
								$destino = substr($lista, 0, 1);
								$destino ++;
								$destino = $dir.'/'.$destino.'.jpg';

								//calcular tamaño
								if ($tamano = imagen_tamano_ajustar($imagen[0], $imagen[1], 600, 600)) {
								echo "2";
								//crear imagen
									if (imagen_crear_dejpg($_FILES['imagen']['tmp_name'][$i], $destino, 75, $tamano[0], $tamano[1] )) {
										$msj .= 'Subida con exito ('.$_FILES['imagen']['name'][$i].')<br>	';
									}
									else{
										$msj .= 'Error al subir la imagen '.$_FILES['imagen']['name'][$i].' <br>';
									}
								}
								else{
									$tamano = imagen_tamano_ajustar($imagen[0], $imagen[1], 600, 600);
									var_dump($tamano);
									echo "1";
									$msj .= 'Error al subir la imagen '.$_FILES['imagen']['name'][$i].' <br>';
								}
							}
							else{
								$msj .= 'La imagen debe estar en formato ".jpg". <br>';
							}
						}
						else{
							$msj .= 'Hubo un error con el archivo ( "'.$_FILES['imagen']['name'][$i].'" ) <br>';
						}
					}
					else{
						$msj .= 'El archivo es demasiado grande. ( "'.$_FILES['imagen']['name'][$i].'" ).<br>';
					}
				}
			}
		}

	//IMAGEN PRINCIPAL
		if (isset($_GET['ficha']) && $_GET['ficha'] == 'imagened' && isset($_POST['principal'])) {
			if (isset($_GET['item']) && !is_null($_GET['item'])) {
				$dir = 'blog/'.$_GET['entrada'];
				$lista = scandir($dir);
				$existe = in_array('principal.jpg', $lista);
				$item = $_GET['item'];
				$principal = 'principal.jpg';
				if($existe === TRUE){
					$medio = 'medio';
					if(	rename('blog/'.$_GET['entrada'].'/'.$principal, 'blog/'.$_GET['entrada'].'/'.$medio) &&
						rename('blog/'.$_GET['entrada'].'/'.$item, 'blog/'.$_GET['entrada'].'/'.$principal) &&
						rename('blog/'.$_GET['entrada'].'/'.$medio, 'blog/'.$_GET['entrada'].'/'.$item)) {
						$msj .= 'Nombre cambiado correctamente.(PRESIONA F5)';
					}
				}
				else{
					if(	rename('blog/'.$_GET['entrada'].'/'.$item, 'blog/'.$_GET['entrada'].'/'.$principal)) {
						$msj .= 'Nombre cambiado correctamente.(PRESIONA F5)';
					}
				}
			}
		}


	//BORRAR IMÁGENES
		if (isset($_GET['ficha']) && $_GET['ficha'] == 'imagened' && isset($_POST['borrar'])) {
			if (isset($_GET['item']) && !is_null($_GET['item'])) {
				$borrar = 'blog/'.$_GET['entrada'].'/'.$_GET['item'];
				if(unlink($borrar)){
					$msj .= 'Borrado  correctamente.';
				}
			}
		}

	//EDITAR TITULO
		if (isset($_GET['ficha']) && $_GET['ficha'] == 'titulo' && isset($_POST['titulo']) && !empty($_POST['titulo'])){
			$titulo = $_POST['titulo'];
			$archivo = fopen('blog/'.$_GET['entrada'].'/titulo.txt', 'w');
			if(fwrite($archivo, $titulo)){
				$msj .= 'Título Cambiado.';
			}
		}

	//EDITAR CONTENIDO
		if (isset($_GET['ficha']) && $_GET['ficha'] == 'contenido' && isset($_POST['contenido']) && !empty($_POST['contenido'])){
			$contenido = $_POST['contenido'];
			$archivo = fopen('blog/'.$_GET['entrada'].'/contenido.txt', 'w');
			if(fwrite($archivo, $contenido)){
				$msj .= 'Contenido Cambiado.';
			}
		}

	//REDIRECCIONAMIENTO
		if (empty($msj)){ unset($msj); }
		if (isset($msj)){ $redir = 'blog_editor.php?entrada='.$_GET['entrada'].'&msj='.$msj; }
		elseif(!isset($msj)){ $redir = 'blog_editor.php?entrada='.$_GET['entrada']; }
		header('location:'.$redir);
	}

//NO EXISTE GET ENTRADA
	else {
		$msj .= 'No indicó el artículo que desea modificar.';
		header('location:blog_editor.php');
	}
?>