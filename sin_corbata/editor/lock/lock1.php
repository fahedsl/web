<?php
	if (isset($_GET['red']) && isset($_POST['pase'])) {
		if (session_status() == PHP_SESSION_NONE) {
			session_start();
			if (!isset($_SESSION['pase'])) {
				$_SESSION['pase'] = '0';
			}
		}

		if ($_SESSION['pase'] == 0) {
			if (isset($_SESSION['pase'])) {
				require('../../php/sql.php');

				$sql = 'select pase from info';
				$result = $conn->query($sql);
				$conn -> close();

				if ($result->num_rows > 0) {
					$row = $result->fetch_assoc();
					if ($row['pase'] == $_POST['pase']){
						$_SESSION['pase'] = 1;
					}
				}
				header('location:'.$_GET['red']);
			}
		}
		elseif ($_SESSION['pase'] == 1) { header('location:../index.php'); }
	}
	else{ header('location:../index.php');}
?>