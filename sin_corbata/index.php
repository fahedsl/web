<?php
	require('php/arriba.php');
?>
<!-- PALABRA QUE CAMBIA -->
	<section class="d-flex fondo1 h-min-25 centrado">
		<h3 class="palabra "> Hagámoslo <span style="overflow-y: initial;" id="palabra">simple<span></h3>
		<ul>
			<li class="">bonito</li>
			<li class="">fácil</li>
			<li class="">eficiente</li>
			<li class="">rápido</li>
			<li class="">claro</li>
			<li class="">diferente</li>
			<li class="">moderno</li>
			<li class="">simple</li>
		</ul>
	</section>

<!-- RAZONES PARA ESTAR SIN CORBATA -->
	<section class="s-pad-2 m-pad-3 l-pad-4">
		<div class="s12">
			<div class="">
				<h3>Razones para estar "Sin Corbata"</h3>
			</div>
		</div>
		<div class="s12 m6 s-pad-t-2 m-pad-t-3 l-pad-t-4 s-pad-h-1">
			<a href="nosotros.php">
				<div class="s12 l6">
					<img alt="Hablamos el mismo idioma" class="s-tam-h-6 m-tam-h-8" src="img/razones/1.svg">
				</div>
				<div class="s12 l6 s-pad-t-1 l-pad-t-0">
					<h5 class="s12 mayuscula">Hablamos el mismo idioma</h5>
					<p class="s12 justificado">
						Nuestro lenguaje es simple y ameno, siempre procuramos estar cerca de nuestros clientes para facilitar el intercambio de ideas y así lograr los resultados deseados.
					</p>
				</div>
			</a>
		</div>
		<div class="s12 m6 s-pad-t-2 m-pad-t-3 l-pad-t-4 s-pad-h-1">
			<a href="nosotros.php">
				<div class="s12 l6">
					<img alt="Sin complicaciones" class="s-tam-h-6 m-tam-h-8" src="img/razones/2.svg">
				</div>
				<div class="s12 l6 s-pad-t-1 l-pad-t-0">
					<h5 class="s12 mayuscula">Sin complicaciones</h5>
					<p class="s12 justificado">
						Porque no te complicamos con cosas que tu empresa no necesita, por esto no contamos con paquetes estáticos, por el contrario, agrupamos los servicios que REALMENTE necesitas.
					</p>
				</div>
			</a>
		</div>
		<div class="s12 m6 s-pad-t-2 m-pad-t-3 l-pad-t-4 s-pad-h-1">
			<a href="nosotros.php">
				<div class="s12 l6">
					<img alt="Ideas frescas" class="s-tam-h-6 m-tam-h-8" src="img/razones/3.svg">
				</div>
				<div class="s12 l6 s-pad-t-1 l-pad-t-0">
					<h5 class="s12 mayuscula">Ideas frescas</h5>
					<p class="s12 justificado">
						Porque somos una empresa conformada por profesionales jóvenes, con ideas frescas y a la vanguardia de las tendencias que existen en marketing y publicidad.
					</p>
				</div>
			</a>
		</div>
		<div class="s12 m6 s-pad-t-2 m-pad-t-3 l-pad-t-4 s-pad-h-1">
			<a href="nosotros.php">
				<div class="s12 l6">
					<img alt="Nunca estarás solo" class="s-tam-h-6 m-tam-h-8" src="img/razones/4.svg">
				</div>
				<div class="s12 l6 s-pad-t-1 l-pad-t-0">
					<h5 class="s12 mayuscula">Nunca estarás solo</h5>
					<p class="s12 justificado">
						Estamos comprometidos contigo en apoyarte en tu emprendimiento, es por esto que estamos disponibles ante cualquier duda o problema, brindándote soluciones creativas y eficientes.
					</p>
				</div>
			</a>
		</div>
		<div class="s12 m6 s-pad-t-2 m-pad-t-3 l-pad-t-4 s-pad-h-1">
			<a href="nosotros.php">
				<div class="s12 l6">
					<img alt="Diagnostico gratuito" class="s-tam-h-6 m-tam-h-8" src="img/razones/5.svg">
				</div>
				<div class="s12 l6 s-pad-t-1 l-pad-t-0">
					<h5 class="s12 mayuscula">Diagnostico gratuito</h5>
					<p class="s12 justificado">
						Porque te daremos un diagnostico gratuito de la necesidades de tu empresa, para que de esa manera entendamos juntos cuales puntos se necesita mejorar.
					</p>
				</div>
			</a>
		</div>
		<div class="s12 m6 s-pad-t-2 m-pad-t-3 l-pad-t-4 s-pad-h-1">
			<a href="nosotros.php">
				<div class="s12 l6">
					<img alt="Amamos lo verde" class="s-tam-h-6 m-tam-h-8" src="img/razones/6.svg">
				</div>
				<div class="s12 l6 s-pad-t-1 l-pad-t-0">
					<h5 class="s12 mayuscula">Amamos lo verde</h5>
					<p class="s12 justificado">
						Porque somos una empresa socialmente responsable con la ecología y la comunidad, es por esto que cuidamos el uso de materiales y evitamos el uso de recursos que dañen el medio ambiente.
					</p>
				</div>
			</a>
		</div>
	</section>
<!-- PORTAFOLIO 
	<section class="fondo1">
		<div class="color3 s-pad-v-1 m-pad-t-2 l-pad-v-3 s-pad-h-1">
			<div class="s12">
				<h3 style="color: black;">Nuestro Portafolio</h3>
			</div>

	$archivo = 'img/trabajos';
	$marcas = scandir($archivo);
	foreach ($marcas as $nombre) {
		if(($nombre != '.') && ($nombre != '..')){
			$imagen = $archivo.'/'.$nombre.'/marca.jpg';
			echo'
			<div class="s12 m6 l4 s-pad-t-2 m-pad-t-3 l-pad-t-4 s-pad-h-1">
				<a href="trabajos.php?archivo='.$nombre.'">
					<h4 style="color:black;">'.$nombre.'</h4>
				</a>
				<a href="trabajos.php?archivo='.$nombre.'">
						<img alt="'.$nombre.'" class="s-pad-1" src="'.$imagen.'" style="width: 100%;">
				</a>
			</div>';
		}
	}
	echo '
		</div>
	</section>';
-->
<?php
	require('php/abajo.php');
?>