		</div>
		<div class="pre-footer"></div>
		<footer class="fondo2 s-pad-2">
			<div>
				<p>
					<a href="contacto.php" title="Este link te envía a la página de contacto">Contáctanos</a>, tenemos golosinas  :)
				</p>
				<p>
					<a href="Contacto - Marketing Sin Corbata.vcf" target="_blank" title="Descárgate nuestra tarjeta virtual de contacto.">Tarjeta Virtual de Contacto</a>
				</p>
				<a class="d-block" href="https://www.facebook.com/mktsincorbata" target="_blank" title="Dale like a nuestro hermoso Facebook.">
					<img alt="Nuestro Facebook" style="margin: 0;width: 2em; height: 2em;" src="img/web/facebook.svg">
				</a>
			</div>
		</footer>
		<div style="">
			<a class="copyright" href="https://www.facebook.com/mktsincorbata" target="_blank" title="La hicimos nosotros mismos... somos el éxito ^_^">
				<p>
					Hecho por Marketing Sin Corbata - <?php echo date("Y"); ?>
				</p>
			</a>
		</div>
	</body>
</html>
<?php
	if(isset($conn)){
		$conn -> close();
		unset($conn);
	}
?>