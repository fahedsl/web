<?php
	require('php/sql.php');
	$sql = 'select * from info';
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		$row = $result->fetch_assoc();
		if (!isset($row['url']))		{ $row['url'] = ''; }
		if (!isset($row['icono']))		{ $row['icono'] = ''; }
		if (!isset($row['titulo']))		{ $row['titulo'] = ''; }
		if (!isset($row['descripcion']))	{ $row['descripcion'] = ''; }
		if (!isset($row['facebookid']))	{ $row['facebookid'] = ''; }
		if (!isset($row['twitter']))	{ $row['twitter'] = ''; }
		if (!isset($row['pinterest']))	{ $row['pinterest'] = ''; }
		$tipo = 'website';

		if ($_SERVER['PHP_SELF'] == '/blog.php') {
			if(isset($_GET['entrada'])) {
				$titulofd = 'blog/'.$_GET['entrada'].'/titulo.txt';
				$titulof = fopen($titulofd, 'r');
				$titulo = fread($titulof,filesize($titulofd));
				fclose($titulof);
				$descripcionfd = 'blog/'.$_GET['entrada'].'/descripcion.txt';
				$descripcionf = fopen($descripcionfd, 'r');
				$descripcion = fread($descripcionf,filesize($descripcionfd));
				fclose($descripcionf);

				$row['url'] = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
				$row['titulo'] = $titulo.' - Marketing Sin Corbata';
				$row['descripcion'] = $descripcion;
				$tipo = 'article';
			}
		}
		echo'
<!DOCTYPE html>
	<html lang="es" xml:lang="es" xmlns="http://www.w3.org/1999/xhtml"
	xmlns="https://www.w3.org/1999/xhtml" 
	xmlns:og="https://ogp.me/ns#" 
	xmlns:fb="https://www.facebook.com/2008/fbml">
	<head>
		<!--Meta tagss-->
		<link rel="canonical" href="'.$row['url'].'">
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0;">
		<meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="title" content="'.$row['titulo'].'">
		<meta name="description" content="'.$row['descripcion'].'">
		<title>'.$row['titulo'].'</title>

		<link rel="icon" href="img/web/sc1.svg">
		<link rel="shortcut icon" href="img/web/sc1.svg">
		<link rel="image_src" href="http://www.sincorbata.com/img/web/sc1.svg">

		<meta name="ROBOTS" content="INDEX,FOLLOW">
		<meta name="google" value="notranslate">

		<!--Open graph-->
		<meta property="og:title" content="'.$row['titulo'].'"/>
		<meta property="og:type" content="'.$tipo.'"/>
		<meta property="og:url" content="'.$row['url'].'"/>
		<meta property="og:image" content="http://www.mktsincorbata.com/'.$row['icono'].'"/>
		<meta property="og:description" content="'.$row['descripcion'].'"/>
		';
		if (isset($row['facebookid']) && !empty($row['facebookid'])) {
			echo'
		<meta property="fb:pages" content="1676997485853698"/>
		<meta property="fb:app_id" content="'.$row['facebookid'].'"/>
			';
		}
		if (isset($row['twitter']) && !empty($row['twitter'])) {
			echo'
		<!--Twitter cards-->
		<meta name="twitter:card" content="summary" />
		<meta name="twitter:title" content="'.$row['titulo'].'">
		<meta name="twitter:description" content="'.$row['descripcion'].'">
		<meta name="twitter:image" content="'.$row['url'].'/'.$row['icono'].'">
		<meta name="twitter:site" content="'.$row['twitter'].'">
			';
		}
		if (isset($row['pinterest']) && !empty($row['pinterest'])) {
			echo'
		<!--Pinterest-->
		<meta name="p:domain_verify" content="'.$row['pinterest'].'"/>
		';
		}
		echo'
		<!--Estilo-->
		<link rel="stylesheet" type="text/css" href="css/base.min.css">
		
		<!--Scripts-->
		<script src="https://code.jquery.com/jquery-2.2.3.min.js"></script>
		<script src="js/base.js"></script>
	</head>

	<body>
		';
		if (isset($row['facebookid']) && !is_null($row['facebookid']) && $row['facebookid'] != '') {
			echo'
		<!--Facebook-->
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.8&appId='.$row['facebookid'].'";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, "script", "facebook-jssdk"));</script>
			';
		}
		echo'
	<div class="header">
		<div class="menu-barra">
			<div class="logo-out d-flex centrado">
				<span class="logo-in">
					<a href="index.php">
						<img alt="Marketing Sin Corbata" src="img/web/sc2.svg">
					</a>
				</span>
			</div>
			<a id="menu-boton" onclick="menu_on()">
				<div class="menu-icono-out">
					<div id="menu-icono" class="menu-icono-off">
						<span></span>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div id="menu-pop" class="menu-pop-off">
		<ul class="menu-lista">
			<li class="menu-item">
				<a href="index.php">
					<p class="menu-p-off">Inicio</p>
				</a>
			</li>
			<li class="menu-item">
				<a href="nosotros.php">
					<p class="menu-p-off">Nosotros</p>
				</a>
			</li>
			<li class="menu-item">
				<a href="servicios.php">
					<p class="menu-p-off">Servicios</p>
				</a>
			</li>
			<li class="menu-item">
				<a href="contacto.php">
					<p class="menu-p-off">Contacto</p>
				</a>
			</li>
			<li class="menu-item">
				<a href="blog.php" target="_blank">
					<p class="menu-p-off" style="color: #E866BA">Blog</p>
				</a>
			</li>
		</ul>
	</div>
		';
		if(isset($_GET['msj'])){
			echo '	
	<div class="pop">
		'.$_GET['msj'].'
	</div>
			';
		}
		echo '
	<div id="overlay" class="overlay-off" onclick="menu_off()"></div>
	<div id="cuerpo" class="cuerpo1">
			';
	}
?>