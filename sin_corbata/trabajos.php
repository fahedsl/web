<?php
	require('php/arriba.php');
?>

		<div class="galeria">
			<div id="galeria" class="grid"  style="overflow: hidden;">
				<div class="grid-sizer"></div>

<?php
	$principal = 'img/trabajos';
	$carpeta1 = scandir($principal.'/'.$_GET['archivo']);
	foreach ($carpeta1 as $imagen) {
		if(($imagen != '.') && ($imagen != '..') && ($imagen != 'mini') && ($imagen != 'marca.jpg')){
			$miniatura = $principal.'/'.$_GET['archivo'].'/mini/'.$imagen;
			$galeria = $principal.'/'.$_GET['archivo'].'/'.$imagen;
			echo '
					<div class="grid-item" data-src="'.$galeria.'" data-sub-html="">
						<div class="img_info">
							<div class="hmedio vmedio">
							</div>
						</div>
						<img alt="Imagen de nuestros trabajos" src="'.$miniatura.'">
					</div>';
			}
		}
?>
			</div>
		</div>

<?php
	require('php/abajo.php');
?>
	<script src="js/lightgallery.min.js"></script>
	<script src="js/imagesloaded.min.js"></script>
	<script src="js/masonry.min.js"></script>
	<script>
		var grid = document.querySelector('.grid');
		var msnry;

		imagesLoaded( grid, function() {
		  msnry = new Masonry( grid, {
			itemSelector: '.grid-item',
		  });
		});
	</script>
	<script>
		$(document).ready(function() {
			$("#galeria").lightGallery({
				download:false,
				loop:false,
			});
		});
	</script>