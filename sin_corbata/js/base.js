//Funciones que abren y cierran el menu principal, respectivamente.
function menu_on() {
	var boton = document.getElementById("menu-boton");
	var icono = document.getElementById("menu-icono");
	var menu = document.getElementById("menu-pop");
	var fondo = document.getElementById("overlay");
	var textos = document.getElementsByClassName("menu-p-off");

	boton.setAttribute("onClick", "menu_off()");
	icono.setAttribute("class", "menu-icono-on");
	fondo.setAttribute("class", "overlay-on");
	menu.setAttribute("class", "menu-pop-on");
	for (var i = textos.length - 1; i >= 0; i--) {
		textos[i].setAttribute("class", "menu-p-on");
	}
}

function menu_off() {
	var boton = document.getElementById("menu-boton");
	var icono = document.getElementById("menu-icono");
	var menu = document.getElementById("menu-pop");
	var fondo = document.getElementById("overlay");
	var textos = document.getElementsByClassName("menu-p-on");

	boton.setAttribute("onClick", "menu_on()");
	icono.setAttribute("class", "menu-icono-off");
	fondo.setAttribute("class", "overlay-off");
	menu.setAttribute("class", "menu-pop-off");
	for (var i = textos.length - 1; i >= 0; i--) {
		textos[i].setAttribute("class", "menu-p-off");
	}
}

//Cambia el texto del editor de blog
function texto_cambia(){
	var texto =	document.getElementById("texto").value;
	document.getElementById("muestra").innerHTML = texto;
}

//Cerrar el menu principal al apretar ESC
$(document).keyup(function(esc) {
	if (esc.keyCode == 27) {
		menu_off();
	}
});

window.onload =	function(){
//quita el BLUR cuando se carga la página
	document.getElementById("cuerpo").setAttribute("class", "cuerpo2");
//Cambia "la palabra" en index.php
	if(document.getElementById("palabra") !== null){
		var palabras = ["bonito","fácil","eficiente","rápido", "claro", "diferente", "moderno", "simple"];
		var i = 0;
		setTimeout(	function(){
						setInterval(function(){
							var lista = palabras.length;
							var a = palabras[i];
							document.getElementById("palabra").innerHTML = a;
							if(i == lista-1){i = 0;} else { i++; }
						},1300)
					},1000)
	}
}