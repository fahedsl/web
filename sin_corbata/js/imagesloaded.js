(function() {
    function a() {}

    function c(a, b) {
        for (var c = a.length; c--;)
            if (a[c].listener === b) return c;
        return -1
    }

    function d(a) {
        return function() {
            return this[a].apply(this, arguments)
        }
    }
    var b = a.prototype;
    b.getListeners = function(b) {
        var d, e, c = this._getEvents();
        if ("object" == typeof b) {
            d = {};
            for (e in c) c.hasOwnProperty(e) && b.test(e) && (d[e] = c[e])
        } else d = c[b] || (c[b] = []);
        return d
    }, b.flattenListeners = function(b) {
        var d, c = [];
        for (d = 0; d < b.length; d += 1) c.push(b[d].listener);
        return c
    }, b.getListenersAsObject = function(b) {
        var d, c = this.getListeners(b);
        return c instanceof Array && (d = {}, d[b] = c), d || c
    }, b.addListener = function(b, d) {
        var g, e = this.getListenersAsObject(b),
            f = "object" == typeof d;
        for (g in e) e.hasOwnProperty(g) && c(e[g], d) === -1 && e[g].push(f ? d : {
            listener: d,
            once: !1
        });
        return this
    }, b.on = d("addListener"), b.addOnceListener = function(b, c) {
        return this.addListener(b, {
            listener: c,
            once: !0
        })
    }, b.once = d("addOnceListener"), b.defineEvent = function(b) {
        return this.getListeners(b), this
    }, b.defineEvents = function(b) {
        for (var c = 0; c < b.length; c += 1) this.defineEvent(b[c]);
        return this
    }, b.removeListener = function(b, d) {
        var f, g, e = this.getListenersAsObject(b);
        for (g in e) e.hasOwnProperty(g) && (f = c(e[g], d), f !== -1 && e[g].splice(f, 1));
        return this
    }, b.off = d("removeListener"), b.addListeners = function(b, c) {
        return this.manipulateListeners(!1, b, c)
    }, b.removeListeners = function(b, c) {
        return this.manipulateListeners(!0, b, c)
    }, b.manipulateListeners = function(b, c, d) {
        var e, f, g = b ? this.removeListener : this.addListener,
            h = b ? this.removeListeners : this.addListeners;
        if ("object" != typeof c || c instanceof RegExp)
            for (e = d.length; e--;) g.call(this, c, d[e]);
        else
            for (e in c) c.hasOwnProperty(e) && (f = c[e]) && ("function" == typeof f ? g.call(this, e, f) : h.call(this, e, f));
        return this
    }, b.removeEvent = function(b) {
        var e, c = typeof b,
            d = this._getEvents();
        if ("string" === c) delete d[b];
        else if ("object" === c)
            for (e in d) d.hasOwnProperty(e) && b.test(e) && delete d[e];
        else delete this._events;
        return this
    }, b.removeAllListeners = d("removeEvent"), b.emitEvent = function(b, c) {
        var e, f, g, h, d = this.getListenersAsObject(b);
        for (g in d)
            if (d.hasOwnProperty(g))
                for (f = d[g].length; f--;) e = d[g][f], e.once === !0 && this.removeListener(b, e.listener), h = e.listener.apply(this, c || []), h === this._getOnceReturnValue() && this.removeListener(b, e.listener);
        return this
    }, b.trigger = d("emitEvent"), b.emit = function(b) {
        var c = Array.prototype.slice.call(arguments, 1);
        return this.emitEvent(b, c)
    }, b.setOnceReturnValue = function(b) {
        return this._onceReturnValue = b, this
    }, b._getOnceReturnValue = function() {
        return !this.hasOwnProperty("_onceReturnValue") || this._onceReturnValue
    }, b._getEvents = function() {
        return this._events || (this._events = {})
    }, "function" == typeof define && define.amd ? define("eventEmitter/EventEmitter", [], function() {
        return a
    }) : "object" == typeof module && module.exports ? module.exports = a : this.EventEmitter = a
}).call(this),
    function(a) {
        var b = document.documentElement,
            c = function() {};
        b.addEventListener ? c = function(a, b, c) {
            a.addEventListener(b, c, !1)
        } : b.attachEvent && (c = function(b, c, d) {
            b[c + d] = d.handleEvent ? function() {
                var b = a.event;
                b.target = b.target || b.srcElement, d.handleEvent.call(d, b)
            } : function() {
                var c = a.event;
                c.target = c.target || c.srcElement, d.call(b, c)
            }, b.attachEvent("on" + c, b[c + d])
        });
        var d = function() {};
        b.removeEventListener ? d = function(a, b, c) {
            a.removeEventListener(b, c, !1)
        } : b.detachEvent && (d = function(a, b, c) {
            a.detachEvent("on" + b, a[b + c]);
            try {
                delete a[b + c]
            } catch (d) {
                a[b + c] = void 0
            }
        });
        var e = {
            bind: c,
            unbind: d
        };
        "function" == typeof define && define.amd ? define("eventie/eventie", e) : a.eventie = e
    }(this),
    function(a) {
        function e(a, b) {
            for (var c in b) a[c] = b[c];
            return a
        }

        function g(a) {
            return "[object Array]" === f.call(a)
        }

        function h(a) {
            var b = [];
            if (g(a)) b = a;
            else if ("number" == typeof a.length)
                for (var c = 0, d = a.length; c < d; c++) b.push(a[c]);
            else b.push(a);
            return b
        }

        function i(a, f) {
            function g(a, c, d) {
                if (!(this instanceof g)) return new g(a, c);
                "string" == typeof a && (a = document.querySelectorAll(a)), this.elements = h(a), this.options = e({}, this.options), "function" == typeof c ? d = c : e(this.options, c), d && this.on("always", d), this.getImages(), b && (this.jqDeferred = new b.Deferred);
                var f = this;
                setTimeout(function() {
                    f.check()
                })
            }

            function j(a) {
                this.img = a
            }
            g.prototype = new a, g.prototype.options = {}, g.prototype.getImages = function() {
                this.images = [];
                for (var a = 0, b = this.elements.length; a < b; a++) {
                    var c = this.elements[a];
                    "IMG" === c.nodeName && this.addImage(c);
                    for (var d = c.querySelectorAll("img"), e = 0, f = d.length; e < f; e++) {
                        var g = d[e];
                        this.addImage(g)
                    }
                }
            }, g.prototype.addImage = function(a) {
                var b = new j(a);
                this.images.push(b)
            }, g.prototype.check = function() {
                function f(f, g) {
                    return a.options.debug && d && c.log("confirm", f, g), a.progress(f), b++, b === e && a.complete(), !0
                }
                var a = this,
                    b = 0,
                    e = this.images.length;
                if (this.hasAnyBroken = !1, !e) return void this.complete();
                for (var g = 0; g < e; g++) {
                    var h = this.images[g];
                    h.on("confirm", f), h.check()
                }
            }, g.prototype.progress = function(a) {
                this.hasAnyBroken = this.hasAnyBroken || !a.isLoaded;
                var b = this;
                setTimeout(function() {
                    b.emit("progress", b, a), b.jqDeferred && b.jqDeferred.notify(b, a)
                })
            }, g.prototype.complete = function() {
                var a = this.hasAnyBroken ? "fail" : "done";
                this.isComplete = !0;
                var b = this;
                setTimeout(function() {
                    if (b.emit(a, b), b.emit("always", b), b.jqDeferred) {
                        var c = b.hasAnyBroken ? "reject" : "resolve";
                        b.jqDeferred[c](b)
                    }
                })
            }, b && (b.fn.imagesLoaded = function(a, c) {
                var d = new g(this, a, c);
                return d.jqDeferred.promise(b(this))
            });
            var i = {};
            return j.prototype = new a, j.prototype.check = function() {
                var a = i[this.img.src];
                if (a) return void this.useCached(a);
                if (i[this.img.src] = this, this.img.complete && void 0 !== this.img.naturalWidth) return void this.confirm(0 !== this.img.naturalWidth, "naturalWidth");
                var b = this.proxyImage = new Image;
                f.bind(b, "load", this), f.bind(b, "error", this), b.src = this.img.src
            }, j.prototype.useCached = function(a) {
                if (a.isConfirmed) this.confirm(a.isLoaded, "cached was confirmed");
                else {
                    var b = this;
                    a.on("confirm", function(a) {
                        return b.confirm(a.isLoaded, "cache emitted confirmed"), !0
                    })
                }
            }, j.prototype.confirm = function(a, b) {
                this.isConfirmed = !0, this.isLoaded = a, this.emit("confirm", this, b)
            }, j.prototype.handleEvent = function(a) {
                var b = "on" + a.type;
                this[b] && this[b](a)
            }, j.prototype.onload = function() {
                this.confirm(!0, "onload"), this.unbindProxyEvents()
            }, j.prototype.onerror = function() {
                this.confirm(!1, "onerror"), this.unbindProxyEvents()
            }, j.prototype.unbindProxyEvents = function() {
                f.unbind(this.proxyImage, "load", this), f.unbind(this.proxyImage, "error", this)
            }, g
        }
        var b = a.jQuery,
            c = a.console,
            d = "undefined" != typeof c,
            f = Object.prototype.toString;
        "function" == typeof define && define.amd ? define(["eventEmitter/EventEmitter", "eventie/eventie"], i) : a.imagesLoaded = i(a.EventEmitter, a.eventie)
    }(window);