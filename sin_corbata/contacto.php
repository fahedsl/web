<?php
	require('php/arriba.php');

	$telefono1	= '940235704';
	$telefono2	= '973699467';
	$email		= 'sincorbata@mail.com';

	$c_nombre = 'Tu nombre';
	$c_email = 'Tu correo electrónico';
	$c_mensaje = '¿Quieres información, una cotización, decirnos hola?';
	$c_boton = 'Enviar';

	echo'
	<section class="s12 fondo1">
		<div class="color2 s-pad-v-2 m-pad-v-3 l-pad-v-4 s-pad-h-1 m-pad-h-2">
			<h3>CONTÁCTO</h3>
			<div class="s12">
				<div class="item4">';

	//Iconos de contacto
	if (isset($telefono1) && !is_null($telefono1) && !empty($telefono1)) {
		echo '
					<a class="vinculo" href="tel:'.$telefono1.'" title="ola k ase?">
						<span class="">
							<img alt="teléfono" src="img/web/telf.svg" style="height:1.5rem; width:1.5rem; margin: 0 0.3em">
						</span>
						<span class="">'.$telefono1.'</span>
					</a>';
	}
	if (isset($telefono2) && !is_null($telefono2) && !empty($telefono2)) {
		echo '
					<a class="vinculo" href="tel:'.$telefono2.'" title="Llamamé, abababa.">
						<span class="">
							<img alt="teléfono" src="img/web/telf.svg" style="height:1.5rem; width:1.5rem; margin: 0 0.3em">
						</span>
						<span class="">'.$telefono2.'</span>
					</a>';
	}
	if (isset($email) && !is_null($email) && !empty($email)) {
		echo '
					<a class="vinculo" href="mailto:'.$email.'" title="Envíanos un correo electrónico, o usa el formulario de abajo. :)">
						<span class="correo electrónico">
							<img alt="" src="img/web/email.svg" style="height:1.5rem; width:1.5rem; margin: 0 0.3em">
						</span>
						<span class="">'.$email.'</span>
					</a>';
	}
	echo '
					<a href="Contacto - Marketing Sin Corbata.vcf" title="Descárgate nuestra tarjeta virtual de contacto.">
						<span class="">
							<img alt="Tarjeta de contacto" src="img/web/tarjeta.svg" style="height:1.5rem; width:1.5rem; margin: 0 0.3em">
						</span>
						<span class="">VCard</span>
					</a>
				</div>
			</div>';

	//Formulario de contacto
	echo '
			<form class="s-pad-h-1 s-pad-t-2" id="contacto" action="contacto_envio.php?ficha=contacto&red='.$_SERVER['PHP_SELF'].'" method="POST">
				<div class="s12 s-pad-t-1 s-pad-b-0">
					<p class="centrado">Puedes escribirnos desde aquí:</p>
				</div>
				
				<div class="s12 s-pad-v-1">
					<input class="entrada" id="nombre" type="text" name="nombre" required="" placeholder="'.$c_nombre.'" title="¿Cómo deseas que te llamemos?"/>
				</div>
				<div class="s12 s-pad-v-1">
					<input class="entrada" id="email" type="email" name="email" required="" placeholder="'.$c_email.'"  title="¿Cómo podemos ubicarte?"/>
				</div>
				<div class="s12 s-pad-v-1">
					<textarea class="entrada" style="min-height: 5rem;" id="msj" name="msj" rows="5" required="" placeholder="'.$c_mensaje.'"  title="¿Comentarios, preguntas, o... una poesía tal vez?"></textarea>
				</div>
				<div class="s12 s-pad-v-1">
					<input class="boton" id="submit" type="submit" value="'.$c_boton.'"  title="Envíiiiialo, Envíiiiialo :)">
				</div>
			</form>';
	echo '
		</div>
	</section>';

	require('php/abajo.php');
?>