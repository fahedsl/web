<?php
	require('php/arriba.php');
?>

	<section class="s12 fondo3">
		<div class="s12 color3 t-color2 s-pad-v-2 m-pad-v-3 l-pad-v-4 s-pad-h-1 m-pad-h-2 centrado">
			<div class="s12 centrado">
				<h3>NUESTROS SERVICIOS</h3>
				<p class="s-pad-t-1 font-15 centrado s-noveo ">Aquí va algun texto descriptivo de nuestros servicios horribles.</p>
			</div>
<!-- Lista de servicios -->
			<div class="s12 m6 s-pad-t-2 m-pad-t-3 l-pad-t-4 s-pad-h-1">
				<div class="s12 l6">
					<img alt="Publicidad Social" class="s-tam-h-6 m-tam-h-8" src="img/servicios/4.svg">
				</div>
				<div class="s12 l6 s-pad-t-1 l-pad-t-0">
					<h5 class="s12 mayuscula">Publicidad Social</h5>
					<p class="s12 justificado">
						Diseñamos tu campaña de marketing en redes sociales y creamos contenido para que tengas presencia en los sitios web que interesan más a tu público objetivo.
					</p>
				</div>
			</div>
			<div class="s12 m6 s-pad-t-2 m-pad-t-3 l-pad-t-4 s-pad-h-1">
				<div class="s12 l6">
					<img alt="Diseño publicitario" class="s-tam-h-6 m-tam-h-8" src="img/servicios/2.svg">
				</div>
				<div class="s12 l6 s-pad-t-1 l-pad-t-0">
					<h5 class="s12 mayuscula">Diseño Publicitario</h5>
					<p class="s12 justificado">
						Creamos logos, imágenes, paneles, papelería, publicaciones y cualquier diseño que tu empresa requiera para publicitarse.
					</p>
				</div>
			</div>
			<div class="s12 m6 s-pad-t-2 m-pad-t-3 l-pad-t-4 s-pad-h-1">
				<div class="s12 l6">
					<img alt="Desarrollo web" class="s-tam-h-6 m-tam-h-8" src="img/servicios/3.svg">
				</div>
				<div class="s12 l6 s-pad-t-1 l-pad-t-0">
					<h5 class="s12 mayuscula">Desarrollo web</h5>
					<p class="s12 justificado">
						Creamos sitios web adaptables a todos los dispositivos, con los estándares de calidad que requeridos para poder posicionar tu marca en internet.
					</p>
				</div>
			</div>
			<div class="s12 m6 s-pad-t-2 m-pad-t-3 l-pad-t-4 s-pad-h-1">
				<div class="s12 l6">
					<img alt="Desarrollo de imagen corporativa" class="s-tam-h-6 m-tam-h-8" src="img/servicios/1.svg">
				</div>
				<div class="s12 l6 s-pad-t-1 l-pad-t-0">
					<h5 class="s12 mayuscula">Desarrollo de Imagen Corporativa</h5>
					<p class="s12 justificado">
						Nuestro lenguaje es simple y ameno, siempre procuramos ser cercanos con nuestros clientes para facilitar el intercambio de ideas y así lograr los resultados deseados.
					</p>
				</div>
			</div>
		</div>
	</section>

<?php
	require('php/abajo.php');
?>