<?php
	require('php/arriba.php');
// EXISTE UNA ENTRADA ELEGIDA
	if(isset($_GET['entrada']) && !is_null($_GET['entrada'])){
		$dir = 'blog/'.$_GET['entrada'];
		$lista = scandir($dir);
		$titulofd = $dir.'/titulo.txt';
		$titulof = fopen($titulofd, 'r');
		$titulo = fread($titulof,filesize($titulofd));
		fclose($titulof);
		$contenidofd = $dir.'/contenido.txt';
		$contenidof = fopen($contenidofd, 'r');
		$contenido = fread($contenidof,filesize($contenidofd));
		fclose($contenidof);
		$socialdir = $_SERVER['HTTP_REFERER'].'?'.$_SERVER['QUERY_STRING'];
		$socialdir1 = urlencode($socialdir);
		$socialtitulo = urlencode($titulo);
		echo'
		<div class="fondo3">
			<div class="color3">
				<div class="social">
<!--Facebook-->
					<div class="rs facebook">
						<a target="_blank" href="https://www.facebook.com/sharer.php?u='.$socialdir1.'&t='.$socialtitulo.'"></a>
					</div>
<!--Twitter-->
					<div class="rs twitter">
						<a target="_blank" href="https://twitter.com/intent/tweet?text='.$socialtitulo.'&url='.$socialdir1.'&via=Mkt_Sin_Corbata"></a>
					</div>
<!--Google plus-->
					<div class="rs googleplus">
						<a target="_blank" href="https://plus.google.com/share?url='.$socialdir1.'"></a>
					</div>
<!--Linkedin-->
					<div class="rs linkedin">
						<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url='.$socialdir1.'&title='.$socialtitulo.'&source=http://www.mktsincorbata.com/blog.php"></a>
					</div>
<!--Pinterest-->
					<div class="rs pinterest">
						<a target="_blank" href="https://pinterest.com/pin/create/bookmarklet/?url='.$socialdir1.'&media=http://www.mktsincorbata.com/blog/'.$_GET['entrada'].'/principal.jpg&description='.$socialtitulo.'"></a>
					</div>
<!--E-mail-->
					<div class="rs email">
						<a href="mailto:?subject='.$socialtitulo.'&body='.$socialdir1.'"></a>
					</div>
				</div>
				<div class="blog fondo1 s-marg-v-2 ">
					<div class="color1 s-pad-v-1 m-pad-v-2 l-pad-v-3 s-pad-h-1 m-pad-h-3">
						<h1>'.$titulo.'</h1>
						'.$contenido.'
					</div>
				</div>
			</div>
		</div>';
	}
// LISTA DE ENTRADAS EXISTENTES
	elseif(!isset($_GET['entrada']) || is_null($_GET['entrada'])) {
		echo'
		<div id="blog-lista">';

		$dir = 'blog';
		$lista = scandir($dir);
		for( $i = 0; $i < sizeof($lista); $i++ ){
			$item = $lista[$i];
			if ($item != '.' && $item != '..') {
				$titulofd = $dir.'/'.$item.'/titulo.txt';
				$titulof = fopen($titulofd, 'r');
				$titulo = fread($titulof,filesize($titulofd));
				fclose($titulof);
				$descripcionfd = $dir.'/'.$item.'/descripcion.txt';
				$descripcionf = fopen($descripcionfd, 'r');
				$descripcion = fread($descripcionf,filesize($descripcionfd));
				fclose($descripcionf);

				echo'
				<section class="sec m-pad-t-1 l-pad-t-2">
					<h3 style="display:none">'.$titulo.' - Blog - Marketing Sin Corbata</h3>
					<div class="s-pad-t-2">
						<a href="blog.php?entrada='.$item.'">
							<h3>'.$titulo.'</h3>
						</a>
						<a href="blog.php?entrada='.$item.'">
							<img alt="imagen de portada del artículo" style="height:200px;" src="'.$dir.'/'.$item.'/principal.jpg">
						</a>
						<a href="blog.php?entrada='.$item.'">
							<p class="s-pad-h-2 m-pad-h-3 l-pad-h-4 s-pad-v-1 centrado">'.$descripcion.'</p>
						</a>
					</div>
				</section>';
			}
		}
		echo'
			</div>';
	}


	require('php/abajo.php');
?>